-- BRZ-Proxy setzen
npm config set proxy 172.30.9.12:8080
npm config set https-proxy 172.30.9.12:8080
-- die selbsterstellten BRZ-Proxy-Zertifikate akzeptieren
npm set strict-ssl false

-- Dev-Dependencies global installieren damit sie als Prozesse ausgeführt werden können
npm install -g concurrently
npm install -g lite-server
npm install -g typescript
-- Compile-Dependencies
npm install core-js
npm install systemjs

jquery.d.ts
npm install -g depcheck typescript

-- lite-server-Doku: https://github.com/johnpapa/lite-server