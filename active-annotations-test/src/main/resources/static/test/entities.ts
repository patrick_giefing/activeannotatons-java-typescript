

declare var Date : any;

export enum Enum1 {
	A,
	B,
	C,
	D,
	E
}
export interface X2<X> {
}
export abstract class Node<T> {
	public value: T;

	public static createNodeFromPojo<T>(pojo: any): Node<T> {
		if(!pojo) return null;
		let instance = Node.createSpecificNode<T>(pojo);
		instance.loadFromPojo(pojo);
		return instance;
	}

	public static createSpecificNode<T>(pojo: any): Node<T> {
		let discriminator = pojo.discriminator;
		if (discriminator == 'StringNode') {return StringNode.createStringNodeFromPojo(pojo) as any as Node<T>;}
		else 		if (discriminator == 'IntegerNode') {return IntegerNode.createIntegerNodeFromPojo(pojo) as any as Node<T>;}
		else 		if (discriminator == 'NodeWithParameter') {return NodeWithParameter.createNodeWithParameterFromPojo(pojo) as any as Node<T>;}
		throw new Error("Can't create instance for discriminator " + discriminator);
	}

	public loadFromPojo(pojo: any): this {
		if(!pojo) return this;
this.value = pojo['value'];
		return this;
	}

public toPojo(): any {
var pojo: any = {};
pojo['value'] = PojoBuilder.toPojo(this.value);	return pojo;
}
	public getValue(): T {
		return this.value;
	}

	public setValue(value: T): this {
		this.value = value;
		return this;
	}

}

export class StringNode extends Node<S> {

	public static createStringNode(): StringNode {
		return new StringNode();
	}

	public static createStringNodeFromPojo(pojo: any): StringNode {
		if(!pojo) return null;
		let instance = StringNode.createSpecificStringNode(pojo);
		instance.loadFromPojo(pojo);
		return instance;
	}

	public static createSpecificStringNode(pojo: any): StringNode {
		return StringNode.createStringNodeFromPojo(pojo);
	}

	public loadFromPojo(pojo: any): this {
		if(!pojo) return this;
		super.loadFromPojo(pojo);

		return this;
	}

public toPojo(): any {
var pojo: any = super.toPojo();
	return pojo;
}
}

export class IntegerNode extends Node<number> {

	public static createIntegerNode(): IntegerNode {
		return new IntegerNode();
	}

	public static createIntegerNodeFromPojo(pojo: any): IntegerNode {
		if(!pojo) return null;
		let instance = IntegerNode.createSpecificIntegerNode(pojo);
		instance.loadFromPojo(pojo);
		return instance;
	}

	public static createSpecificIntegerNode(pojo: any): IntegerNode {
		return IntegerNode.createIntegerNodeFromPojo(pojo);
	}

	public loadFromPojo(pojo: any): this {
		if(!pojo) return this;
		super.loadFromPojo(pojo);

		return this;
	}

public toPojo(): any {
var pojo: any = super.toPojo();
	return pojo;
}
}

export class NodeWithParameter<P> extends Node<P> {

	public static createNodeWithParameter<P>(): NodeWithParameter<P> {
		return new NodeWithParameter<P>();
	}

	public static createNodeWithParameterFromPojo<P>(pojo: any): NodeWithParameter<P> {
		if(!pojo) return null;
		let instance = NodeWithParameter.createSpecificNodeWithParameter<P>(pojo);
		instance.loadFromPojo(pojo);
		return instance;
	}

	public static createSpecificNodeWithParameter<P>(pojo: any): NodeWithParameter<P> {
		return NodeWithParameter.createNodeWithParameterFromPojo<P>(pojo);
	}

	public loadFromPojo(pojo: any): this {
		if(!pojo) return this;
		super.loadFromPojo(pojo);

		return this;
	}

public toPojo(): any {
var pojo: any = super.toPojo();
	return pojo;
}
}

export class EntityA {
	public s: string;
	public sa: string;
	public d: Date;
	public da: Date[];
	public i: number;
	public ia: number[];
	public sh: number;
	public sha: number[];
	public f: number;
	public fa: number[];
	public l: number;
	public la: number[];
	public dbl: number;
	public dbla: number[];
	public b: number;
	public ba: number[];
	public bo: boolean;
	public boa: boolean[];
	public wi: number;
	public wia: number[];
	public wsh: number;
	public wsha: number[];
	public wf: number;
	public wfa: number[];
	public wl: number;
	public wla: number[];
	public wdbl: number;
	public wdbla: number[];
	public wb: number;
	public wba: number[];
	public wbo: boolean;
	public wboa: boolean[];
	public strings: string[];
	public stringMap: {[key: string]: string};
	public stringListMap: {[key: string]: string[]};
	public stringMapMap: {[key: string]: {[key: string]: string}};
	public entityBMap: {[key: string]: EntityB};
	public entityBMapList: {[key: string]: EntityB[]};
	public entityBMapMapList: {[key: string]: {[key: number]: EntityB[]}};
	public entityBMapListMap: {[key: string]: {[key: number]: EntityB}[]};
	public localDate: Date;
	public localDates: Date[];
	public localDateList: Date[];
	public localDateSet: Date[];
	public localTime: Date;
	public localTimes: Date[];
	public localTimeList: Date[];
	public localTimeSet: Date[];
	public localDateTime: Date;
	public localDateTimes: Date[];
	public localDateTimeList: Date[];
	public localDateTimeSet: Date[];
	public laInts: number[][];
	public laInts2: number[][];
	public lllInts: number[][][];
	public aaaInts: number[][][];
	public enum1: Enum1;
	public enumList: Enum1[];
	public enumMap: {[key: string]: Enum1};
	public enumMapMapList: {[key: string]: {[key: number]: Enum1[]}};
	public enum1_s: Enum1;
	public enum1_num: Enum1;
	public enum1_nat: Enum1;
	public enum1_scal: Enum1;
	public enum1_arr: Enum1;

	public static createEntityA(): EntityA {
		return new EntityA();
	}

	public static createEntityAFromPojo(pojo: any): EntityA {
		if(!pojo) return null;
		let instance = EntityA.createSpecificEntityA(pojo);
		instance.loadFromPojo(pojo);
		return instance;
	}

	public static createSpecificEntityA(pojo: any): EntityA {
		return EntityA.createEntityAFromPojo(pojo);
	}

	public loadFromPojo(pojo: any): this {
		if(!pojo) return this;
this.s = pojo['s'];
this.sa = pojo['sa'];
this.d = pojo['d'] ? new Date(pojo['d']) : null;
	this.da = pojo['da'] ? pojo['da'].map(it => it ? new Date(it) : null) : []
this.i = pojo['i'];
	this.ia = pojo['ia'] ? pojo['ia'].map(it => it) : []
this.sh = pojo['sh'];
	this.sha = pojo['sha'] ? pojo['sha'].map(it => it) : []
this.f = pojo['f'];
	this.fa = pojo['fa'] ? pojo['fa'].map(it => it) : []
this.l = pojo['l'];
	this.la = pojo['la'] ? pojo['la'].map(it => it) : []
this.dbl = pojo['dbl'];
	this.dbla = pojo['dbla'] ? pojo['dbla'].map(it => it) : []
this.b = pojo['b'];
	this.ba = pojo['ba'] ? pojo['ba'].map(it => it) : []
this.bo = pojo['bo'];
	this.boa = pojo['boa'] ? pojo['boa'].map(it => it) : []
this.wi = pojo['wi'];
	this.wia = pojo['wia'] ? pojo['wia'].map(it => it) : []
this.wsh = pojo['wsh'];
	this.wsha = pojo['wsha'] ? pojo['wsha'].map(it => it) : []
this.wf = pojo['wf'];
	this.wfa = pojo['wfa'] ? pojo['wfa'].map(it => it) : []
this.wl = pojo['wl'];
	this.wla = pojo['wla'] ? pojo['wla'].map(it => it) : []
this.wdbl = pojo['wdbl'];
	this.wdbla = pojo['wdbla'] ? pojo['wdbla'].map(it => it) : []
this.wb = pojo['wb'];
	this.wba = pojo['wba'] ? pojo['wba'].map(it => it) : []
this.wbo = pojo['wbo'];
	this.wboa = pojo['wboa'] ? pojo['wboa'].map(it => it) : []
	this.strings = pojo['string-s'] ? pojo['string-s'].map(it => it) : []
	if(pojo['string-map']) {
		let obj1 = pojo['string-map'];
		this.stringMap = {};
		for(let key1 in obj1) {
			this.stringMap[key1] = obj1[key1];
		}
	}
	if(pojo['stringListMap']) {
		let obj1 = pojo['stringListMap'];
		this.stringListMap = {};
		for(let key1 in obj1) {
					this.stringListMap[key1] = obj1[key1] ? obj1[key1].map(it => it) : []
		}
	}
	if(pojo['stringMapMap']) {
		let obj1 = pojo['stringMapMap'];
		this.stringMapMap = {};
		for(let key1 in obj1) {
					if(obj1[key1]) {
					let obj2 = obj1[key1];
					this.stringMapMap[key1] = {};
					for(let key2 in obj2) {
						this.stringMapMap[key1][key2] = obj2[key2];
					}
				}
		}
	}
	if(pojo['entityBMap']) {
		let obj1 = pojo['entityBMap'];
		this.entityBMap = {};
		for(let key1 in obj1) {
			this.entityBMap[key1] = EntityB.createEntityBFromPojo(obj1[key1]);
		}
	}
	if(pojo['entityBMapList']) {
		let obj1 = pojo['entityBMapList'];
		this.entityBMapList = {};
		for(let key1 in obj1) {
					this.entityBMapList[key1] = obj1[key1] ? obj1[key1].map(pojo2 => EntityB.createEntityBFromPojo(pojo2)) : []
		}
	}
	if(pojo['entityBMapMapList']) {
		let obj1 = pojo['entityBMapMapList'];
		this.entityBMapMapList = {};
		for(let key1 in obj1) {
					if(obj1[key1]) {
					let obj2 = obj1[key1];
					this.entityBMapMapList[key1] = {};
					for(let key2 in obj2) {
									this.entityBMapMapList[key1][key2] = obj2[key2] ? obj2[key2].map(pojo3 => EntityB.createEntityBFromPojo(pojo3)) : []
					}
				}
		}
	}
	if(pojo['entityBMapListMap']) {
		let obj1 = pojo['entityBMapListMap'];
		this.entityBMapListMap = {};
		for(let key1 in obj1) {
					this.entityBMapListMap[key1] = obj1[key1] ? obj1[key1].map(pojo2 => 			{
					if(pojo2) {
						let obj3 = pojo2;
						pojo2 = {};
						for(let key3 in obj3) {
							pojo2[key3] = EntityB.createEntityBFromPojo(obj3[key3]);
						}
					}
					return pojo2;
		}) : []
		}
	}
this.localDate = pojo['localDate'] ? Date.parseString(pojo['localDate'], 'yyyy-MM-dd') : null;
	this.localDates = pojo['localDates'] ? pojo['localDates'].map(it => it ? Date.parseString(it, 'yyyy-MM-dd') : null) : []
	this.localDateList = pojo['localDateList'] ? pojo['localDateList'].map(it => it ? Date.parseString(it, 'yyyy-MM-dd') : null) : []
	this.localDateSet = pojo['localDateSet'] ? pojo['localDateSet'].map(it => it ? Date.parseString(it, 'yyyy-MM-dd') : null) : []
this.localTime = pojo['localTime'] ? Date.parseString(pojo['localTime'], 'hh:mm') : null;
	this.localTimes = pojo['localTimes'] ? pojo['localTimes'].map(it => it ? Date.parseString(it, 'hh:mm') : null) : []
	this.localTimeList = pojo['localTimeList'] ? pojo['localTimeList'].map(it => it ? Date.parseString(it, 'hh:mm') : null) : []
	this.localTimeSet = pojo['localTimeSet'] ? pojo['localTimeSet'].map(it => it ? Date.parseString(it, 'hh:mm') : null) : []
this.localDateTime = pojo['localDateTime'] ? Date.parseString(pojo['localDateTime'], 'yyyy-MM-dd hh:mm') : null;
	this.localDateTimes = pojo['localDateTimes'] ? pojo['localDateTimes'].map(it => it ? Date.parseString(it, 'yyyy-MM-dd hh:mm') : null) : []
	this.localDateTimeList = pojo['localDateTimeList'] ? pojo['localDateTimeList'].map(it => it ? Date.parseString(it, 'yyyy-MM-dd hh:mm') : null) : []
	this.localDateTimeSet = pojo['localDateTimeSet'] ? pojo['localDateTimeSet'].map(it => it ? Date.parseString(it, 'yyyy-MM-dd hh:mm') : null) : []
	this.laInts = pojo['laInts'] ? pojo['laInts'].map(pojo1 => 		pojo1 = pojo1 ? pojo1.map(it => it) : []) : []
	this.laInts2 = pojo['laInts2'] ? pojo['laInts2'].map(it => it) : []
	this.lllInts = pojo['lllInts'] ? pojo['lllInts'].map(pojo1 => 		pojo1 = pojo1 ? pojo1.map(pojo2 => 			pojo2 = pojo2 ? pojo2.map(it => it) : []) : []) : []
	this.aaaInts = pojo['aaaInts'] ? pojo['aaaInts'].map(it => it) : []
this.enum1 = pojo['enum1'] ? Enum1[pojo['enum1'] as string] : null;
	this.enumList = pojo['enumList'] ? pojo['enumList'].map(it => it) : []
	if(pojo['enumMap']) {
		let obj1 = pojo['enumMap'];
		this.enumMap = {};
		for(let key1 in obj1) {
			this.enumMap[key1] = obj1[key1] ? Enum1[obj1[key1] as string] : null;
		}
	}
	if(pojo['enumMapMapList']) {
		let obj1 = pojo['enumMapMapList'];
		this.enumMapMapList = {};
		for(let key1 in obj1) {
					if(obj1[key1]) {
					let obj2 = obj1[key1];
					this.enumMapMapList[key1] = {};
					for(let key2 in obj2) {
									this.enumMapMapList[key1][key2] = obj2[key2] ? obj2[key2].map(it => it) : []
					}
				}
		}
	}
this.enum1_s = pojo['enum1_s'] ? Enum1[pojo['enum1_s'] as string] : null;
this.enum1_num = pojo['enum1_num'] ? Enum1[pojo['enum1_num'] as string] : null;
this.enum1_nat = pojo['enum1_nat'] ? Enum1[pojo['enum1_nat'] as string] : null;
this.enum1_scal = pojo['enum1_scal'] ? Enum1[pojo['enum1_scal'] as string] : null;
this.enum1_arr = pojo['enum1_arr'] ? Enum1[pojo['enum1_arr'] as string] : null;
		return this;
	}

public toPojo(): any {
var pojo: any = {};
pojo['s'] = PojoBuilder.toPojo(this.s);
pojo['sa'] = PojoBuilder.toPojo(this.sa);
pojo['d'] = this.d ? this.d.getTime() : null;
pojo['da'] = PojoBuilder.toPojo(this.da);
pojo['i'] = PojoBuilder.toPojo(this.i);
pojo['ia'] = PojoBuilder.toPojo(this.ia);
pojo['sh'] = PojoBuilder.toPojo(this.sh);
pojo['sha'] = PojoBuilder.toPojo(this.sha);
pojo['f'] = PojoBuilder.toPojo(this.f);
pojo['fa'] = PojoBuilder.toPojo(this.fa);
pojo['l'] = PojoBuilder.toPojo(this.l);
pojo['la'] = PojoBuilder.toPojo(this.la);
pojo['dbl'] = PojoBuilder.toPojo(this.dbl);
pojo['dbla'] = PojoBuilder.toPojo(this.dbla);
pojo['b'] = PojoBuilder.toPojo(this.b);
pojo['ba'] = PojoBuilder.toPojo(this.ba);
pojo['bo'] = PojoBuilder.toPojo(this.bo);
pojo['boa'] = PojoBuilder.toPojo(this.boa);
pojo['wi'] = PojoBuilder.toPojo(this.wi);
pojo['wia'] = PojoBuilder.toPojo(this.wia);
pojo['wsh'] = PojoBuilder.toPojo(this.wsh);
pojo['wsha'] = PojoBuilder.toPojo(this.wsha);
pojo['wf'] = PojoBuilder.toPojo(this.wf);
pojo['wfa'] = PojoBuilder.toPojo(this.wfa);
pojo['wl'] = PojoBuilder.toPojo(this.wl);
pojo['wla'] = PojoBuilder.toPojo(this.wla);
pojo['wdbl'] = PojoBuilder.toPojo(this.wdbl);
pojo['wdbla'] = PojoBuilder.toPojo(this.wdbla);
pojo['wb'] = PojoBuilder.toPojo(this.wb);
pojo['wba'] = PojoBuilder.toPojo(this.wba);
pojo['wbo'] = PojoBuilder.toPojo(this.wbo);
pojo['wboa'] = PojoBuilder.toPojo(this.wboa);
pojo['string-s'] = PojoBuilder.toPojo(this.strings);
pojo['string-map'] = PojoBuilder.toPojo(this.stringMap);
pojo['stringListMap'] = PojoBuilder.toPojo(this.stringListMap);
pojo['stringMapMap'] = PojoBuilder.toPojo(this.stringMapMap);
pojo['entityBMap'] = PojoBuilder.toPojo(this.entityBMap);
pojo['entityBMapList'] = PojoBuilder.toPojo(this.entityBMapList);
pojo['entityBMapMapList'] = PojoBuilder.toPojo(this.entityBMapMapList);
pojo['entityBMapListMap'] = PojoBuilder.toPojo(this.entityBMapListMap);
pojo['localDate'] = this.localDate ? Date.toString(this.localDate, 'yyyy-MM-dd') : null;
pojo['localDates'] = PojoBuilder.toPojo(this.localDates);
pojo['localDateList'] = PojoBuilder.toPojo(this.localDateList);
pojo['localDateSet'] = PojoBuilder.toPojo(this.localDateSet);
pojo['localTime'] = this.localTime ? Date.toString(this.localTime, 'hh:mm') : null;
pojo['localTimes'] = PojoBuilder.toPojo(this.localTimes);
pojo['localTimeList'] = PojoBuilder.toPojo(this.localTimeList);
pojo['localTimeSet'] = PojoBuilder.toPojo(this.localTimeSet);
pojo['localDateTime'] = this.localDateTime ? Date.toString(this.localDateTime, 'yyyy-MM-dd hh:mm') : null;
pojo['localDateTimes'] = PojoBuilder.toPojo(this.localDateTimes);
pojo['localDateTimeList'] = PojoBuilder.toPojo(this.localDateTimeList);
pojo['localDateTimeSet'] = PojoBuilder.toPojo(this.localDateTimeSet);
pojo['laInts'] = PojoBuilder.toPojo(this.laInts);
pojo['laInts2'] = PojoBuilder.toPojo(this.laInts2);
pojo['lllInts'] = PojoBuilder.toPojo(this.lllInts);
pojo['aaaInts'] = PojoBuilder.toPojo(this.aaaInts);
pojo['enum1'] ? this.enum1 ? Enum1[this.enum1 as number] : null;
pojo['enumList'] = PojoBuilder.toPojo(this.enumList);
pojo['enumMap'] = PojoBuilder.toPojo(this.enumMap);
pojo['enumMapMapList'] = PojoBuilder.toPojo(this.enumMapMapList);
pojo['enum1_s'] ? this.enum1_s ? Enum1[this.enum1_s as number] : null;
pojo['enum1_num'] ? this.enum1_num ? Enum1[this.enum1_num as number] : null;
pojo['enum1_nat'] ? this.enum1_nat ? Enum1[this.enum1_nat as number] : null;
pojo['enum1_scal'] ? this.enum1_scal ? Enum1[this.enum1_scal as number] : null;
pojo['enum1_arr'] ? this.enum1_arr ? Enum1[this.enum1_arr as number] : null;	return pojo;
}
	public getS(): string {
		return this.s;
	}

	public setS(s: string): this {
		this.s = s;
		return this;
	}

	public getSa(): string {
		return this.sa;
	}

	public setSa(sa: string): this {
		this.sa = sa;
		return this;
	}

	public getD(): Date {
		return this.d;
	}

	public setD(d: Date): this {
		this.d = d;
		return this;
	}

	public getDa(): Date[] {
		return this.da;
	}

	public setDa(da: Date[]): this {
		this.da = da;
		return this;
	}

	public getI(): number {
		return this.i;
	}

	public setI(i: number): this {
		this.i = i;
		return this;
	}

	public getIa(): number[] {
		return this.ia;
	}

	public setIa(ia: number[]): this {
		this.ia = ia;
		return this;
	}

	public getSh(): number {
		return this.sh;
	}

	public setSh(sh: number): this {
		this.sh = sh;
		return this;
	}

	public getSha(): number[] {
		return this.sha;
	}

	public setSha(sha: number[]): this {
		this.sha = sha;
		return this;
	}

	public getF(): number {
		return this.f;
	}

	public setF(f: number): this {
		this.f = f;
		return this;
	}

	public getFa(): number[] {
		return this.fa;
	}

	public setFa(fa: number[]): this {
		this.fa = fa;
		return this;
	}

	public getL(): number {
		return this.l;
	}

	public setL(l: number): this {
		this.l = l;
		return this;
	}

	public getLa(): number[] {
		return this.la;
	}

	public setLa(la: number[]): this {
		this.la = la;
		return this;
	}

	public getDbl(): number {
		return this.dbl;
	}

	public setDbl(dbl: number): this {
		this.dbl = dbl;
		return this;
	}

	public getDbla(): number[] {
		return this.dbla;
	}

	public setDbla(dbla: number[]): this {
		this.dbla = dbla;
		return this;
	}

	public getB(): number {
		return this.b;
	}

	public setB(b: number): this {
		this.b = b;
		return this;
	}

	public getBa(): number[] {
		return this.ba;
	}

	public setBa(ba: number[]): this {
		this.ba = ba;
		return this;
	}

	public isBo(): boolean {
		return this.bo;
	}

	public setBo(bo: boolean): this {
		this.bo = bo;
		return this;
	}

	public getBoa(): boolean[] {
		return this.boa;
	}

	public setBoa(boa: boolean[]): this {
		this.boa = boa;
		return this;
	}

	public getWi(): number {
		return this.wi;
	}

	public setWi(wi: number): this {
		this.wi = wi;
		return this;
	}

	public getWia(): number[] {
		return this.wia;
	}

	public setWia(wia: number[]): this {
		this.wia = wia;
		return this;
	}

	public getWsh(): number {
		return this.wsh;
	}

	public setWsh(wsh: number): this {
		this.wsh = wsh;
		return this;
	}

	public getWsha(): number[] {
		return this.wsha;
	}

	public setWsha(wsha: number[]): this {
		this.wsha = wsha;
		return this;
	}

	public getWf(): number {
		return this.wf;
	}

	public setWf(wf: number): this {
		this.wf = wf;
		return this;
	}

	public getWfa(): number[] {
		return this.wfa;
	}

	public setWfa(wfa: number[]): this {
		this.wfa = wfa;
		return this;
	}

	public getWl(): number {
		return this.wl;
	}

	public setWl(wl: number): this {
		this.wl = wl;
		return this;
	}

	public getWla(): number[] {
		return this.wla;
	}

	public setWla(wla: number[]): this {
		this.wla = wla;
		return this;
	}

	public getWdbl(): number {
		return this.wdbl;
	}

	public setWdbl(wdbl: number): this {
		this.wdbl = wdbl;
		return this;
	}

	public getWdbla(): number[] {
		return this.wdbla;
	}

	public setWdbla(wdbla: number[]): this {
		this.wdbla = wdbla;
		return this;
	}

	public getWb(): number {
		return this.wb;
	}

	public setWb(wb: number): this {
		this.wb = wb;
		return this;
	}

	public getWba(): number[] {
		return this.wba;
	}

	public setWba(wba: number[]): this {
		this.wba = wba;
		return this;
	}

	public getWbo(): boolean {
		return this.wbo;
	}

	public setWbo(wbo: boolean): this {
		this.wbo = wbo;
		return this;
	}

	public getWboa(): boolean[] {
		return this.wboa;
	}

	public setWboa(wboa: boolean[]): this {
		this.wboa = wboa;
		return this;
	}

	public getStrings(): string[] {
		return this.strings;
	}

	public setStrings(strings: string[]): this {
		this.strings = strings;
		return this;
	}

	public getStringMap(): {[key: string]: string} {
		return this.stringMap;
	}

	public setStringMap(stringMap: {[key: string]: string}): this {
		this.stringMap = stringMap;
		return this;
	}

	public getStringListMap(): {[key: string]: string[]} {
		return this.stringListMap;
	}

	public setStringListMap(stringListMap: {[key: string]: string[]}): this {
		this.stringListMap = stringListMap;
		return this;
	}

	public getStringMapMap(): {[key: string]: {[key: string]: string}} {
		return this.stringMapMap;
	}

	public setStringMapMap(stringMapMap: {[key: string]: {[key: string]: string}}): this {
		this.stringMapMap = stringMapMap;
		return this;
	}

	public getEntityBMap(): {[key: string]: EntityB} {
		return this.entityBMap;
	}

	public setEntityBMap(entityBMap: {[key: string]: EntityB}): this {
		this.entityBMap = entityBMap;
		return this;
	}

	public getEntityBMapList(): {[key: string]: EntityB[]} {
		return this.entityBMapList;
	}

	public setEntityBMapList(entityBMapList: {[key: string]: EntityB[]}): this {
		this.entityBMapList = entityBMapList;
		return this;
	}

	public getEntityBMapMapList(): {[key: string]: {[key: number]: EntityB[]}} {
		return this.entityBMapMapList;
	}

	public setEntityBMapMapList(entityBMapMapList: {[key: string]: {[key: number]: EntityB[]}}): this {
		this.entityBMapMapList = entityBMapMapList;
		return this;
	}

	public getEntityBMapListMap(): {[key: string]: {[key: number]: EntityB}[]} {
		return this.entityBMapListMap;
	}

	public setEntityBMapListMap(entityBMapListMap: {[key: string]: {[key: number]: EntityB}[]}): this {
		this.entityBMapListMap = entityBMapListMap;
		return this;
	}

	public getLocalDate(): Date {
		return this.localDate;
	}

	public setLocalDate(localDate: Date): this {
		this.localDate = localDate;
		return this;
	}

	public getLocalDates(): Date[] {
		return this.localDates;
	}

	public setLocalDates(localDates: Date[]): this {
		this.localDates = localDates;
		return this;
	}

	public getLocalDateList(): Date[] {
		return this.localDateList;
	}

	public setLocalDateList(localDateList: Date[]): this {
		this.localDateList = localDateList;
		return this;
	}

	public getLocalDateSet(): Date[] {
		return this.localDateSet;
	}

	public setLocalDateSet(localDateSet: Date[]): this {
		this.localDateSet = localDateSet;
		return this;
	}

	public getLocalTime(): Date {
		return this.localTime;
	}

	public setLocalTime(localTime: Date): this {
		this.localTime = localTime;
		return this;
	}

	public getLocalTimes(): Date[] {
		return this.localTimes;
	}

	public setLocalTimes(localTimes: Date[]): this {
		this.localTimes = localTimes;
		return this;
	}

	public getLocalTimeList(): Date[] {
		return this.localTimeList;
	}

	public setLocalTimeList(localTimeList: Date[]): this {
		this.localTimeList = localTimeList;
		return this;
	}

	public getLocalTimeSet(): Date[] {
		return this.localTimeSet;
	}

	public setLocalTimeSet(localTimeSet: Date[]): this {
		this.localTimeSet = localTimeSet;
		return this;
	}

	public getLocalDateTime(): Date {
		return this.localDateTime;
	}

	public setLocalDateTime(localDateTime: Date): this {
		this.localDateTime = localDateTime;
		return this;
	}

	public getLocalDateTimes(): Date[] {
		return this.localDateTimes;
	}

	public setLocalDateTimes(localDateTimes: Date[]): this {
		this.localDateTimes = localDateTimes;
		return this;
	}

	public getLocalDateTimeList(): Date[] {
		return this.localDateTimeList;
	}

	public setLocalDateTimeList(localDateTimeList: Date[]): this {
		this.localDateTimeList = localDateTimeList;
		return this;
	}

	public getLocalDateTimeSet(): Date[] {
		return this.localDateTimeSet;
	}

	public setLocalDateTimeSet(localDateTimeSet: Date[]): this {
		this.localDateTimeSet = localDateTimeSet;
		return this;
	}

	public getLaInts(): number[][] {
		return this.laInts;
	}

	public setLaInts(laInts: number[][]): this {
		this.laInts = laInts;
		return this;
	}

	public getLaInts2(): number[][] {
		return this.laInts2;
	}

	public setLaInts2(laInts2: number[][]): this {
		this.laInts2 = laInts2;
		return this;
	}

	public getLllInts(): number[][][] {
		return this.lllInts;
	}

	public setLllInts(lllInts: number[][][]): this {
		this.lllInts = lllInts;
		return this;
	}

	public getAaaInts(): number[][][] {
		return this.aaaInts;
	}

	public setAaaInts(aaaInts: number[][][]): this {
		this.aaaInts = aaaInts;
		return this;
	}

	public getEnum1(): Enum1 {
		return this.enum1;
	}

	public setEnum1(enum1: Enum1): this {
		this.enum1 = enum1;
		return this;
	}

	public getEnumList(): Enum1[] {
		return this.enumList;
	}

	public setEnumList(enumList: Enum1[]): this {
		this.enumList = enumList;
		return this;
	}

	public getEnumMap(): {[key: string]: Enum1} {
		return this.enumMap;
	}

	public setEnumMap(enumMap: {[key: string]: Enum1}): this {
		this.enumMap = enumMap;
		return this;
	}

	public getEnumMapMapList(): {[key: string]: {[key: number]: Enum1[]}} {
		return this.enumMapMapList;
	}

	public setEnumMapMapList(enumMapMapList: {[key: string]: {[key: number]: Enum1[]}}): this {
		this.enumMapMapList = enumMapMapList;
		return this;
	}

	public getEnum1_s(): Enum1 {
		return this.enum1_s;
	}

	public setEnum1_s(enum1_s: Enum1): this {
		this.enum1_s = enum1_s;
		return this;
	}

	public getEnum1_num(): Enum1 {
		return this.enum1_num;
	}

	public setEnum1_num(enum1_num: Enum1): this {
		this.enum1_num = enum1_num;
		return this;
	}

	public getEnum1_nat(): Enum1 {
		return this.enum1_nat;
	}

	public setEnum1_nat(enum1_nat: Enum1): this {
		this.enum1_nat = enum1_nat;
		return this;
	}

	public getEnum1_scal(): Enum1 {
		return this.enum1_scal;
	}

	public setEnum1_scal(enum1_scal: Enum1): this {
		this.enum1_scal = enum1_scal;
		return this;
	}

	public getEnum1_arr(): Enum1 {
		return this.enum1_arr;
	}

	public setEnum1_arr(enum1_arr: Enum1): this {
		this.enum1_arr = enum1_arr;
		return this;
	}

}

export class X1<X, Y> {

	public static createX1<X, Y>(): X1<X, Y> {
		return new X1<X, Y>();
	}

	public static createX1FromPojo<X, Y>(pojo: any): X1<X, Y> {
		if(!pojo) return null;
		let instance = X1.createSpecificX1<X, Y>(pojo);
		instance.loadFromPojo(pojo);
		return instance;
	}

	public static createSpecificX1<X, Y>(pojo: any): X1<X, Y> {
		return X1.createX1FromPojo<X, Y>(pojo);
	}

	public loadFromPojo(pojo: any): this {
		if(!pojo) return this;

		return this;
	}

public toPojo(): any {
var pojo: any = {};
	return pojo;
}
}

export class Entity1<A, B, C extends X1<string, number> & X2<X1<string, string[]>>> {
	public i: number;
	public l: number;
	public s: string;
	public c: C;
	public entity2: Entity2<A, string, string>;
	public entities2: Entity2<number, A, X2<string>>[];

	public static createEntity1<A, B, C extends X1<string, number> & X2<X1<string, string[]>>>(): Entity1<A, B, C> {
		return new Entity1<A, B, C>();
	}

	public static createEntity1FromPojo<A, B, C extends X1<string, number> & X2<X1<string, string[]>>>(pojo: any): Entity1<A, B, C> {
		if(!pojo) return null;
		let instance = Entity1.createSpecificEntity1<A, B, C>(pojo);
		instance.loadFromPojo(pojo);
		return instance;
	}

	public static createSpecificEntity1<A, B, C extends X1<string, number> & X2<X1<string, string[]>>>(pojo: any): Entity1<A, B, C> {
		return Entity1.createEntity1FromPojo<A, B, C>(pojo);
	}

	public loadFromPojo(pojo: any): this {
		if(!pojo) return this;
this.i = pojo['i'];
this.l = pojo['l'];
this.s = pojo['s'];
this.c = pojo['c'];
this.entity2 = Entity2.createEntity2FromPojo<A, string, string>(pojo['entity2']);
	this.entities2 = pojo['entities2'] ? pojo['entities2'].map(pojo1 => Entity2.createEntity2FromPojo<number, A, X2<string>>(pojo1)) : []
		return this;
	}

public toPojo(): any {
var pojo: any = {};
pojo['i'] = PojoBuilder.toPojo(this.i);
pojo['l'] = PojoBuilder.toPojo(this.l);
pojo['s'] = PojoBuilder.toPojo(this.s);
pojo['c'] = PojoBuilder.toPojo(this.c);
pojo['entity2'] = this.entity2 ? this.entity2.toPojo() : null;
pojo['entities2'] = PojoBuilder.toPojo(this.entities2);	return pojo;
}
	public getI(): number {
		return this.i;
	}

	public setI(i: number): this {
		this.i = i;
		return this;
	}

	public getL(): number {
		return this.l;
	}

	public setL(l: number): this {
		this.l = l;
		return this;
	}

	public getS(): string {
		return this.s;
	}

	public setS(s: string): this {
		this.s = s;
		return this;
	}

	public getC(): C {
		return this.c;
	}

	public setC(c: C): this {
		this.c = c;
		return this;
	}

	public getEntity2(): Entity2<A, string, string> {
		return this.entity2;
	}

	public setEntity2(entity2: Entity2<A, string, string>): this {
		this.entity2 = entity2;
		return this;
	}

	public getEntities2(): Entity2<number, A, X2<string>>[] {
		return this.entities2;
	}

	public setEntities2(entities2: Entity2<number, A, X2<string>>[]): this {
		this.entities2 = entities2;
		return this;
	}

}

export class Entity2<X, Y, Z> {
	public nodeList: Node<any>[];
	public nodeList2: Node<any>[];
	public nodeList3: NodeWithParameter<string>[];

	public static createEntity2<X, Y, Z>(): Entity2<X, Y, Z> {
		return new Entity2<X, Y, Z>();
	}

	public static createEntity2FromPojo<X, Y, Z>(pojo: any): Entity2<X, Y, Z> {
		if(!pojo) return null;
		let instance = Entity2.createSpecificEntity2<X, Y, Z>(pojo);
		instance.loadFromPojo(pojo);
		return instance;
	}

	public static createSpecificEntity2<X, Y, Z>(pojo: any): Entity2<X, Y, Z> {
		return Entity2.createEntity2FromPojo<X, Y, Z>(pojo);
	}

	public loadFromPojo(pojo: any): this {
		if(!pojo) return this;
	this.nodeList = pojo['nodeList'] ? pojo['nodeList'].map(pojo1 => Node.createNodeFromPojo<any>(pojo1)) : []
	this.nodeList2 = pojo['nodeList2'] ? pojo['nodeList2'].map(pojo1 => Node.createNodeFromPojo<any>(pojo1)) : []
	this.nodeList3 = pojo['nodeList3'] ? pojo['nodeList3'].map(pojo1 => NodeWithParameter.createNodeWithParameterFromPojo<string>(pojo1)) : []
		return this;
	}

public toPojo(): any {
var pojo: any = {};
pojo['nodeList'] = PojoBuilder.toPojo(this.nodeList);
pojo['nodeList2'] = PojoBuilder.toPojo(this.nodeList2);
pojo['nodeList3'] = PojoBuilder.toPojo(this.nodeList3);	return pojo;
}
	public getNodeList(): Node<any>[] {
		return this.nodeList;
	}

	public setNodeList(nodeList: Node<any>[]): this {
		this.nodeList = nodeList;
		return this;
	}

	public getNodeList2(): Node<any>[] {
		return this.nodeList2;
	}

	public setNodeList2(nodeList2: Node<any>[]): this {
		this.nodeList2 = nodeList2;
		return this;
	}

	public getNodeList3(): NodeWithParameter<string>[] {
		return this.nodeList3;
	}

	public setNodeList3(nodeList3: NodeWithParameter<string>[]): this {
		this.nodeList3 = nodeList3;
		return this;
	}

}

export class S {

	public static createS(): S {
		return new S();
	}

	public static createSFromPojo(pojo: any): S {
		if(!pojo) return null;
		let instance = S.createSpecificS(pojo);
		instance.loadFromPojo(pojo);
		return instance;
	}

	public static createSpecificS(pojo: any): S {
		return S.createSFromPojo(pojo);
	}

	public loadFromPojo(pojo: any): this {
		if(!pojo) return this;

		return this;
	}

public toPojo(): any {
var pojo: any = {};
	return pojo;
}
}

export class EntityB {
	public a: EntityA;
	public aa: EntityA[];
	public aaa: EntityA[][];
	public la1: EntityA[];
	public la2: EntityA[];
	public la3: EntityA[];
	public la4: EntityA[][];
	public la5: EntityA[][];
	public la6: EntityA[][];

	public static createEntityB(): EntityB {
		return new EntityB();
	}

	public static createEntityBFromPojo(pojo: any): EntityB {
		if(!pojo) return null;
		let instance = EntityB.createSpecificEntityB(pojo);
		instance.loadFromPojo(pojo);
		return instance;
	}

	public static createSpecificEntityB(pojo: any): EntityB {
		return EntityB.createEntityBFromPojo(pojo);
	}

	public loadFromPojo(pojo: any): this {
		if(!pojo) return this;
this.a = EntityA.createEntityAFromPojo(pojo['a']);
	this.aa = pojo['aa'] ? pojo['aa'].map(pojo1 => EntityA.createEntityAFromPojo(pojo1)) : []
	this.aaa = pojo['aaa'] ? pojo['aaa'].map(pojo1 => 		pojo1 = pojo1 ? pojo1.map(pojo2 => EntityA.createEntityAFromPojo(pojo2)) : []) : []
	this.la1 = pojo['la1'] ? pojo['la1'].map(pojo1 => EntityA.createEntityAFromPojo(pojo1)) : []
	this.la2 = pojo['la2'] ? pojo['la2'].map(pojo1 => EntityA.createEntityAFromPojo(pojo1)) : []
	this.la3 = pojo['la3'] ? pojo['la3'].map(pojo1 => EntityA.createEntityAFromPojo(pojo1)) : []
	this.la4 = pojo['la4'] ? pojo['la4'].map(pojo1 => 		pojo1 = pojo1 ? pojo1.map(pojo2 => EntityA.createEntityAFromPojo(pojo2)) : []) : []
	this.la5 = pojo['la5'] ? pojo['la5'].map(pojo1 => 		pojo1 = pojo1 ? pojo1.map(pojo2 => EntityA.createEntityAFromPojo(pojo2)) : []) : []
	this.la6 = pojo['la6'] ? pojo['la6'].map(pojo1 => 		pojo1 = pojo1 ? pojo1.map(pojo2 => EntityA.createEntityAFromPojo(pojo2)) : []) : []
		return this;
	}

public toPojo(): any {
var pojo: any = {};
pojo['a'] = this.a ? this.a.toPojo() : null;
pojo['aa'] = PojoBuilder.toPojo(this.aa);
pojo['aaa'] = PojoBuilder.toPojo(this.aaa);
pojo['la1'] = PojoBuilder.toPojo(this.la1);
pojo['la2'] = PojoBuilder.toPojo(this.la2);
pojo['la3'] = PojoBuilder.toPojo(this.la3);
pojo['la4'] = PojoBuilder.toPojo(this.la4);
pojo['la5'] = PojoBuilder.toPojo(this.la5);
pojo['la6'] = PojoBuilder.toPojo(this.la6);	return pojo;
}
	public getA(): EntityA {
		return this.a;
	}

	public setA(a: EntityA): this {
		this.a = a;
		return this;
	}

	public getAa(): EntityA[] {
		return this.aa;
	}

	public setAa(aa: EntityA[]): this {
		this.aa = aa;
		return this;
	}

	public getAaa(): EntityA[][] {
		return this.aaa;
	}

	public setAaa(aaa: EntityA[][]): this {
		this.aaa = aaa;
		return this;
	}

	public getLa1(): EntityA[] {
		return this.la1;
	}

	public setLa1(la1: EntityA[]): this {
		this.la1 = la1;
		return this;
	}

	public getLa2(): EntityA[] {
		return this.la2;
	}

	public setLa2(la2: EntityA[]): this {
		this.la2 = la2;
		return this;
	}

	public getLa3(): EntityA[] {
		return this.la3;
	}

	public setLa3(la3: EntityA[]): this {
		this.la3 = la3;
		return this;
	}

	public getLa4(): EntityA[][] {
		return this.la4;
	}

	public setLa4(la4: EntityA[][]): this {
		this.la4 = la4;
		return this;
	}

	public getLa5(): EntityA[][] {
		return this.la5;
	}

	public setLa5(la5: EntityA[][]): this {
		this.la5 = la5;
		return this;
	}

	public getLa6(): EntityA[][] {
		return this.la6;
	}

	public setLa6(la6: EntityA[][]): this {
		this.la6 = la6;
		return this;
	}

}

