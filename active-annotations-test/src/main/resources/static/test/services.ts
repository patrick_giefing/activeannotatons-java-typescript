import {EntityA} from "./entities";

export class TypeScriptTestInterface {
	public static readonly A = "TypeScriptTestInterface.a";
	public static readonly A_REQUEST = "TypeScriptTestInterface.aRequest";
	public static readonly A_RESPONSE_SUCCESS = "TypeScriptTestInterface.aResponseSuccess";
	public static readonly A_RESPONSE_ERROR = "TypeScriptTestInterface.aResponseError";
	public static readonly AA = "TypeScriptTestInterface.aa";
	public static readonly AA_REQUEST = "TypeScriptTestInterface.aaRequest";
	public static readonly AA_RESPONSE_SUCCESS = "TypeScriptTestInterface.aaResponseSuccess";
	public static readonly AA_RESPONSE_ERROR = "TypeScriptTestInterface.aaResponseError";
	public static readonly AAA = "TypeScriptTestInterface.aaa";
	public static readonly AAA_REQUEST = "TypeScriptTestInterface.aaaRequest";
	public static readonly AAA_RESPONSE_SUCCESS = "TypeScriptTestInterface.aaaResponseSuccess";
	public static readonly AAA_RESPONSE_ERROR = "TypeScriptTestInterface.aaaResponseError";
	public static readonly LA1 = "TypeScriptTestInterface.la1";
	public static readonly LA1_REQUEST = "TypeScriptTestInterface.la1Request";
	public static readonly LA1_RESPONSE_SUCCESS = "TypeScriptTestInterface.la1ResponseSuccess";
	public static readonly LA1_RESPONSE_ERROR = "TypeScriptTestInterface.la1ResponseError";
	public static readonly LA2 = "TypeScriptTestInterface.la2";
	public static readonly LA2_REQUEST = "TypeScriptTestInterface.la2Request";
	public static readonly LA2_RESPONSE_SUCCESS = "TypeScriptTestInterface.la2ResponseSuccess";
	public static readonly LA2_RESPONSE_ERROR = "TypeScriptTestInterface.la2ResponseError";
	public static readonly LA3 = "TypeScriptTestInterface.la3";
	public static readonly LA3_REQUEST = "TypeScriptTestInterface.la3Request";
	public static readonly LA3_RESPONSE_SUCCESS = "TypeScriptTestInterface.la3ResponseSuccess";
	public static readonly LA3_RESPONSE_ERROR = "TypeScriptTestInterface.la3ResponseError";
	public static readonly LA4 = "TypeScriptTestInterface.la4";
	public static readonly LA4_REQUEST = "TypeScriptTestInterface.la4Request";
	public static readonly LA4_RESPONSE_SUCCESS = "TypeScriptTestInterface.la4ResponseSuccess";
	public static readonly LA4_RESPONSE_ERROR = "TypeScriptTestInterface.la4ResponseError";
	public static readonly LA5 = "TypeScriptTestInterface.la5";
	public static readonly LA5_REQUEST = "TypeScriptTestInterface.la5Request";
	public static readonly LA5_RESPONSE_SUCCESS = "TypeScriptTestInterface.la5ResponseSuccess";
	public static readonly LA5_RESPONSE_ERROR = "TypeScriptTestInterface.la5ResponseError";
	public static readonly LA6 = "TypeScriptTestInterface.la6";
	public static readonly LA6_REQUEST = "TypeScriptTestInterface.la6Request";
	public static readonly LA6_RESPONSE_SUCCESS = "TypeScriptTestInterface.la6ResponseSuccess";
	public static readonly LA6_RESPONSE_ERROR = "TypeScriptTestInterface.la6ResponseError";
	private contextPath : string = null;

//
	constructor(contextPath : string) {
		this.contextPath = contextPath;
	}

	public aObject(data : any) {
		this.a(data.callbackSuccess, data.callbackError, data.callbackAfter);
	}

	public aFunc = (callbackSuccess : (entity: EntityA) => void, callbackError : (error: any) => void = null, callbackAfter : () => void = null) : void => {
		this.a(callbackSuccess, callbackError, callbackAfter);
	}

	public a(callbackSuccess : (entity: EntityA) => void, callbackError : (error: any) => void = null, callbackAfter : () => void = null) : void {
		let sUrl : string = this.contextPath + "//?callTimestamp=" + (new Date()).getTime();
//
		let callCallbackAfter = () => {
			if (callbackAfter != null) {
				try {
					callbackAfter();
				} catch (_ex2) {
					console.error("Error while executing after handler in ajax method TypeScriptTestInterface.a " + _ex2, _ex2);
				}
			}
		};
		let ajaxParams : any = {
		    method: "GET",
		    url: sUrl,
		    contentType: "text/plain; charset=utf-8"
		};
		$(document).trigger(TypeScriptTestInterface.A_REQUEST, ajaxParams);
		$.ajax(ajaxParams).done((data : any, textStatus : string, jqXHR : any) => {
			let successParams : any = {ajax: this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status};
			$(document).trigger("basis.http.response", successParams);
			$(document).trigger(TypeScriptTestInterface.A_RESPONSE_SUCCESS, successParams);
			let a = EntityA.createEntityAFromPojo(data);
							let entity = a;
			try {
			   	callbackSuccess(entity);
			   } catch (_ex1) {
			   	console.error("Error while executing success handler in ajax method TypeScriptTestInterface.a " + _ex1, _ex1);
			   }
			   callCallbackAfter();
		}).fail((jqXHR : any, textStatus : string, errorThrown : any) => {
			let errorParams : any = {ajax: this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status};
			$(document).trigger("basis.http.response", errorParams);
			$(document).trigger(TypeScriptTestInterface.A_RESPONSE_ERROR, errorParams);
			   console.error("error in ajax method TypeScriptTestInterface.a " + textStatus + " - " + errorThrown);
			if(callbackError) {
			    try {
			    	callbackError(jqXHR);
			    } catch (_ex) {
			    	console.error("Error while executing error handler in ajax method TypeScriptTestInterface.a " + _ex, _ex);
			    }
			   }
			   callCallbackAfter();
		});
	}

	public aaObject(data : any) {
		this.aa(data.callbackSuccess, data.callbackError, data.callbackAfter);
	}

	public aaFunc = (callbackSuccess : (entity: EntityA[]) => void, callbackError : (error: any) => void = null, callbackAfter : () => void = null) : void => {
		this.aa(callbackSuccess, callbackError, callbackAfter);
	}

	public aa(callbackSuccess : (entity: EntityA[]) => void, callbackError : (error: any) => void = null, callbackAfter : () => void = null) : void {
		let sUrl : string = this.contextPath + "//?callTimestamp=" + (new Date()).getTime();
//
		let callCallbackAfter = () => {
			if (callbackAfter != null) {
				try {
					callbackAfter();
				} catch (_ex2) {
					console.error("Error while executing after handler in ajax method TypeScriptTestInterface.aa " + _ex2, _ex2);
				}
			}
		};
		let ajaxParams : any = {
		    method: "GET",
		    url: sUrl,
		    contentType: "text/plain; charset=utf-8"
		};
		$(document).trigger(TypeScriptTestInterface.AA_REQUEST, ajaxParams);
		$.ajax(ajaxParams).done((data : any, textStatus : string, jqXHR : any) => {
			let successParams : any = {ajax: this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status};
			$(document).trigger("basis.http.response", successParams);
			$(document).trigger(TypeScriptTestInterface.AA_RESPONSE_SUCCESS, successParams);
			let 	aa = data ? data.map(pojo1 => EntityA.createEntityAFromPojo(pojo1)) : []
							let entity = aa;
			try {
			   	callbackSuccess(entity);
			   } catch (_ex1) {
			   	console.error("Error while executing success handler in ajax method TypeScriptTestInterface.aa " + _ex1, _ex1);
			   }
			   callCallbackAfter();
		}).fail((jqXHR : any, textStatus : string, errorThrown : any) => {
			let errorParams : any = {ajax: this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status};
			$(document).trigger("basis.http.response", errorParams);
			$(document).trigger(TypeScriptTestInterface.AA_RESPONSE_ERROR, errorParams);
			   console.error("error in ajax method TypeScriptTestInterface.aa " + textStatus + " - " + errorThrown);
			if(callbackError) {
			    try {
			    	callbackError(jqXHR);
			    } catch (_ex) {
			    	console.error("Error while executing error handler in ajax method TypeScriptTestInterface.aa " + _ex, _ex);
			    }
			   }
			   callCallbackAfter();
		});
	}

	public aaaObject(data : any) {
		this.aaa(data.callbackSuccess, data.callbackError, data.callbackAfter);
	}

	public aaaFunc = (callbackSuccess : (entity: EntityA[][]) => void, callbackError : (error: any) => void = null, callbackAfter : () => void = null) : void => {
		this.aaa(callbackSuccess, callbackError, callbackAfter);
	}

	public aaa(callbackSuccess : (entity: EntityA[][]) => void, callbackError : (error: any) => void = null, callbackAfter : () => void = null) : void {
		let sUrl : string = this.contextPath + "//?callTimestamp=" + (new Date()).getTime();
//
		let callCallbackAfter = () => {
			if (callbackAfter != null) {
				try {
					callbackAfter();
				} catch (_ex2) {
					console.error("Error while executing after handler in ajax method TypeScriptTestInterface.aaa " + _ex2, _ex2);
				}
			}
		};
		let ajaxParams : any = {
		    method: "GET",
		    url: sUrl,
		    contentType: "text/plain; charset=utf-8"
		};
		$(document).trigger(TypeScriptTestInterface.AAA_REQUEST, ajaxParams);
		$.ajax(ajaxParams).done((data : any, textStatus : string, jqXHR : any) => {
			let successParams : any = {ajax: this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status};
			$(document).trigger("basis.http.response", successParams);
			$(document).trigger(TypeScriptTestInterface.AAA_RESPONSE_SUCCESS, successParams);
			let 	aaa = data ? data.map(pojo1 => 		pojo1 = pojo1 ? pojo1.map(pojo2 => EntityA.createEntityAFromPojo(pojo2)) : []) : []
							let entity = aaa;
			try {
			   	callbackSuccess(entity);
			   } catch (_ex1) {
			   	console.error("Error while executing success handler in ajax method TypeScriptTestInterface.aaa " + _ex1, _ex1);
			   }
			   callCallbackAfter();
		}).fail((jqXHR : any, textStatus : string, errorThrown : any) => {
			let errorParams : any = {ajax: this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status};
			$(document).trigger("basis.http.response", errorParams);
			$(document).trigger(TypeScriptTestInterface.AAA_RESPONSE_ERROR, errorParams);
			   console.error("error in ajax method TypeScriptTestInterface.aaa " + textStatus + " - " + errorThrown);
			if(callbackError) {
			    try {
			    	callbackError(jqXHR);
			    } catch (_ex) {
			    	console.error("Error while executing error handler in ajax method TypeScriptTestInterface.aaa " + _ex, _ex);
			    }
			   }
			   callCallbackAfter();
		});
	}

	public la1Object(data : any) {
		this.la1(data.callbackSuccess, data.callbackError, data.callbackAfter);
	}

	public la1Func = (callbackSuccess : (entity: EntityA[]) => void, callbackError : (error: any) => void = null, callbackAfter : () => void = null) : void => {
		this.la1(callbackSuccess, callbackError, callbackAfter);
	}

	public la1(callbackSuccess : (entity: EntityA[]) => void, callbackError : (error: any) => void = null, callbackAfter : () => void = null) : void {
		let sUrl : string = this.contextPath + "//?callTimestamp=" + (new Date()).getTime();
//
		let callCallbackAfter = () => {
			if (callbackAfter != null) {
				try {
					callbackAfter();
				} catch (_ex2) {
					console.error("Error while executing after handler in ajax method TypeScriptTestInterface.la1 " + _ex2, _ex2);
				}
			}
		};
		let ajaxParams : any = {
		    method: "GET",
		    url: sUrl,
		    contentType: "text/plain; charset=utf-8"
		};
		$(document).trigger(TypeScriptTestInterface.LA1_REQUEST, ajaxParams);
		$.ajax(ajaxParams).done((data : any, textStatus : string, jqXHR : any) => {
			let successParams : any = {ajax: this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status};
			$(document).trigger("basis.http.response", successParams);
			$(document).trigger(TypeScriptTestInterface.LA1_RESPONSE_SUCCESS, successParams);
			let 	la1 = data ? data.map(pojo1 => EntityA.createEntityAFromPojo(pojo1)) : []
							let entity = la1;
			try {
			   	callbackSuccess(entity);
			   } catch (_ex1) {
			   	console.error("Error while executing success handler in ajax method TypeScriptTestInterface.la1 " + _ex1, _ex1);
			   }
			   callCallbackAfter();
		}).fail((jqXHR : any, textStatus : string, errorThrown : any) => {
			let errorParams : any = {ajax: this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status};
			$(document).trigger("basis.http.response", errorParams);
			$(document).trigger(TypeScriptTestInterface.LA1_RESPONSE_ERROR, errorParams);
			   console.error("error in ajax method TypeScriptTestInterface.la1 " + textStatus + " - " + errorThrown);
			if(callbackError) {
			    try {
			    	callbackError(jqXHR);
			    } catch (_ex) {
			    	console.error("Error while executing error handler in ajax method TypeScriptTestInterface.la1 " + _ex, _ex);
			    }
			   }
			   callCallbackAfter();
		});
	}

	public la2Object(data : any) {
		this.la2(data.callbackSuccess, data.callbackError, data.callbackAfter);
	}

	public la2Func = (callbackSuccess : (entity: EntityA[]) => void, callbackError : (error: any) => void = null, callbackAfter : () => void = null) : void => {
		this.la2(callbackSuccess, callbackError, callbackAfter);
	}

	public la2(callbackSuccess : (entity: EntityA[]) => void, callbackError : (error: any) => void = null, callbackAfter : () => void = null) : void {
		let sUrl : string = this.contextPath + "//?callTimestamp=" + (new Date()).getTime();
//
		let callCallbackAfter = () => {
			if (callbackAfter != null) {
				try {
					callbackAfter();
				} catch (_ex2) {
					console.error("Error while executing after handler in ajax method TypeScriptTestInterface.la2 " + _ex2, _ex2);
				}
			}
		};
		let ajaxParams : any = {
		    method: "GET",
		    url: sUrl,
		    contentType: "text/plain; charset=utf-8"
		};
		$(document).trigger(TypeScriptTestInterface.LA2_REQUEST, ajaxParams);
		$.ajax(ajaxParams).done((data : any, textStatus : string, jqXHR : any) => {
			let successParams : any = {ajax: this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status};
			$(document).trigger("basis.http.response", successParams);
			$(document).trigger(TypeScriptTestInterface.LA2_RESPONSE_SUCCESS, successParams);
			let 	la2 = data ? data.map(pojo1 => EntityA.createEntityAFromPojo(pojo1)) : []
							let entity = la2;
			try {
			   	callbackSuccess(entity);
			   } catch (_ex1) {
			   	console.error("Error while executing success handler in ajax method TypeScriptTestInterface.la2 " + _ex1, _ex1);
			   }
			   callCallbackAfter();
		}).fail((jqXHR : any, textStatus : string, errorThrown : any) => {
			let errorParams : any = {ajax: this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status};
			$(document).trigger("basis.http.response", errorParams);
			$(document).trigger(TypeScriptTestInterface.LA2_RESPONSE_ERROR, errorParams);
			   console.error("error in ajax method TypeScriptTestInterface.la2 " + textStatus + " - " + errorThrown);
			if(callbackError) {
			    try {
			    	callbackError(jqXHR);
			    } catch (_ex) {
			    	console.error("Error while executing error handler in ajax method TypeScriptTestInterface.la2 " + _ex, _ex);
			    }
			   }
			   callCallbackAfter();
		});
	}

	public la3Object(data : any) {
		this.la3(data.callbackSuccess, data.callbackError, data.callbackAfter);
	}

	public la3Func = (callbackSuccess : (entity: EntityA[]) => void, callbackError : (error: any) => void = null, callbackAfter : () => void = null) : void => {
		this.la3(callbackSuccess, callbackError, callbackAfter);
	}

	public la3(callbackSuccess : (entity: EntityA[]) => void, callbackError : (error: any) => void = null, callbackAfter : () => void = null) : void {
		let sUrl : string = this.contextPath + "//?callTimestamp=" + (new Date()).getTime();
//
		let callCallbackAfter = () => {
			if (callbackAfter != null) {
				try {
					callbackAfter();
				} catch (_ex2) {
					console.error("Error while executing after handler in ajax method TypeScriptTestInterface.la3 " + _ex2, _ex2);
				}
			}
		};
		let ajaxParams : any = {
		    method: "GET",
		    url: sUrl,
		    contentType: "text/plain; charset=utf-8"
		};
		$(document).trigger(TypeScriptTestInterface.LA3_REQUEST, ajaxParams);
		$.ajax(ajaxParams).done((data : any, textStatus : string, jqXHR : any) => {
			let successParams : any = {ajax: this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status};
			$(document).trigger("basis.http.response", successParams);
			$(document).trigger(TypeScriptTestInterface.LA3_RESPONSE_SUCCESS, successParams);
			let 	la3 = data ? data.map(pojo1 => EntityA.createEntityAFromPojo(pojo1)) : []
							let entity = la3;
			try {
			   	callbackSuccess(entity);
			   } catch (_ex1) {
			   	console.error("Error while executing success handler in ajax method TypeScriptTestInterface.la3 " + _ex1, _ex1);
			   }
			   callCallbackAfter();
		}).fail((jqXHR : any, textStatus : string, errorThrown : any) => {
			let errorParams : any = {ajax: this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status};
			$(document).trigger("basis.http.response", errorParams);
			$(document).trigger(TypeScriptTestInterface.LA3_RESPONSE_ERROR, errorParams);
			   console.error("error in ajax method TypeScriptTestInterface.la3 " + textStatus + " - " + errorThrown);
			if(callbackError) {
			    try {
			    	callbackError(jqXHR);
			    } catch (_ex) {
			    	console.error("Error while executing error handler in ajax method TypeScriptTestInterface.la3 " + _ex, _ex);
			    }
			   }
			   callCallbackAfter();
		});
	}

	public la4Object(data : any) {
		this.la4(data.callbackSuccess, data.callbackError, data.callbackAfter);
	}

	public la4Func = (callbackSuccess : (entity: EntityA[][]) => void, callbackError : (error: any) => void = null, callbackAfter : () => void = null) : void => {
		this.la4(callbackSuccess, callbackError, callbackAfter);
	}

	public la4(callbackSuccess : (entity: EntityA[][]) => void, callbackError : (error: any) => void = null, callbackAfter : () => void = null) : void {
		let sUrl : string = this.contextPath + "//?callTimestamp=" + (new Date()).getTime();
//
		let callCallbackAfter = () => {
			if (callbackAfter != null) {
				try {
					callbackAfter();
				} catch (_ex2) {
					console.error("Error while executing after handler in ajax method TypeScriptTestInterface.la4 " + _ex2, _ex2);
				}
			}
		};
		let ajaxParams : any = {
		    method: "GET",
		    url: sUrl,
		    contentType: "text/plain; charset=utf-8"
		};
		$(document).trigger(TypeScriptTestInterface.LA4_REQUEST, ajaxParams);
		$.ajax(ajaxParams).done((data : any, textStatus : string, jqXHR : any) => {
			let successParams : any = {ajax: this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status};
			$(document).trigger("basis.http.response", successParams);
			$(document).trigger(TypeScriptTestInterface.LA4_RESPONSE_SUCCESS, successParams);
			let 	la4 = data ? data.map(pojo1 => 		pojo1 = pojo1 ? pojo1.map(pojo2 => EntityA.createEntityAFromPojo(pojo2)) : []) : []
							let entity = la4;
			try {
			   	callbackSuccess(entity);
			   } catch (_ex1) {
			   	console.error("Error while executing success handler in ajax method TypeScriptTestInterface.la4 " + _ex1, _ex1);
			   }
			   callCallbackAfter();
		}).fail((jqXHR : any, textStatus : string, errorThrown : any) => {
			let errorParams : any = {ajax: this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status};
			$(document).trigger("basis.http.response", errorParams);
			$(document).trigger(TypeScriptTestInterface.LA4_RESPONSE_ERROR, errorParams);
			   console.error("error in ajax method TypeScriptTestInterface.la4 " + textStatus + " - " + errorThrown);
			if(callbackError) {
			    try {
			    	callbackError(jqXHR);
			    } catch (_ex) {
			    	console.error("Error while executing error handler in ajax method TypeScriptTestInterface.la4 " + _ex, _ex);
			    }
			   }
			   callCallbackAfter();
		});
	}

	public la5Object(data : any) {
		this.la5(data.callbackSuccess, data.callbackError, data.callbackAfter);
	}

	public la5Func = (callbackSuccess : (entity: EntityA[][]) => void, callbackError : (error: any) => void = null, callbackAfter : () => void = null) : void => {
		this.la5(callbackSuccess, callbackError, callbackAfter);
	}

	public la5(callbackSuccess : (entity: EntityA[][]) => void, callbackError : (error: any) => void = null, callbackAfter : () => void = null) : void {
		let sUrl : string = this.contextPath + "//?callTimestamp=" + (new Date()).getTime();
//
		let callCallbackAfter = () => {
			if (callbackAfter != null) {
				try {
					callbackAfter();
				} catch (_ex2) {
					console.error("Error while executing after handler in ajax method TypeScriptTestInterface.la5 " + _ex2, _ex2);
				}
			}
		};
		let ajaxParams : any = {
		    method: "GET",
		    url: sUrl,
		    contentType: "text/plain; charset=utf-8"
		};
		$(document).trigger(TypeScriptTestInterface.LA5_REQUEST, ajaxParams);
		$.ajax(ajaxParams).done((data : any, textStatus : string, jqXHR : any) => {
			let successParams : any = {ajax: this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status};
			$(document).trigger("basis.http.response", successParams);
			$(document).trigger(TypeScriptTestInterface.LA5_RESPONSE_SUCCESS, successParams);
			let 	la5 = data ? data.map(pojo1 => 		pojo1 = pojo1 ? pojo1.map(pojo2 => EntityA.createEntityAFromPojo(pojo2)) : []) : []
							let entity = la5;
			try {
			   	callbackSuccess(entity);
			   } catch (_ex1) {
			   	console.error("Error while executing success handler in ajax method TypeScriptTestInterface.la5 " + _ex1, _ex1);
			   }
			   callCallbackAfter();
		}).fail((jqXHR : any, textStatus : string, errorThrown : any) => {
			let errorParams : any = {ajax: this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status};
			$(document).trigger("basis.http.response", errorParams);
			$(document).trigger(TypeScriptTestInterface.LA5_RESPONSE_ERROR, errorParams);
			   console.error("error in ajax method TypeScriptTestInterface.la5 " + textStatus + " - " + errorThrown);
			if(callbackError) {
			    try {
			    	callbackError(jqXHR);
			    } catch (_ex) {
			    	console.error("Error while executing error handler in ajax method TypeScriptTestInterface.la5 " + _ex, _ex);
			    }
			   }
			   callCallbackAfter();
		});
	}

	public la6Object(data : any) {
		this.la6(data.aas, data.callbackSuccess, data.callbackError, data.callbackAfter);
	}

	public la6Func = (aas : EntityA[], callbackSuccess : (entity: EntityA[][]) => void, callbackError : (error: any) => void = null, callbackAfter : () => void = null) : void => {
		this.la6(aas, callbackSuccess, callbackError, callbackAfter);
	}

	public la6(aas : EntityA[], callbackSuccess : (entity: EntityA[][]) => void, callbackError : (error: any) => void = null, callbackAfter : () => void = null) : void {
		let sUrl : string = this.contextPath + "//?callTimestamp=" + (new Date()).getTime();
//
		let callCallbackAfter = () => {
			if (callbackAfter != null) {
				try {
					callbackAfter();
				} catch (_ex2) {
					console.error("Error while executing after handler in ajax method TypeScriptTestInterface.la6 " + _ex2, _ex2);
				}
			}
		};
		let ajaxParams : any = {
		    method: "GET",
		    url: sUrl,
		    contentType: "text/plain; charset=utf-8"
		};
		$(document).trigger(TypeScriptTestInterface.LA6_REQUEST, ajaxParams);
		$.ajax(ajaxParams).done((data : any, textStatus : string, jqXHR : any) => {
			let successParams : any = {ajax: this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status};
			$(document).trigger("basis.http.response", successParams);
			$(document).trigger(TypeScriptTestInterface.LA6_RESPONSE_SUCCESS, successParams);
			let 	la6 = data ? data.map(pojo1 => 		pojo1 = pojo1 ? pojo1.map(pojo2 => EntityA.createEntityAFromPojo(pojo2)) : []) : []
							let entity = la6;
			try {
			   	callbackSuccess(entity);
			   } catch (_ex1) {
			   	console.error("Error while executing success handler in ajax method TypeScriptTestInterface.la6 " + _ex1, _ex1);
			   }
			   callCallbackAfter();
		}).fail((jqXHR : any, textStatus : string, errorThrown : any) => {
			let errorParams : any = {ajax: this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status};
			$(document).trigger("basis.http.response", errorParams);
			$(document).trigger(TypeScriptTestInterface.LA6_RESPONSE_ERROR, errorParams);
			   console.error("error in ajax method TypeScriptTestInterface.la6 " + textStatus + " - " + errorThrown);
			if(callbackError) {
			    try {
			    	callbackError(jqXHR);
			    } catch (_ex) {
			    	console.error("Error while executing error handler in ajax method TypeScriptTestInterface.la6 " + _ex, _ex);
			    }
			   }
			   callCallbackAfter();
		});
	}
}
