"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var entities_1 = require("./entities");
var TypeScriptTestInterface = (function () {
    function TypeScriptTestInterface(contextPath) {
        var _this = this;
        this.contextPath = null;
        this.aFunc = function (callbackSuccess, callbackError, callbackAfter) {
            if (callbackError === void 0) { callbackError = null; }
            if (callbackAfter === void 0) { callbackAfter = null; }
            _this.a(callbackSuccess, callbackError, callbackAfter);
        };
        this.aaFunc = function (callbackSuccess, callbackError, callbackAfter) {
            if (callbackError === void 0) { callbackError = null; }
            if (callbackAfter === void 0) { callbackAfter = null; }
            _this.aa(callbackSuccess, callbackError, callbackAfter);
        };
        this.aaaFunc = function (callbackSuccess, callbackError, callbackAfter) {
            if (callbackError === void 0) { callbackError = null; }
            if (callbackAfter === void 0) { callbackAfter = null; }
            _this.aaa(callbackSuccess, callbackError, callbackAfter);
        };
        this.la1Func = function (callbackSuccess, callbackError, callbackAfter) {
            if (callbackError === void 0) { callbackError = null; }
            if (callbackAfter === void 0) { callbackAfter = null; }
            _this.la1(callbackSuccess, callbackError, callbackAfter);
        };
        this.la2Func = function (callbackSuccess, callbackError, callbackAfter) {
            if (callbackError === void 0) { callbackError = null; }
            if (callbackAfter === void 0) { callbackAfter = null; }
            _this.la2(callbackSuccess, callbackError, callbackAfter);
        };
        this.la3Func = function (callbackSuccess, callbackError, callbackAfter) {
            if (callbackError === void 0) { callbackError = null; }
            if (callbackAfter === void 0) { callbackAfter = null; }
            _this.la3(callbackSuccess, callbackError, callbackAfter);
        };
        this.la4Func = function (callbackSuccess, callbackError, callbackAfter) {
            if (callbackError === void 0) { callbackError = null; }
            if (callbackAfter === void 0) { callbackAfter = null; }
            _this.la4(callbackSuccess, callbackError, callbackAfter);
        };
        this.la5Func = function (callbackSuccess, callbackError, callbackAfter) {
            if (callbackError === void 0) { callbackError = null; }
            if (callbackAfter === void 0) { callbackAfter = null; }
            _this.la5(callbackSuccess, callbackError, callbackAfter);
        };
        this.la6Func = function (aas, callbackSuccess, callbackError, callbackAfter) {
            if (callbackError === void 0) { callbackError = null; }
            if (callbackAfter === void 0) { callbackAfter = null; }
            _this.la6(aas, callbackSuccess, callbackError, callbackAfter);
        };
        this.contextPath = contextPath;
    }
    TypeScriptTestInterface.prototype.aObject = function (data) {
        this.a(data.callbackSuccess, data.callbackError, data.callbackAfter);
    };
    TypeScriptTestInterface.prototype.a = function (callbackSuccess, callbackError, callbackAfter) {
        var _this = this;
        if (callbackError === void 0) { callbackError = null; }
        if (callbackAfter === void 0) { callbackAfter = null; }
        var sUrl = this.contextPath + "//?callTimestamp=" + (new Date()).getTime();
        var callCallbackAfter = function () {
            if (callbackAfter != null) {
                try {
                    callbackAfter();
                }
                catch (_ex2) {
                    console.error("Error while executing after handler in ajax method TypeScriptTestInterface.a " + _ex2, _ex2);
                }
            }
        };
        var ajaxParams = {
            method: "GET",
            url: sUrl,
            contentType: "text/plain; charset=utf-8"
        };
        $(document).trigger(TypeScriptTestInterface.A_REQUEST, ajaxParams);
        $.ajax(ajaxParams).done(function (data, textStatus, jqXHR) {
            var successParams = { ajax: _this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status };
            $(document).trigger("basis.http.response", successParams);
            $(document).trigger(TypeScriptTestInterface.A_RESPONSE_SUCCESS, successParams);
            var a = entities_1.EntityA.createEntityAFromPojo(data);
            var entity = a;
            try {
                callbackSuccess(entity);
            }
            catch (_ex1) {
                console.error("Error while executing success handler in ajax method TypeScriptTestInterface.a " + _ex1, _ex1);
            }
            callCallbackAfter();
        }).fail(function (jqXHR, textStatus, errorThrown) {
            var errorParams = { ajax: _this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status };
            $(document).trigger("basis.http.response", errorParams);
            $(document).trigger(TypeScriptTestInterface.A_RESPONSE_ERROR, errorParams);
            console.error("error in ajax method TypeScriptTestInterface.a " + textStatus + " - " + errorThrown);
            if (callbackError) {
                try {
                    callbackError(jqXHR);
                }
                catch (_ex) {
                    console.error("Error while executing error handler in ajax method TypeScriptTestInterface.a " + _ex, _ex);
                }
            }
            callCallbackAfter();
        });
    };
    TypeScriptTestInterface.prototype.aaObject = function (data) {
        this.aa(data.callbackSuccess, data.callbackError, data.callbackAfter);
    };
    TypeScriptTestInterface.prototype.aa = function (callbackSuccess, callbackError, callbackAfter) {
        var _this = this;
        if (callbackError === void 0) { callbackError = null; }
        if (callbackAfter === void 0) { callbackAfter = null; }
        var sUrl = this.contextPath + "//?callTimestamp=" + (new Date()).getTime();
        var callCallbackAfter = function () {
            if (callbackAfter != null) {
                try {
                    callbackAfter();
                }
                catch (_ex2) {
                    console.error("Error while executing after handler in ajax method TypeScriptTestInterface.aa " + _ex2, _ex2);
                }
            }
        };
        var ajaxParams = {
            method: "GET",
            url: sUrl,
            contentType: "text/plain; charset=utf-8"
        };
        $(document).trigger(TypeScriptTestInterface.AA_REQUEST, ajaxParams);
        $.ajax(ajaxParams).done(function (data, textStatus, jqXHR) {
            var successParams = { ajax: _this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status };
            $(document).trigger("basis.http.response", successParams);
            $(document).trigger(TypeScriptTestInterface.AA_RESPONSE_SUCCESS, successParams);
            var aa = data ? data.map(function (pojo1) { return entities_1.EntityA.createEntityAFromPojo(pojo1); }) : [];
            var entity = aa;
            try {
                callbackSuccess(entity);
            }
            catch (_ex1) {
                console.error("Error while executing success handler in ajax method TypeScriptTestInterface.aa " + _ex1, _ex1);
            }
            callCallbackAfter();
        }).fail(function (jqXHR, textStatus, errorThrown) {
            var errorParams = { ajax: _this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status };
            $(document).trigger("basis.http.response", errorParams);
            $(document).trigger(TypeScriptTestInterface.AA_RESPONSE_ERROR, errorParams);
            console.error("error in ajax method TypeScriptTestInterface.aa " + textStatus + " - " + errorThrown);
            if (callbackError) {
                try {
                    callbackError(jqXHR);
                }
                catch (_ex) {
                    console.error("Error while executing error handler in ajax method TypeScriptTestInterface.aa " + _ex, _ex);
                }
            }
            callCallbackAfter();
        });
    };
    TypeScriptTestInterface.prototype.aaaObject = function (data) {
        this.aaa(data.callbackSuccess, data.callbackError, data.callbackAfter);
    };
    TypeScriptTestInterface.prototype.aaa = function (callbackSuccess, callbackError, callbackAfter) {
        var _this = this;
        if (callbackError === void 0) { callbackError = null; }
        if (callbackAfter === void 0) { callbackAfter = null; }
        var sUrl = this.contextPath + "//?callTimestamp=" + (new Date()).getTime();
        var callCallbackAfter = function () {
            if (callbackAfter != null) {
                try {
                    callbackAfter();
                }
                catch (_ex2) {
                    console.error("Error while executing after handler in ajax method TypeScriptTestInterface.aaa " + _ex2, _ex2);
                }
            }
        };
        var ajaxParams = {
            method: "GET",
            url: sUrl,
            contentType: "text/plain; charset=utf-8"
        };
        $(document).trigger(TypeScriptTestInterface.AAA_REQUEST, ajaxParams);
        $.ajax(ajaxParams).done(function (data, textStatus, jqXHR) {
            var successParams = { ajax: _this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status };
            $(document).trigger("basis.http.response", successParams);
            $(document).trigger(TypeScriptTestInterface.AAA_RESPONSE_SUCCESS, successParams);
            var aaa = data ? data.map(function (pojo1) { return pojo1 = pojo1 ? pojo1.map(function (pojo2) { return entities_1.EntityA.createEntityAFromPojo(pojo2); }) : []; }) : [];
            var entity = aaa;
            try {
                callbackSuccess(entity);
            }
            catch (_ex1) {
                console.error("Error while executing success handler in ajax method TypeScriptTestInterface.aaa " + _ex1, _ex1);
            }
            callCallbackAfter();
        }).fail(function (jqXHR, textStatus, errorThrown) {
            var errorParams = { ajax: _this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status };
            $(document).trigger("basis.http.response", errorParams);
            $(document).trigger(TypeScriptTestInterface.AAA_RESPONSE_ERROR, errorParams);
            console.error("error in ajax method TypeScriptTestInterface.aaa " + textStatus + " - " + errorThrown);
            if (callbackError) {
                try {
                    callbackError(jqXHR);
                }
                catch (_ex) {
                    console.error("Error while executing error handler in ajax method TypeScriptTestInterface.aaa " + _ex, _ex);
                }
            }
            callCallbackAfter();
        });
    };
    TypeScriptTestInterface.prototype.la1Object = function (data) {
        this.la1(data.callbackSuccess, data.callbackError, data.callbackAfter);
    };
    TypeScriptTestInterface.prototype.la1 = function (callbackSuccess, callbackError, callbackAfter) {
        var _this = this;
        if (callbackError === void 0) { callbackError = null; }
        if (callbackAfter === void 0) { callbackAfter = null; }
        var sUrl = this.contextPath + "//?callTimestamp=" + (new Date()).getTime();
        var callCallbackAfter = function () {
            if (callbackAfter != null) {
                try {
                    callbackAfter();
                }
                catch (_ex2) {
                    console.error("Error while executing after handler in ajax method TypeScriptTestInterface.la1 " + _ex2, _ex2);
                }
            }
        };
        var ajaxParams = {
            method: "GET",
            url: sUrl,
            contentType: "text/plain; charset=utf-8"
        };
        $(document).trigger(TypeScriptTestInterface.LA1_REQUEST, ajaxParams);
        $.ajax(ajaxParams).done(function (data, textStatus, jqXHR) {
            var successParams = { ajax: _this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status };
            $(document).trigger("basis.http.response", successParams);
            $(document).trigger(TypeScriptTestInterface.LA1_RESPONSE_SUCCESS, successParams);
            var la1 = data ? data.map(function (pojo1) { return entities_1.EntityA.createEntityAFromPojo(pojo1); }) : [];
            var entity = la1;
            try {
                callbackSuccess(entity);
            }
            catch (_ex1) {
                console.error("Error while executing success handler in ajax method TypeScriptTestInterface.la1 " + _ex1, _ex1);
            }
            callCallbackAfter();
        }).fail(function (jqXHR, textStatus, errorThrown) {
            var errorParams = { ajax: _this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status };
            $(document).trigger("basis.http.response", errorParams);
            $(document).trigger(TypeScriptTestInterface.LA1_RESPONSE_ERROR, errorParams);
            console.error("error in ajax method TypeScriptTestInterface.la1 " + textStatus + " - " + errorThrown);
            if (callbackError) {
                try {
                    callbackError(jqXHR);
                }
                catch (_ex) {
                    console.error("Error while executing error handler in ajax method TypeScriptTestInterface.la1 " + _ex, _ex);
                }
            }
            callCallbackAfter();
        });
    };
    TypeScriptTestInterface.prototype.la2Object = function (data) {
        this.la2(data.callbackSuccess, data.callbackError, data.callbackAfter);
    };
    TypeScriptTestInterface.prototype.la2 = function (callbackSuccess, callbackError, callbackAfter) {
        var _this = this;
        if (callbackError === void 0) { callbackError = null; }
        if (callbackAfter === void 0) { callbackAfter = null; }
        var sUrl = this.contextPath + "//?callTimestamp=" + (new Date()).getTime();
        var callCallbackAfter = function () {
            if (callbackAfter != null) {
                try {
                    callbackAfter();
                }
                catch (_ex2) {
                    console.error("Error while executing after handler in ajax method TypeScriptTestInterface.la2 " + _ex2, _ex2);
                }
            }
        };
        var ajaxParams = {
            method: "GET",
            url: sUrl,
            contentType: "text/plain; charset=utf-8"
        };
        $(document).trigger(TypeScriptTestInterface.LA2_REQUEST, ajaxParams);
        $.ajax(ajaxParams).done(function (data, textStatus, jqXHR) {
            var successParams = { ajax: _this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status };
            $(document).trigger("basis.http.response", successParams);
            $(document).trigger(TypeScriptTestInterface.LA2_RESPONSE_SUCCESS, successParams);
            var la2 = data ? data.map(function (pojo1) { return entities_1.EntityA.createEntityAFromPojo(pojo1); }) : [];
            var entity = la2;
            try {
                callbackSuccess(entity);
            }
            catch (_ex1) {
                console.error("Error while executing success handler in ajax method TypeScriptTestInterface.la2 " + _ex1, _ex1);
            }
            callCallbackAfter();
        }).fail(function (jqXHR, textStatus, errorThrown) {
            var errorParams = { ajax: _this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status };
            $(document).trigger("basis.http.response", errorParams);
            $(document).trigger(TypeScriptTestInterface.LA2_RESPONSE_ERROR, errorParams);
            console.error("error in ajax method TypeScriptTestInterface.la2 " + textStatus + " - " + errorThrown);
            if (callbackError) {
                try {
                    callbackError(jqXHR);
                }
                catch (_ex) {
                    console.error("Error while executing error handler in ajax method TypeScriptTestInterface.la2 " + _ex, _ex);
                }
            }
            callCallbackAfter();
        });
    };
    TypeScriptTestInterface.prototype.la3Object = function (data) {
        this.la3(data.callbackSuccess, data.callbackError, data.callbackAfter);
    };
    TypeScriptTestInterface.prototype.la3 = function (callbackSuccess, callbackError, callbackAfter) {
        var _this = this;
        if (callbackError === void 0) { callbackError = null; }
        if (callbackAfter === void 0) { callbackAfter = null; }
        var sUrl = this.contextPath + "//?callTimestamp=" + (new Date()).getTime();
        var callCallbackAfter = function () {
            if (callbackAfter != null) {
                try {
                    callbackAfter();
                }
                catch (_ex2) {
                    console.error("Error while executing after handler in ajax method TypeScriptTestInterface.la3 " + _ex2, _ex2);
                }
            }
        };
        var ajaxParams = {
            method: "GET",
            url: sUrl,
            contentType: "text/plain; charset=utf-8"
        };
        $(document).trigger(TypeScriptTestInterface.LA3_REQUEST, ajaxParams);
        $.ajax(ajaxParams).done(function (data, textStatus, jqXHR) {
            var successParams = { ajax: _this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status };
            $(document).trigger("basis.http.response", successParams);
            $(document).trigger(TypeScriptTestInterface.LA3_RESPONSE_SUCCESS, successParams);
            var la3 = data ? data.map(function (pojo1) { return entities_1.EntityA.createEntityAFromPojo(pojo1); }) : [];
            var entity = la3;
            try {
                callbackSuccess(entity);
            }
            catch (_ex1) {
                console.error("Error while executing success handler in ajax method TypeScriptTestInterface.la3 " + _ex1, _ex1);
            }
            callCallbackAfter();
        }).fail(function (jqXHR, textStatus, errorThrown) {
            var errorParams = { ajax: _this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status };
            $(document).trigger("basis.http.response", errorParams);
            $(document).trigger(TypeScriptTestInterface.LA3_RESPONSE_ERROR, errorParams);
            console.error("error in ajax method TypeScriptTestInterface.la3 " + textStatus + " - " + errorThrown);
            if (callbackError) {
                try {
                    callbackError(jqXHR);
                }
                catch (_ex) {
                    console.error("Error while executing error handler in ajax method TypeScriptTestInterface.la3 " + _ex, _ex);
                }
            }
            callCallbackAfter();
        });
    };
    TypeScriptTestInterface.prototype.la4Object = function (data) {
        this.la4(data.callbackSuccess, data.callbackError, data.callbackAfter);
    };
    TypeScriptTestInterface.prototype.la4 = function (callbackSuccess, callbackError, callbackAfter) {
        var _this = this;
        if (callbackError === void 0) { callbackError = null; }
        if (callbackAfter === void 0) { callbackAfter = null; }
        var sUrl = this.contextPath + "//?callTimestamp=" + (new Date()).getTime();
        var callCallbackAfter = function () {
            if (callbackAfter != null) {
                try {
                    callbackAfter();
                }
                catch (_ex2) {
                    console.error("Error while executing after handler in ajax method TypeScriptTestInterface.la4 " + _ex2, _ex2);
                }
            }
        };
        var ajaxParams = {
            method: "GET",
            url: sUrl,
            contentType: "text/plain; charset=utf-8"
        };
        $(document).trigger(TypeScriptTestInterface.LA4_REQUEST, ajaxParams);
        $.ajax(ajaxParams).done(function (data, textStatus, jqXHR) {
            var successParams = { ajax: _this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status };
            $(document).trigger("basis.http.response", successParams);
            $(document).trigger(TypeScriptTestInterface.LA4_RESPONSE_SUCCESS, successParams);
            var la4 = data ? data.map(function (pojo1) { return pojo1 = pojo1 ? pojo1.map(function (pojo2) { return entities_1.EntityA.createEntityAFromPojo(pojo2); }) : []; }) : [];
            var entity = la4;
            try {
                callbackSuccess(entity);
            }
            catch (_ex1) {
                console.error("Error while executing success handler in ajax method TypeScriptTestInterface.la4 " + _ex1, _ex1);
            }
            callCallbackAfter();
        }).fail(function (jqXHR, textStatus, errorThrown) {
            var errorParams = { ajax: _this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status };
            $(document).trigger("basis.http.response", errorParams);
            $(document).trigger(TypeScriptTestInterface.LA4_RESPONSE_ERROR, errorParams);
            console.error("error in ajax method TypeScriptTestInterface.la4 " + textStatus + " - " + errorThrown);
            if (callbackError) {
                try {
                    callbackError(jqXHR);
                }
                catch (_ex) {
                    console.error("Error while executing error handler in ajax method TypeScriptTestInterface.la4 " + _ex, _ex);
                }
            }
            callCallbackAfter();
        });
    };
    TypeScriptTestInterface.prototype.la5Object = function (data) {
        this.la5(data.callbackSuccess, data.callbackError, data.callbackAfter);
    };
    TypeScriptTestInterface.prototype.la5 = function (callbackSuccess, callbackError, callbackAfter) {
        var _this = this;
        if (callbackError === void 0) { callbackError = null; }
        if (callbackAfter === void 0) { callbackAfter = null; }
        var sUrl = this.contextPath + "//?callTimestamp=" + (new Date()).getTime();
        var callCallbackAfter = function () {
            if (callbackAfter != null) {
                try {
                    callbackAfter();
                }
                catch (_ex2) {
                    console.error("Error while executing after handler in ajax method TypeScriptTestInterface.la5 " + _ex2, _ex2);
                }
            }
        };
        var ajaxParams = {
            method: "GET",
            url: sUrl,
            contentType: "text/plain; charset=utf-8"
        };
        $(document).trigger(TypeScriptTestInterface.LA5_REQUEST, ajaxParams);
        $.ajax(ajaxParams).done(function (data, textStatus, jqXHR) {
            var successParams = { ajax: _this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status };
            $(document).trigger("basis.http.response", successParams);
            $(document).trigger(TypeScriptTestInterface.LA5_RESPONSE_SUCCESS, successParams);
            var la5 = data ? data.map(function (pojo1) { return pojo1 = pojo1 ? pojo1.map(function (pojo2) { return entities_1.EntityA.createEntityAFromPojo(pojo2); }) : []; }) : [];
            var entity = la5;
            try {
                callbackSuccess(entity);
            }
            catch (_ex1) {
                console.error("Error while executing success handler in ajax method TypeScriptTestInterface.la5 " + _ex1, _ex1);
            }
            callCallbackAfter();
        }).fail(function (jqXHR, textStatus, errorThrown) {
            var errorParams = { ajax: _this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status };
            $(document).trigger("basis.http.response", errorParams);
            $(document).trigger(TypeScriptTestInterface.LA5_RESPONSE_ERROR, errorParams);
            console.error("error in ajax method TypeScriptTestInterface.la5 " + textStatus + " - " + errorThrown);
            if (callbackError) {
                try {
                    callbackError(jqXHR);
                }
                catch (_ex) {
                    console.error("Error while executing error handler in ajax method TypeScriptTestInterface.la5 " + _ex, _ex);
                }
            }
            callCallbackAfter();
        });
    };
    TypeScriptTestInterface.prototype.la6Object = function (data) {
        this.la6(data.aas, data.callbackSuccess, data.callbackError, data.callbackAfter);
    };
    TypeScriptTestInterface.prototype.la6 = function (aas, callbackSuccess, callbackError, callbackAfter) {
        var _this = this;
        if (callbackError === void 0) { callbackError = null; }
        if (callbackAfter === void 0) { callbackAfter = null; }
        var sUrl = this.contextPath + "//?callTimestamp=" + (new Date()).getTime();
        var callCallbackAfter = function () {
            if (callbackAfter != null) {
                try {
                    callbackAfter();
                }
                catch (_ex2) {
                    console.error("Error while executing after handler in ajax method TypeScriptTestInterface.la6 " + _ex2, _ex2);
                }
            }
        };
        var ajaxParams = {
            method: "GET",
            url: sUrl,
            contentType: "text/plain; charset=utf-8"
        };
        $(document).trigger(TypeScriptTestInterface.LA6_REQUEST, ajaxParams);
        $.ajax(ajaxParams).done(function (data, textStatus, jqXHR) {
            var successParams = { ajax: _this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status };
            $(document).trigger("basis.http.response", successParams);
            $(document).trigger(TypeScriptTestInterface.LA6_RESPONSE_SUCCESS, successParams);
            var la6 = data ? data.map(function (pojo1) { return pojo1 = pojo1 ? pojo1.map(function (pojo2) { return entities_1.EntityA.createEntityAFromPojo(pojo2); }) : []; }) : [];
            var entity = la6;
            try {
                callbackSuccess(entity);
            }
            catch (_ex1) {
                console.error("Error while executing success handler in ajax method TypeScriptTestInterface.la6 " + _ex1, _ex1);
            }
            callCallbackAfter();
        }).fail(function (jqXHR, textStatus, errorThrown) {
            var errorParams = { ajax: _this, response: jqXHR, url: sUrl, requestMethod: "GET", code: jqXHR.status };
            $(document).trigger("basis.http.response", errorParams);
            $(document).trigger(TypeScriptTestInterface.LA6_RESPONSE_ERROR, errorParams);
            console.error("error in ajax method TypeScriptTestInterface.la6 " + textStatus + " - " + errorThrown);
            if (callbackError) {
                try {
                    callbackError(jqXHR);
                }
                catch (_ex) {
                    console.error("Error while executing error handler in ajax method TypeScriptTestInterface.la6 " + _ex, _ex);
                }
            }
            callCallbackAfter();
        });
    };
    TypeScriptTestInterface.A = "TypeScriptTestInterface.a";
    TypeScriptTestInterface.A_REQUEST = "TypeScriptTestInterface.aRequest";
    TypeScriptTestInterface.A_RESPONSE_SUCCESS = "TypeScriptTestInterface.aResponseSuccess";
    TypeScriptTestInterface.A_RESPONSE_ERROR = "TypeScriptTestInterface.aResponseError";
    TypeScriptTestInterface.AA = "TypeScriptTestInterface.aa";
    TypeScriptTestInterface.AA_REQUEST = "TypeScriptTestInterface.aaRequest";
    TypeScriptTestInterface.AA_RESPONSE_SUCCESS = "TypeScriptTestInterface.aaResponseSuccess";
    TypeScriptTestInterface.AA_RESPONSE_ERROR = "TypeScriptTestInterface.aaResponseError";
    TypeScriptTestInterface.AAA = "TypeScriptTestInterface.aaa";
    TypeScriptTestInterface.AAA_REQUEST = "TypeScriptTestInterface.aaaRequest";
    TypeScriptTestInterface.AAA_RESPONSE_SUCCESS = "TypeScriptTestInterface.aaaResponseSuccess";
    TypeScriptTestInterface.AAA_RESPONSE_ERROR = "TypeScriptTestInterface.aaaResponseError";
    TypeScriptTestInterface.LA1 = "TypeScriptTestInterface.la1";
    TypeScriptTestInterface.LA1_REQUEST = "TypeScriptTestInterface.la1Request";
    TypeScriptTestInterface.LA1_RESPONSE_SUCCESS = "TypeScriptTestInterface.la1ResponseSuccess";
    TypeScriptTestInterface.LA1_RESPONSE_ERROR = "TypeScriptTestInterface.la1ResponseError";
    TypeScriptTestInterface.LA2 = "TypeScriptTestInterface.la2";
    TypeScriptTestInterface.LA2_REQUEST = "TypeScriptTestInterface.la2Request";
    TypeScriptTestInterface.LA2_RESPONSE_SUCCESS = "TypeScriptTestInterface.la2ResponseSuccess";
    TypeScriptTestInterface.LA2_RESPONSE_ERROR = "TypeScriptTestInterface.la2ResponseError";
    TypeScriptTestInterface.LA3 = "TypeScriptTestInterface.la3";
    TypeScriptTestInterface.LA3_REQUEST = "TypeScriptTestInterface.la3Request";
    TypeScriptTestInterface.LA3_RESPONSE_SUCCESS = "TypeScriptTestInterface.la3ResponseSuccess";
    TypeScriptTestInterface.LA3_RESPONSE_ERROR = "TypeScriptTestInterface.la3ResponseError";
    TypeScriptTestInterface.LA4 = "TypeScriptTestInterface.la4";
    TypeScriptTestInterface.LA4_REQUEST = "TypeScriptTestInterface.la4Request";
    TypeScriptTestInterface.LA4_RESPONSE_SUCCESS = "TypeScriptTestInterface.la4ResponseSuccess";
    TypeScriptTestInterface.LA4_RESPONSE_ERROR = "TypeScriptTestInterface.la4ResponseError";
    TypeScriptTestInterface.LA5 = "TypeScriptTestInterface.la5";
    TypeScriptTestInterface.LA5_REQUEST = "TypeScriptTestInterface.la5Request";
    TypeScriptTestInterface.LA5_RESPONSE_SUCCESS = "TypeScriptTestInterface.la5ResponseSuccess";
    TypeScriptTestInterface.LA5_RESPONSE_ERROR = "TypeScriptTestInterface.la5ResponseError";
    TypeScriptTestInterface.LA6 = "TypeScriptTestInterface.la6";
    TypeScriptTestInterface.LA6_REQUEST = "TypeScriptTestInterface.la6Request";
    TypeScriptTestInterface.LA6_RESPONSE_SUCCESS = "TypeScriptTestInterface.la6ResponseSuccess";
    TypeScriptTestInterface.LA6_RESPONSE_ERROR = "TypeScriptTestInterface.la6ResponseError";
    return TypeScriptTestInterface;
}());
exports.TypeScriptTestInterface = TypeScriptTestInterface;
//# sourceMappingURL=services.js.map