export class A<T> {
  public static createSpecificInstance<T>(pojo: any): A<T> {
    return null;
  }
}

export enum E {
  A, B, C, D, E
}

export interface I<T> {
  bla<S>(t: T): string;
}

export class B extends A<number> implements I<string> {
  public stringMap: {[key: string]: string};

  public static createSpecificInstance<T>(pojo: any): B {
    return null;
  }

  public bla(t: string): string {
    return "";
  }

  static SubB = class {
  }
}

let nums = [1, 2, 3, 4];
nums = nums.map(it => it = it + 10);
nums.forEach(it => {
  console.info(it);
});