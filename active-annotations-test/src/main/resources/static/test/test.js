"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var A = (function () {
    function A() {
    }
    A.createSpecificInstance = function (pojo) {
        return null;
    };
    return A;
}());
exports.A = A;
var E;
(function (E) {
    E[E["A"] = 0] = "A";
    E[E["B"] = 1] = "B";
    E[E["C"] = 2] = "C";
    E[E["D"] = 3] = "D";
    E[E["E"] = 4] = "E";
})(E = exports.E || (exports.E = {}));
var B = (function (_super) {
    __extends(B, _super);
    function B() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    B.createSpecificInstance = function (pojo) {
        return null;
    };
    B.prototype.bla = function (t) {
        return "";
    };
    B.SubB = (function () {
        function class_1() {
        }
        return class_1;
    }());
    return B;
}(A));
exports.B = B;
var nums = [1, 2, 3, 4];
nums = nums.map(function (it) { return it = it + 10; });
nums.forEach(function (it) {
    console.info(it);
});
//# sourceMappingURL=test.js.map