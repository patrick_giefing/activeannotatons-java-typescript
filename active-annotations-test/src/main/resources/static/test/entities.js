"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Enum1;
(function (Enum1) {
    Enum1[Enum1["A"] = 0] = "A";
    Enum1[Enum1["B"] = 1] = "B";
    Enum1[Enum1["C"] = 2] = "C";
    Enum1[Enum1["D"] = 3] = "D";
    Enum1[Enum1["E"] = 4] = "E";
})(Enum1 = exports.Enum1 || (exports.Enum1 = {}));
var Node = (function () {
    function Node() {
    }
    Node.createNodeFromPojo = function (pojo) {
        if (!pojo)
            return null;
        var instance = Node.createSpecificNode(pojo);
        instance.loadFromPojo(pojo);
        return instance;
    };
    Node.createSpecificNode = function (pojo) {
        var discriminator = pojo.discriminator;
        if (discriminator == 'StringNode') {
            return StringNode.createStringNodeFromPojo(pojo);
        }
        else if (discriminator == 'IntegerNode') {
            return IntegerNode.createIntegerNodeFromPojo(pojo);
        }
        else if (discriminator == 'NodeWithParameter') {
            return NodeWithParameter.createNodeWithParameterFromPojo(pojo);
        }
        throw new Error("Can't create instance for discriminator " + discriminator);
    };
    Node.prototype.loadFromPojo = function (pojo) {
        if (!pojo)
            return this;
        this.value = pojo['value'];
        return this;
    };
    Node.prototype.getValue = function () {
        return this.value;
    };
    Node.prototype.setValue = function (value) {
        this.value = value;
        return this;
    };
    return Node;
}());
exports.Node = Node;
var StringNode = (function (_super) {
    __extends(StringNode, _super);
    function StringNode() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    StringNode.createStringNode = function () {
        return new StringNode();
    };
    StringNode.createStringNodeFromPojo = function (pojo) {
        if (!pojo)
            return null;
        var instance = StringNode.createSpecificStringNode(pojo);
        instance.loadFromPojo(pojo);
        return instance;
    };
    StringNode.createSpecificStringNode = function (pojo) {
        return StringNode.createStringNodeFromPojo(pojo);
    };
    StringNode.prototype.loadFromPojo = function (pojo) {
        if (!pojo)
            return this;
        _super.prototype.loadFromPojo.call(this, pojo);
        return this;
    };
    return StringNode;
}(Node));
exports.StringNode = StringNode;
var IntegerNode = (function (_super) {
    __extends(IntegerNode, _super);
    function IntegerNode() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    IntegerNode.createIntegerNode = function () {
        return new IntegerNode();
    };
    IntegerNode.createIntegerNodeFromPojo = function (pojo) {
        if (!pojo)
            return null;
        var instance = IntegerNode.createSpecificIntegerNode(pojo);
        instance.loadFromPojo(pojo);
        return instance;
    };
    IntegerNode.createSpecificIntegerNode = function (pojo) {
        return IntegerNode.createIntegerNodeFromPojo(pojo);
    };
    IntegerNode.prototype.loadFromPojo = function (pojo) {
        if (!pojo)
            return this;
        _super.prototype.loadFromPojo.call(this, pojo);
        return this;
    };
    return IntegerNode;
}(Node));
exports.IntegerNode = IntegerNode;
var NodeWithParameter = (function (_super) {
    __extends(NodeWithParameter, _super);
    function NodeWithParameter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    NodeWithParameter.createNodeWithParameter = function () {
        return new NodeWithParameter();
    };
    NodeWithParameter.createNodeWithParameterFromPojo = function (pojo) {
        if (!pojo)
            return null;
        var instance = NodeWithParameter.createSpecificNodeWithParameter(pojo);
        instance.loadFromPojo(pojo);
        return instance;
    };
    NodeWithParameter.createSpecificNodeWithParameter = function (pojo) {
        return NodeWithParameter.createNodeWithParameterFromPojo(pojo);
    };
    NodeWithParameter.prototype.loadFromPojo = function (pojo) {
        if (!pojo)
            return this;
        _super.prototype.loadFromPojo.call(this, pojo);
        return this;
    };
    return NodeWithParameter;
}(Node));
exports.NodeWithParameter = NodeWithParameter;
var EntityA = (function () {
    function EntityA() {
    }
    EntityA.createEntityA = function () {
        return new EntityA();
    };
    EntityA.createEntityAFromPojo = function (pojo) {
        if (!pojo)
            return null;
        var instance = EntityA.createSpecificEntityA(pojo);
        instance.loadFromPojo(pojo);
        return instance;
    };
    EntityA.createSpecificEntityA = function (pojo) {
        return EntityA.createEntityAFromPojo(pojo);
    };
    EntityA.prototype.loadFromPojo = function (pojo) {
        if (!pojo)
            return this;
        this.s = pojo['s'];
        this.sa = pojo['sa'];
        this.d = pojo['d'] ? new Date(pojo['d']) : null;
        this.da = pojo['da'] ? pojo['da'].map(function (it) { return it ? new Date(it) : null; }) : [];
        this.i = pojo['i'];
        this.ia = pojo['ia'] ? pojo['ia'].map(function (it) { return it; }) : [];
        this.sh = pojo['sh'];
        this.sha = pojo['sha'] ? pojo['sha'].map(function (it) { return it; }) : [];
        this.f = pojo['f'];
        this.fa = pojo['fa'] ? pojo['fa'].map(function (it) { return it; }) : [];
        this.l = pojo['l'];
        this.la = pojo['la'] ? pojo['la'].map(function (it) { return it; }) : [];
        this.dbl = pojo['dbl'];
        this.dbla = pojo['dbla'] ? pojo['dbla'].map(function (it) { return it; }) : [];
        this.b = pojo['b'];
        this.ba = pojo['ba'] ? pojo['ba'].map(function (it) { return it; }) : [];
        this.bo = pojo['bo'];
        this.boa = pojo['boa'] ? pojo['boa'].map(function (it) { return it; }) : [];
        this.wi = pojo['wi'];
        this.wia = pojo['wia'] ? pojo['wia'].map(function (it) { return it; }) : [];
        this.wsh = pojo['wsh'];
        this.wsha = pojo['wsha'] ? pojo['wsha'].map(function (it) { return it; }) : [];
        this.wf = pojo['wf'];
        this.wfa = pojo['wfa'] ? pojo['wfa'].map(function (it) { return it; }) : [];
        this.wl = pojo['wl'];
        this.wla = pojo['wla'] ? pojo['wla'].map(function (it) { return it; }) : [];
        this.wdbl = pojo['wdbl'];
        this.wdbla = pojo['wdbla'] ? pojo['wdbla'].map(function (it) { return it; }) : [];
        this.wb = pojo['wb'];
        this.wba = pojo['wba'] ? pojo['wba'].map(function (it) { return it; }) : [];
        this.wbo = pojo['wbo'];
        this.wboa = pojo['wboa'] ? pojo['wboa'].map(function (it) { return it; }) : [];
        this.strings = pojo['string-s'] ? pojo['string-s'].map(function (it) { return it; }) : [];
        if (pojo['string-map']) {
            var obj1 = pojo['string-map'];
            this.stringMap = {};
            for (var key1 in obj1) {
                this.stringMap[key1] = obj1[key1];
            }
        }
        if (pojo['stringListMap']) {
            var obj1 = pojo['stringListMap'];
            this.stringListMap = {};
            for (var key1 in obj1) {
                this.stringListMap[key1] = obj1[key1] ? obj1[key1].map(function (it) { return it; }) : [];
            }
        }
        if (pojo['stringMapMap']) {
            var obj1 = pojo['stringMapMap'];
            this.stringMapMap = {};
            for (var key1 in obj1) {
                if (obj1[key1]) {
                    var obj2 = obj1[key1];
                    this.stringMapMap[key1] = {};
                    for (var key2 in obj2) {
                        this.stringMapMap[key1][key2] = obj2[key2];
                    }
                }
            }
        }
        if (pojo['entityBMap']) {
            var obj1 = pojo['entityBMap'];
            this.entityBMap = {};
            for (var key1 in obj1) {
                this.entityBMap[key1] = EntityB.createEntityBFromPojo(obj1[key1]);
            }
        }
        if (pojo['entityBMapList']) {
            var obj1 = pojo['entityBMapList'];
            this.entityBMapList = {};
            for (var key1 in obj1) {
                this.entityBMapList[key1] = obj1[key1] ? obj1[key1].map(function (pojo2) { return EntityB.createEntityBFromPojo(pojo2); }) : [];
            }
        }
        if (pojo['entityBMapMapList']) {
            var obj1 = pojo['entityBMapMapList'];
            this.entityBMapMapList = {};
            for (var key1 in obj1) {
                if (obj1[key1]) {
                    var obj2 = obj1[key1];
                    this.entityBMapMapList[key1] = {};
                    for (var key2 in obj2) {
                        this.entityBMapMapList[key1][key2] = obj2[key2] ? obj2[key2].map(function (pojo3) { return EntityB.createEntityBFromPojo(pojo3); }) : [];
                    }
                }
            }
        }
        if (pojo['entityBMapListMap']) {
            var obj1 = pojo['entityBMapListMap'];
            this.entityBMapListMap = {};
            for (var key1 in obj1) {
                this.entityBMapListMap[key1] = obj1[key1] ? obj1[key1].map(function (pojo2) {
                    if (pojo2) {
                        var obj3 = pojo2;
                        pojo2 = {};
                        for (var key3 in obj3) {
                            pojo2[key3] = EntityB.createEntityBFromPojo(obj3[key3]);
                        }
                    }
                    return pojo2;
                }) : [];
            }
        }
        this.localDate = pojo['localDate'] ? Date.parseString(pojo['localDate'], 'yyyy-MM-dd') : null;
        this.localDates = pojo['localDates'] ? pojo['localDates'].map(function (it) { return it ? Date.parseString(it, 'yyyy-MM-dd') : null; }) : [];
        this.localDateList = pojo['localDateList'] ? pojo['localDateList'].map(function (it) { return it ? Date.parseString(it, 'yyyy-MM-dd') : null; }) : [];
        this.localDateSet = pojo['localDateSet'] ? pojo['localDateSet'].map(function (it) { return it ? Date.parseString(it, 'yyyy-MM-dd') : null; }) : [];
        this.localTime = pojo['localTime'] ? Date.parseString(pojo['localTime'], 'hh:mm') : null;
        this.localTimes = pojo['localTimes'] ? pojo['localTimes'].map(function (it) { return it ? Date.parseString(it, 'hh:mm') : null; }) : [];
        this.localTimeList = pojo['localTimeList'] ? pojo['localTimeList'].map(function (it) { return it ? Date.parseString(it, 'hh:mm') : null; }) : [];
        this.localTimeSet = pojo['localTimeSet'] ? pojo['localTimeSet'].map(function (it) { return it ? Date.parseString(it, 'hh:mm') : null; }) : [];
        this.localDateTime = pojo['localDateTime'] ? Date.parseString(pojo['localDateTime'], 'yyyy-MM-dd hh:mm') : null;
        this.localDateTimes = pojo['localDateTimes'] ? pojo['localDateTimes'].map(function (it) { return it ? Date.parseString(it, 'yyyy-MM-dd hh:mm') : null; }) : [];
        this.localDateTimeList = pojo['localDateTimeList'] ? pojo['localDateTimeList'].map(function (it) { return it ? Date.parseString(it, 'yyyy-MM-dd hh:mm') : null; }) : [];
        this.localDateTimeSet = pojo['localDateTimeSet'] ? pojo['localDateTimeSet'].map(function (it) { return it ? Date.parseString(it, 'yyyy-MM-dd hh:mm') : null; }) : [];
        this.laInts = pojo['laInts'] ? pojo['laInts'].map(function (pojo1) { return pojo1 = pojo1 ? pojo1.map(function (it) { return it; }) : []; }) : [];
        this.laInts2 = pojo['laInts2'] ? pojo['laInts2'].map(function (it) { return it; }) : [];
        this.lllInts = pojo['lllInts'] ? pojo['lllInts'].map(function (pojo1) { return pojo1 = pojo1 ? pojo1.map(function (pojo2) { return pojo2 = pojo2 ? pojo2.map(function (it) { return it; }) : []; }) : []; }) : [];
        this.aaaInts = pojo['aaaInts'] ? pojo['aaaInts'].map(function (it) { return it; }) : [];
        this.enum1 = pojo['enum1'] ? Enum1[pojo['enum1']] : null;
        this.enumList = pojo['enumList'] ? pojo['enumList'].map(function (it) { return it; }) : [];
        if (pojo['enumMap']) {
            var obj1 = pojo['enumMap'];
            this.enumMap = {};
            for (var key1 in obj1) {
                this.enumMap[key1] = obj1[key1] ? Enum1[obj1[key1]] : null;
            }
        }
        if (pojo['enumMapMapList']) {
            var obj1 = pojo['enumMapMapList'];
            this.enumMapMapList = {};
            for (var key1 in obj1) {
                if (obj1[key1]) {
                    var obj2 = obj1[key1];
                    this.enumMapMapList[key1] = {};
                    for (var key2 in obj2) {
                        this.enumMapMapList[key1][key2] = obj2[key2] ? obj2[key2].map(function (it) { return it; }) : [];
                    }
                }
            }
        }
        this.enum1_s = pojo['enum1_s'] ? Enum1[pojo['enum1_s']] : null;
        this.enum1_num = pojo['enum1_num'] ? Enum1[pojo['enum1_num']] : null;
        this.enum1_nat = pojo['enum1_nat'] ? Enum1[pojo['enum1_nat']] : null;
        this.enum1_scal = pojo['enum1_scal'] ? Enum1[pojo['enum1_scal']] : null;
        this.enum1_arr = pojo['enum1_arr'] ? Enum1[pojo['enum1_arr']] : null;
        return this;
    };
    EntityA.prototype.getS = function () {
        return this.s;
    };
    EntityA.prototype.setS = function (s) {
        this.s = s;
        return this;
    };
    EntityA.prototype.getSa = function () {
        return this.sa;
    };
    EntityA.prototype.setSa = function (sa) {
        this.sa = sa;
        return this;
    };
    EntityA.prototype.getD = function () {
        return this.d;
    };
    EntityA.prototype.setD = function (d) {
        this.d = d;
        return this;
    };
    EntityA.prototype.getDa = function () {
        return this.da;
    };
    EntityA.prototype.setDa = function (da) {
        this.da = da;
        return this;
    };
    EntityA.prototype.getI = function () {
        return this.i;
    };
    EntityA.prototype.setI = function (i) {
        this.i = i;
        return this;
    };
    EntityA.prototype.getIa = function () {
        return this.ia;
    };
    EntityA.prototype.setIa = function (ia) {
        this.ia = ia;
        return this;
    };
    EntityA.prototype.getSh = function () {
        return this.sh;
    };
    EntityA.prototype.setSh = function (sh) {
        this.sh = sh;
        return this;
    };
    EntityA.prototype.getSha = function () {
        return this.sha;
    };
    EntityA.prototype.setSha = function (sha) {
        this.sha = sha;
        return this;
    };
    EntityA.prototype.getF = function () {
        return this.f;
    };
    EntityA.prototype.setF = function (f) {
        this.f = f;
        return this;
    };
    EntityA.prototype.getFa = function () {
        return this.fa;
    };
    EntityA.prototype.setFa = function (fa) {
        this.fa = fa;
        return this;
    };
    EntityA.prototype.getL = function () {
        return this.l;
    };
    EntityA.prototype.setL = function (l) {
        this.l = l;
        return this;
    };
    EntityA.prototype.getLa = function () {
        return this.la;
    };
    EntityA.prototype.setLa = function (la) {
        this.la = la;
        return this;
    };
    EntityA.prototype.getDbl = function () {
        return this.dbl;
    };
    EntityA.prototype.setDbl = function (dbl) {
        this.dbl = dbl;
        return this;
    };
    EntityA.prototype.getDbla = function () {
        return this.dbla;
    };
    EntityA.prototype.setDbla = function (dbla) {
        this.dbla = dbla;
        return this;
    };
    EntityA.prototype.getB = function () {
        return this.b;
    };
    EntityA.prototype.setB = function (b) {
        this.b = b;
        return this;
    };
    EntityA.prototype.getBa = function () {
        return this.ba;
    };
    EntityA.prototype.setBa = function (ba) {
        this.ba = ba;
        return this;
    };
    EntityA.prototype.isBo = function () {
        return this.bo;
    };
    EntityA.prototype.setBo = function (bo) {
        this.bo = bo;
        return this;
    };
    EntityA.prototype.getBoa = function () {
        return this.boa;
    };
    EntityA.prototype.setBoa = function (boa) {
        this.boa = boa;
        return this;
    };
    EntityA.prototype.getWi = function () {
        return this.wi;
    };
    EntityA.prototype.setWi = function (wi) {
        this.wi = wi;
        return this;
    };
    EntityA.prototype.getWia = function () {
        return this.wia;
    };
    EntityA.prototype.setWia = function (wia) {
        this.wia = wia;
        return this;
    };
    EntityA.prototype.getWsh = function () {
        return this.wsh;
    };
    EntityA.prototype.setWsh = function (wsh) {
        this.wsh = wsh;
        return this;
    };
    EntityA.prototype.getWsha = function () {
        return this.wsha;
    };
    EntityA.prototype.setWsha = function (wsha) {
        this.wsha = wsha;
        return this;
    };
    EntityA.prototype.getWf = function () {
        return this.wf;
    };
    EntityA.prototype.setWf = function (wf) {
        this.wf = wf;
        return this;
    };
    EntityA.prototype.getWfa = function () {
        return this.wfa;
    };
    EntityA.prototype.setWfa = function (wfa) {
        this.wfa = wfa;
        return this;
    };
    EntityA.prototype.getWl = function () {
        return this.wl;
    };
    EntityA.prototype.setWl = function (wl) {
        this.wl = wl;
        return this;
    };
    EntityA.prototype.getWla = function () {
        return this.wla;
    };
    EntityA.prototype.setWla = function (wla) {
        this.wla = wla;
        return this;
    };
    EntityA.prototype.getWdbl = function () {
        return this.wdbl;
    };
    EntityA.prototype.setWdbl = function (wdbl) {
        this.wdbl = wdbl;
        return this;
    };
    EntityA.prototype.getWdbla = function () {
        return this.wdbla;
    };
    EntityA.prototype.setWdbla = function (wdbla) {
        this.wdbla = wdbla;
        return this;
    };
    EntityA.prototype.getWb = function () {
        return this.wb;
    };
    EntityA.prototype.setWb = function (wb) {
        this.wb = wb;
        return this;
    };
    EntityA.prototype.getWba = function () {
        return this.wba;
    };
    EntityA.prototype.setWba = function (wba) {
        this.wba = wba;
        return this;
    };
    EntityA.prototype.getWbo = function () {
        return this.wbo;
    };
    EntityA.prototype.setWbo = function (wbo) {
        this.wbo = wbo;
        return this;
    };
    EntityA.prototype.getWboa = function () {
        return this.wboa;
    };
    EntityA.prototype.setWboa = function (wboa) {
        this.wboa = wboa;
        return this;
    };
    EntityA.prototype.getStrings = function () {
        return this.strings;
    };
    EntityA.prototype.setStrings = function (strings) {
        this.strings = strings;
        return this;
    };
    EntityA.prototype.getStringMap = function () {
        return this.stringMap;
    };
    EntityA.prototype.setStringMap = function (stringMap) {
        this.stringMap = stringMap;
        return this;
    };
    EntityA.prototype.getStringListMap = function () {
        return this.stringListMap;
    };
    EntityA.prototype.setStringListMap = function (stringListMap) {
        this.stringListMap = stringListMap;
        return this;
    };
    EntityA.prototype.getStringMapMap = function () {
        return this.stringMapMap;
    };
    EntityA.prototype.setStringMapMap = function (stringMapMap) {
        this.stringMapMap = stringMapMap;
        return this;
    };
    EntityA.prototype.getEntityBMap = function () {
        return this.entityBMap;
    };
    EntityA.prototype.setEntityBMap = function (entityBMap) {
        this.entityBMap = entityBMap;
        return this;
    };
    EntityA.prototype.getEntityBMapList = function () {
        return this.entityBMapList;
    };
    EntityA.prototype.setEntityBMapList = function (entityBMapList) {
        this.entityBMapList = entityBMapList;
        return this;
    };
    EntityA.prototype.getEntityBMapMapList = function () {
        return this.entityBMapMapList;
    };
    EntityA.prototype.setEntityBMapMapList = function (entityBMapMapList) {
        this.entityBMapMapList = entityBMapMapList;
        return this;
    };
    EntityA.prototype.getEntityBMapListMap = function () {
        return this.entityBMapListMap;
    };
    EntityA.prototype.setEntityBMapListMap = function (entityBMapListMap) {
        this.entityBMapListMap = entityBMapListMap;
        return this;
    };
    EntityA.prototype.getLocalDate = function () {
        return this.localDate;
    };
    EntityA.prototype.setLocalDate = function (localDate) {
        this.localDate = localDate;
        return this;
    };
    EntityA.prototype.getLocalDates = function () {
        return this.localDates;
    };
    EntityA.prototype.setLocalDates = function (localDates) {
        this.localDates = localDates;
        return this;
    };
    EntityA.prototype.getLocalDateList = function () {
        return this.localDateList;
    };
    EntityA.prototype.setLocalDateList = function (localDateList) {
        this.localDateList = localDateList;
        return this;
    };
    EntityA.prototype.getLocalDateSet = function () {
        return this.localDateSet;
    };
    EntityA.prototype.setLocalDateSet = function (localDateSet) {
        this.localDateSet = localDateSet;
        return this;
    };
    EntityA.prototype.getLocalTime = function () {
        return this.localTime;
    };
    EntityA.prototype.setLocalTime = function (localTime) {
        this.localTime = localTime;
        return this;
    };
    EntityA.prototype.getLocalTimes = function () {
        return this.localTimes;
    };
    EntityA.prototype.setLocalTimes = function (localTimes) {
        this.localTimes = localTimes;
        return this;
    };
    EntityA.prototype.getLocalTimeList = function () {
        return this.localTimeList;
    };
    EntityA.prototype.setLocalTimeList = function (localTimeList) {
        this.localTimeList = localTimeList;
        return this;
    };
    EntityA.prototype.getLocalTimeSet = function () {
        return this.localTimeSet;
    };
    EntityA.prototype.setLocalTimeSet = function (localTimeSet) {
        this.localTimeSet = localTimeSet;
        return this;
    };
    EntityA.prototype.getLocalDateTime = function () {
        return this.localDateTime;
    };
    EntityA.prototype.setLocalDateTime = function (localDateTime) {
        this.localDateTime = localDateTime;
        return this;
    };
    EntityA.prototype.getLocalDateTimes = function () {
        return this.localDateTimes;
    };
    EntityA.prototype.setLocalDateTimes = function (localDateTimes) {
        this.localDateTimes = localDateTimes;
        return this;
    };
    EntityA.prototype.getLocalDateTimeList = function () {
        return this.localDateTimeList;
    };
    EntityA.prototype.setLocalDateTimeList = function (localDateTimeList) {
        this.localDateTimeList = localDateTimeList;
        return this;
    };
    EntityA.prototype.getLocalDateTimeSet = function () {
        return this.localDateTimeSet;
    };
    EntityA.prototype.setLocalDateTimeSet = function (localDateTimeSet) {
        this.localDateTimeSet = localDateTimeSet;
        return this;
    };
    EntityA.prototype.getLaInts = function () {
        return this.laInts;
    };
    EntityA.prototype.setLaInts = function (laInts) {
        this.laInts = laInts;
        return this;
    };
    EntityA.prototype.getLaInts2 = function () {
        return this.laInts2;
    };
    EntityA.prototype.setLaInts2 = function (laInts2) {
        this.laInts2 = laInts2;
        return this;
    };
    EntityA.prototype.getLllInts = function () {
        return this.lllInts;
    };
    EntityA.prototype.setLllInts = function (lllInts) {
        this.lllInts = lllInts;
        return this;
    };
    EntityA.prototype.getAaaInts = function () {
        return this.aaaInts;
    };
    EntityA.prototype.setAaaInts = function (aaaInts) {
        this.aaaInts = aaaInts;
        return this;
    };
    EntityA.prototype.getEnum1 = function () {
        return this.enum1;
    };
    EntityA.prototype.setEnum1 = function (enum1) {
        this.enum1 = enum1;
        return this;
    };
    EntityA.prototype.getEnumList = function () {
        return this.enumList;
    };
    EntityA.prototype.setEnumList = function (enumList) {
        this.enumList = enumList;
        return this;
    };
    EntityA.prototype.getEnumMap = function () {
        return this.enumMap;
    };
    EntityA.prototype.setEnumMap = function (enumMap) {
        this.enumMap = enumMap;
        return this;
    };
    EntityA.prototype.getEnumMapMapList = function () {
        return this.enumMapMapList;
    };
    EntityA.prototype.setEnumMapMapList = function (enumMapMapList) {
        this.enumMapMapList = enumMapMapList;
        return this;
    };
    EntityA.prototype.getEnum1_s = function () {
        return this.enum1_s;
    };
    EntityA.prototype.setEnum1_s = function (enum1_s) {
        this.enum1_s = enum1_s;
        return this;
    };
    EntityA.prototype.getEnum1_num = function () {
        return this.enum1_num;
    };
    EntityA.prototype.setEnum1_num = function (enum1_num) {
        this.enum1_num = enum1_num;
        return this;
    };
    EntityA.prototype.getEnum1_nat = function () {
        return this.enum1_nat;
    };
    EntityA.prototype.setEnum1_nat = function (enum1_nat) {
        this.enum1_nat = enum1_nat;
        return this;
    };
    EntityA.prototype.getEnum1_scal = function () {
        return this.enum1_scal;
    };
    EntityA.prototype.setEnum1_scal = function (enum1_scal) {
        this.enum1_scal = enum1_scal;
        return this;
    };
    EntityA.prototype.getEnum1_arr = function () {
        return this.enum1_arr;
    };
    EntityA.prototype.setEnum1_arr = function (enum1_arr) {
        this.enum1_arr = enum1_arr;
        return this;
    };
    return EntityA;
}());
exports.EntityA = EntityA;
var X1 = (function () {
    function X1() {
    }
    X1.createX1 = function () {
        return new X1();
    };
    X1.createX1FromPojo = function (pojo) {
        if (!pojo)
            return null;
        var instance = X1.createSpecificX1(pojo);
        instance.loadFromPojo(pojo);
        return instance;
    };
    X1.createSpecificX1 = function (pojo) {
        return X1.createX1FromPojo(pojo);
    };
    X1.prototype.loadFromPojo = function (pojo) {
        if (!pojo)
            return this;
        return this;
    };
    return X1;
}());
exports.X1 = X1;
var Entity1 = (function () {
    function Entity1() {
    }
    Entity1.createEntity1 = function () {
        return new Entity1();
    };
    Entity1.createEntity1FromPojo = function (pojo) {
        if (!pojo)
            return null;
        var instance = Entity1.createSpecificEntity1(pojo);
        instance.loadFromPojo(pojo);
        return instance;
    };
    Entity1.createSpecificEntity1 = function (pojo) {
        return Entity1.createEntity1FromPojo(pojo);
    };
    Entity1.prototype.loadFromPojo = function (pojo) {
        if (!pojo)
            return this;
        this.i = pojo['i'];
        this.l = pojo['l'];
        this.s = pojo['s'];
        this.c = pojo['c'];
        this.entity2 = Entity2.createEntity2FromPojo(pojo['entity2']);
        this.entities2 = pojo['entities2'] ? pojo['entities2'].map(function (pojo1) { return Entity2.createEntity2FromPojo(pojo1); }) : [];
        return this;
    };
    Entity1.prototype.getI = function () {
        return this.i;
    };
    Entity1.prototype.setI = function (i) {
        this.i = i;
        return this;
    };
    Entity1.prototype.getL = function () {
        return this.l;
    };
    Entity1.prototype.setL = function (l) {
        this.l = l;
        return this;
    };
    Entity1.prototype.getS = function () {
        return this.s;
    };
    Entity1.prototype.setS = function (s) {
        this.s = s;
        return this;
    };
    Entity1.prototype.getC = function () {
        return this.c;
    };
    Entity1.prototype.setC = function (c) {
        this.c = c;
        return this;
    };
    Entity1.prototype.getEntity2 = function () {
        return this.entity2;
    };
    Entity1.prototype.setEntity2 = function (entity2) {
        this.entity2 = entity2;
        return this;
    };
    Entity1.prototype.getEntities2 = function () {
        return this.entities2;
    };
    Entity1.prototype.setEntities2 = function (entities2) {
        this.entities2 = entities2;
        return this;
    };
    return Entity1;
}());
exports.Entity1 = Entity1;
var Entity2 = (function () {
    function Entity2() {
    }
    Entity2.createEntity2 = function () {
        return new Entity2();
    };
    Entity2.createEntity2FromPojo = function (pojo) {
        if (!pojo)
            return null;
        var instance = Entity2.createSpecificEntity2(pojo);
        instance.loadFromPojo(pojo);
        return instance;
    };
    Entity2.createSpecificEntity2 = function (pojo) {
        return Entity2.createEntity2FromPojo(pojo);
    };
    Entity2.prototype.loadFromPojo = function (pojo) {
        if (!pojo)
            return this;
        this.nodeList = pojo['nodeList'] ? pojo['nodeList'].map(function (pojo1) { return Node.createNodeFromPojo(pojo1); }) : [];
        this.nodeList2 = pojo['nodeList2'] ? pojo['nodeList2'].map(function (pojo1) { return Node.createNodeFromPojo(pojo1); }) : [];
        this.nodeList3 = pojo['nodeList3'] ? pojo['nodeList3'].map(function (pojo1) { return NodeWithParameter.createNodeWithParameterFromPojo(pojo1); }) : [];
        return this;
    };
    Entity2.prototype.getNodeList = function () {
        return this.nodeList;
    };
    Entity2.prototype.setNodeList = function (nodeList) {
        this.nodeList = nodeList;
        return this;
    };
    Entity2.prototype.getNodeList2 = function () {
        return this.nodeList2;
    };
    Entity2.prototype.setNodeList2 = function (nodeList2) {
        this.nodeList2 = nodeList2;
        return this;
    };
    Entity2.prototype.getNodeList3 = function () {
        return this.nodeList3;
    };
    Entity2.prototype.setNodeList3 = function (nodeList3) {
        this.nodeList3 = nodeList3;
        return this;
    };
    return Entity2;
}());
exports.Entity2 = Entity2;
var S = (function () {
    function S() {
    }
    S.createS = function () {
        return new S();
    };
    S.createSFromPojo = function (pojo) {
        if (!pojo)
            return null;
        var instance = S.createSpecificS(pojo);
        instance.loadFromPojo(pojo);
        return instance;
    };
    S.createSpecificS = function (pojo) {
        return S.createSFromPojo(pojo);
    };
    S.prototype.loadFromPojo = function (pojo) {
        if (!pojo)
            return this;
        return this;
    };
    return S;
}());
exports.S = S;
var EntityB = (function () {
    function EntityB() {
    }
    EntityB.createEntityB = function () {
        return new EntityB();
    };
    EntityB.createEntityBFromPojo = function (pojo) {
        if (!pojo)
            return null;
        var instance = EntityB.createSpecificEntityB(pojo);
        instance.loadFromPojo(pojo);
        return instance;
    };
    EntityB.createSpecificEntityB = function (pojo) {
        return EntityB.createEntityBFromPojo(pojo);
    };
    EntityB.prototype.loadFromPojo = function (pojo) {
        if (!pojo)
            return this;
        this.a = EntityA.createEntityAFromPojo(pojo['a']);
        this.aa = pojo['aa'] ? pojo['aa'].map(function (pojo1) { return EntityA.createEntityAFromPojo(pojo1); }) : [];
        this.aaa = pojo['aaa'] ? pojo['aaa'].map(function (pojo1) { return pojo1 = pojo1 ? pojo1.map(function (pojo2) { return EntityA.createEntityAFromPojo(pojo2); }) : []; }) : [];
        this.la1 = pojo['la1'] ? pojo['la1'].map(function (pojo1) { return EntityA.createEntityAFromPojo(pojo1); }) : [];
        this.la2 = pojo['la2'] ? pojo['la2'].map(function (pojo1) { return EntityA.createEntityAFromPojo(pojo1); }) : [];
        this.la3 = pojo['la3'] ? pojo['la3'].map(function (pojo1) { return EntityA.createEntityAFromPojo(pojo1); }) : [];
        this.la4 = pojo['la4'] ? pojo['la4'].map(function (pojo1) { return pojo1 = pojo1 ? pojo1.map(function (pojo2) { return EntityA.createEntityAFromPojo(pojo2); }) : []; }) : [];
        this.la5 = pojo['la5'] ? pojo['la5'].map(function (pojo1) { return pojo1 = pojo1 ? pojo1.map(function (pojo2) { return EntityA.createEntityAFromPojo(pojo2); }) : []; }) : [];
        this.la6 = pojo['la6'] ? pojo['la6'].map(function (pojo1) { return pojo1 = pojo1 ? pojo1.map(function (pojo2) { return EntityA.createEntityAFromPojo(pojo2); }) : []; }) : [];
        return this;
    };
    EntityB.prototype.getA = function () {
        return this.a;
    };
    EntityB.prototype.setA = function (a) {
        this.a = a;
        return this;
    };
    EntityB.prototype.getAa = function () {
        return this.aa;
    };
    EntityB.prototype.setAa = function (aa) {
        this.aa = aa;
        return this;
    };
    EntityB.prototype.getAaa = function () {
        return this.aaa;
    };
    EntityB.prototype.setAaa = function (aaa) {
        this.aaa = aaa;
        return this;
    };
    EntityB.prototype.getLa1 = function () {
        return this.la1;
    };
    EntityB.prototype.setLa1 = function (la1) {
        this.la1 = la1;
        return this;
    };
    EntityB.prototype.getLa2 = function () {
        return this.la2;
    };
    EntityB.prototype.setLa2 = function (la2) {
        this.la2 = la2;
        return this;
    };
    EntityB.prototype.getLa3 = function () {
        return this.la3;
    };
    EntityB.prototype.setLa3 = function (la3) {
        this.la3 = la3;
        return this;
    };
    EntityB.prototype.getLa4 = function () {
        return this.la4;
    };
    EntityB.prototype.setLa4 = function (la4) {
        this.la4 = la4;
        return this;
    };
    EntityB.prototype.getLa5 = function () {
        return this.la5;
    };
    EntityB.prototype.setLa5 = function (la5) {
        this.la5 = la5;
        return this;
    };
    EntityB.prototype.getLa6 = function () {
        return this.la6;
    };
    EntityB.prototype.setLa6 = function (la6) {
        this.la6 = la6;
        return this;
    };
    return EntityB;
}());
exports.EntityB = EntityB;
//# sourceMappingURL=entities.js.map