package es.rick.activeannotations.example.entity

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonSubTypes.Type
import com.fasterxml.jackson.annotation.JsonTypeInfo
import es.rick.activeannotations.GenerateEntity
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlRootElement
import javax.xml.bind.annotation.XmlSeeAlso
import java.util.Set
import java.util.List

@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.PROPERTY, property="typeDiscriminator")
@JsonSubTypes(#[
	@Type(Cat),
	@Type(Dog)
])
@XmlRootElement @XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso(#[Cat, Dog])
@GenerateEntity
class Pet {
	String name
	int age
}

@GenerateEntity
class Cat extends Pet {
	int lifes
}

@GenerateEntity
class Dog extends Pet {
	int postmenBitten
}

@GenerateEntity
class CatsAndDogs extends Pet {
	List<Cat> cats
	Set<Dog> dogs
}
