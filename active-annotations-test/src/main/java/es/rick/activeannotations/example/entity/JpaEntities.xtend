package es.rick.activeannotations.example.entity

import es.rick.activeannotations.EntityConstructor
import es.rick.activeannotations.GenerateEntity
import javax.persistence.Id
import es.rick.activeannotations.GenerateInterface

@GenerateEntity(constructors=@EntityConstructor(value=#["id1", "id2", "s"]), generateInterfaces=@GenerateInterface)
class Entity1 {
	@Id String id1
	@Id int id2
	String s
}

@GenerateEntity
class Entity2 {
	int i1
	int i2
}
