package es.rick.activeannotations.example.service

import es.rick.activeannotations.example.entity.Cat
import es.rick.activeannotations.example.entity.CatsAndDogs
import es.rick.activeannotations.example.entity.Dog
import es.rick.activeannotations.example.entity.Pet
import java.util.List
import javax.ws.rs.GET
import javax.ws.rs.Path
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import es.rick.activeannotations.RestClient

@Path(PetService.PATH)
@RequestMapping(PetService.PATH)
@ResponseBody
@RestClient
interface PetService {
	String PATH = "pets"
	String PATH_PETS = "all"
	String PATH_CATS = "cats"
	String PATH_DOGS = "dogs"
	String PATH_CATS_AND_DOGS = "cat-and-dogs"

	@Path(PATH_PETS) @GET @GetMapping(PATH_PETS)
	def List<? extends Pet> getPets()

	@Path(PATH_CATS) @GET @GetMapping(PATH_CATS)
	def List<Cat> getCats()

	@Path(PATH_DOGS) @GET @GetMapping(PATH_DOGS)
	def List<Dog> getDogs()

	@Path(PATH_CATS_AND_DOGS) @GET @GetMapping(PATH_CATS_AND_DOGS)
	def CatsAndDogs getCatsAndDogs()
}
