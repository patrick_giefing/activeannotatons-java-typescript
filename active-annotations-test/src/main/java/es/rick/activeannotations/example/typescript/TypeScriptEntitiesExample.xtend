package es.rick.activeannotations.example.typescript

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonSubTypes.Type
import com.fasterxml.jackson.annotation.JsonTypeInfo
import es.rick.activeannotations.Entities
import es.rick.activeannotations.GenerateEntity
import es.rick.activeannotations.TypeScriptEntities
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.util.Date
import java.util.List
import java.util.Map
import java.util.Set
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlRootElement
import javax.xml.bind.annotation.XmlSeeAlso

@TypeScriptEntities(projectRoot="src/main/resources/static/", filePath="test/entities.ts", entities=@Entities(entityClasses=#[
	Node,
	StringNode,
	IntegerNode,
	NodeWithParameter,
	Enum1,
	EntityA,
	X1,
	X2,
	Entity1,
	Entity2,
	S,
	EntityB
]))
class TypeScriptEntitiesExample {
}

enum Enum1 {
	A, B, C, D, E
}

@GenerateEntity
class EntityA {
	String s
	String sa
	Date d
	Date[] da
	int i
	int[] ia
	short sh
	short[] sha
	float f
	float[] fa
	long l
	long[] la
	double dbl
	double[] dbla
	byte b
	byte[] ba
	boolean bo
	boolean[] boa
	Integer wi
	Integer[] wia
	Short wsh
	Short[] wsha
	Float wf
	Float[] wfa
	Long wl
	Long[] wla
	Double wdbl
	Double[] wdbla
	Byte wb
	Byte[] wba
	Boolean wbo
	Boolean[] wboa
	@JsonProperty("string-s") String[] strings

	@JsonProperty("string-map") Map<String, String> stringMap
	Map<String, List<String>> stringListMap
	Map<String, Map<String, String>> stringMapMap
	// Map<byte[], byte[]> byteMap // invalid key
	Map<String, EntityB> entityBMap
	Map<String, List<EntityB>> entityBMapList
	Map<String, Map<Number, List<EntityB>>> entityBMapMapList
	Map<String, List<Map<Number, EntityB>>> entityBMapListMap

	@JsonFormat(pattern="yyyy-MM-dd") LocalDate localDate
	@JsonFormat(pattern="yyyy-MM-dd") LocalDate[] localDates
	@JsonFormat(pattern="yyyy-MM-dd") List<LocalDate> localDateList
	@JsonFormat(pattern="yyyy-MM-dd") Set<LocalDate> localDateSet

	@JsonFormat(pattern="hh:mm") LocalTime localTime
	@JsonFormat(pattern="hh:mm") LocalTime[] localTimes
	@JsonFormat(pattern="hh:mm") List<LocalTime> localTimeList
	@JsonFormat(pattern="hh:mm") Set<LocalTime> localTimeSet

	@JsonFormat(pattern="yyyy-MM-dd hh:mm") LocalDateTime localDateTime
	@JsonFormat(pattern="yyyy-MM-dd hh:mm") LocalDateTime[] localDateTimes
	@JsonFormat(pattern="yyyy-MM-dd hh:mm") List<LocalDateTime> localDateTimeList
	@JsonFormat(pattern="yyyy-MM-dd hh:mm") Set<LocalDateTime> localDateTimeSet

	List<Integer[]> laInts
	List<int[]> laInts2
	List<List<List<Integer>>> lllInts
	int[][][] aaaInts
	
	Enum1 enum1
	List<Enum1> enumList
	Map<String, Enum1> enumMap
	Map<String, Map<Integer, List<Enum1>>> enumMapMapList
	
	@JsonFormat(shape = JsonFormat.Shape.STRING) Enum1 enum1_s
	@JsonFormat(shape = JsonFormat.Shape.NUMBER) Enum1 enum1_num
	@JsonFormat(shape = JsonFormat.Shape.NATURAL) Enum1 enum1_nat
	@JsonFormat(shape = JsonFormat.Shape.SCALAR) Enum1 enum1_scal
	@JsonFormat(shape = JsonFormat.Shape.ARRAY) Enum1 enum1_arr
}

@GenerateEntity
class EntityB {
	EntityA a
	EntityA[] aa
	EntityA[][] aaa
	List<EntityA> la1
	List<? extends EntityA> la2
	Set<EntityA> la3
	List<EntityA>[] la4
	List<? extends EntityA>[] la5
	Set<EntityA>[] la6
}

class X1<X, Y> {}

interface X2<X> {}

@GenerateEntity
class Entity1<A, B, C extends X1<String, Number> & X2<X1<String, List<String>>>> {
	int i
	long l
	String s
	C c
	Entity2<A, String, String> entity2
	List<Entity2<Integer, A, X2<String>>> entities2
}

@GenerateEntity
class Entity2<X, Y, Z> {
	List<Node<?>> nodeList
	List<? extends Node<?>> nodeList2
	List<NodeWithParameter<String>> nodeList3
}

@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.PROPERTY, property="discriminator")
@JsonSubTypes(#[
	@Type(StringNode),
	@Type(IntegerNode),
	@Type(NodeWithParameter)
])
@XmlRootElement @XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso(#[StringNode, IntegerNode, NodeWithParameter])
@GenerateEntity
abstract class Node<T> {
	T value
}

class S {}

@GenerateEntity
class StringNode extends Node<S> {
}

@GenerateEntity
class IntegerNode extends Node<Integer> {
}

@GenerateEntity
class NodeWithParameter<P> extends Node<P> {
}
