package es.rick.activeannotations.example.typescript

import es.rick.activeannotations.TypeScriptImports
import es.rick.activeannotations.TypeScriptService
import es.rick.activeannotations.TypeScriptServices
import java.util.List
import java.util.Set
import java.util.Map

@TypeScriptServices(projectRoot="src/main/resources/static/", filePath="test/services.ts", restServices=@TypeScriptService(TypeScriptTestInterface), entityImports=@TypeScriptImports(annotatedTypes=TypeScriptEntitiesExample))
class TypeScriptServicesExample {
}

interface TypeScriptTestInterface {
	def EntityA a()

	def EntityA[] aa()

	def EntityA[][] aaa()

	def List<EntityA> la1()

	def List<? extends EntityA> la2()

	def Set<EntityA> la3()

	def List<EntityA>[] la4()

	def List<? extends EntityA>[] la5()

	def Set<EntityA>[] la6(EntityA[] aas
		//, EntityB[][] bs, Map<String, Map<String, EntityB[]>> bMapMap, List<Set<EntityB>> bListSet
		)
}
