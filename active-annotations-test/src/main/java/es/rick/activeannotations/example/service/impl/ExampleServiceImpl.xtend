package es.rick.activeannotations.example.service.impl

import es.rick.activeannotations.example.service.ExampleService
import es.rick.activeannotations.example.typescript.EntityA
import es.rick.activeannotations.example.typescript.Enum1

class ExampleServiceImpl implements ExampleService {
	override EntityA entityA() {
		return new EntityA => [
			stringMap = newHashMap(
				"a" -> "b",
				"c" -> "d"
			)
			enum1_s = Enum1.A
			enum1_num = Enum1.A
			enum1_nat = Enum1.A
			enum1_scal = Enum1.A
			enum1_arr = Enum1.A
		]
	}
}
