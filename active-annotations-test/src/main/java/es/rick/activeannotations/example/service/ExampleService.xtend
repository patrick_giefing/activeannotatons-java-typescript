package es.rick.activeannotations.example.service

import es.rick.activeannotations.example.typescript.EntityA
import javax.ws.rs.Path
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import javax.ws.rs.GET
import org.springframework.web.bind.annotation.GetMapping

@Path(ExampleService.PATH)
@RequestMapping(ExampleService.PATH)
@ResponseBody
interface ExampleService {
	String PATH = "example"

	@GET
	@Path("entityA")
	@GetMapping("entityA")
	def EntityA entityA()
}
