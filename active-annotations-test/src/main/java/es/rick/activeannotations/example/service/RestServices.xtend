package es.rick.activeannotations.example.service

import es.rick.activeannotations.GenerateParameterizedServiceDelegates
import es.rick.activeannotations.GenerateRestClient
import es.rick.activeannotations.GenerateRestClients
import es.rick.activeannotations.NamePattern
import es.rick.activeannotations.ParameterizedDelegate
import es.rick.activeannotations.ParameterizedDelegates
import es.rick.activeannotations.RestClient
import javax.ws.rs.Path

@RestClient( //
namePattern=@NamePattern(suffix="Client"), //
delegates=#[
	@ParameterizedDelegate( //
	attributes=#["idSession"], //
	namePattern=@NamePattern(replace="SessionRestService1a"), //
	generationTarget=INHERIT)
], //
generationTarget=INNERCLASS)
@Path("RestService1")
interface RestService1 {
	def String method1(String idSession)
}

@GenerateRestClient( //
interfaceClass=RestService1, // interfaceName,
restClient=@RestClient(namePattern=@NamePattern(suffix="Client1"), delegates=#[
	@ParameterizedDelegate( //
	attributes=#["idSession"], //
	namePattern=@NamePattern(replace="SessionRestService1b"), //
	generationTarget=INHERIT),
	@ParameterizedDelegate( //
	attributes=#["idSession"], //
	namePattern=@NamePattern(replace="SessionRestService1c"), //
	generationTarget=INHERIT)
], //
generationTarget=INNERCLASS))
class InterfaceGenerator1 {
}

@GenerateRestClients( //
	value=#[@GenerateRestClient( //
	//interfaceClass=RestService1, // 
	interfaceName="es.rick.activeannotations.example.service.RestServiceJava",
	restClient=@RestClient(delegates=#[
		@ParameterizedDelegate( //
		attributes=#["idSession"], //
		namePattern=@NamePattern(replace="SessionRestService2"), //
		generationTarget=INHERIT)
	], //
	namePattern=@NamePattern(suffix="Client2"),
	generationTarget=INNERCLASS))],
	generationTarget=CLASS
)
class InterfaceGenerator2 {
}

@ParameterizedDelegate( //
attributes=#["idSession"], //
namePattern=@NamePattern(replace="RestSessionService2"), //
generationTarget=INHERIT //
)
interface RestService2 {
	def String method2(String idSession)
}

@ParameterizedDelegates( //
delegates=#[ //
	@ParameterizedDelegate( //
	attributes=#["idSession"], //
	namePattern=@NamePattern(replace="RestSessionService2"), //
	generationTarget=INHERIT //
	)
])
interface RestService3 {
	def String method3(String idSession)
}

@GenerateParameterizedServiceDelegates(interfaceClass=RestService1, // interfaceName
delegates=@ParameterizedDelegates(delegates=@ParameterizedDelegate(attributes=#[
	"idSession"], namePattern=@NamePattern(replace="ABC1"))))
class GenerateDelegate2 {
}
