package es.rick.activeannotations.example.launcher

import es.rick.activeannotations.example.service.ExampleService
import es.rick.activeannotations.example.service.impl.ExampleServiceImpl
import java.io.IOException
import javax.servlet.http.HttpServletResponse
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType
import org.springframework.messaging.simp.config.MessageBrokerRegistry
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.springframework.web.servlet.resource.PathResourceResolver
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker
import org.springframework.web.socket.config.annotation.StompEndpointRegistry
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer

@SpringBootApplication(exclude=#[
	SecurityAutoConfiguration,
	DataSourceAutoConfiguration,
	HibernateJpaAutoConfiguration
])
@Import(WebSocketConfig)
@EnableScheduling
@EnableAsync
class ActiveAnnotationsSpringBootLauncher extends SpringBootServletInitializer {
	def static void main(String[] args) {
		SpringApplication.run(ActiveAnnotationsSpringBootLauncher, args)
	}

	@Bean
	def WelcomeFileRedirector welcomeFileRedirector() {
		new WelcomeFileRedirector
	}

	@Bean
	def ExampleService exampleService() {
		new ExampleServiceImpl
	}

	@Import(WebMvcInitializer)
	@EnableWebSocketMessageBroker
	static class WebSocketConfig implements WebSocketMessageBrokerConfigurer {
		override void configureMessageBroker(MessageBrokerRegistry config) {
			config.enableSimpleBroker("/topic")
			config.setApplicationDestinationPrefixes("/app")
		}

		override void registerStompEndpoints(StompEndpointRegistry registry) {
			registry.addEndpoint("/websocket").setAllowedOrigins("http://127.0.0.1:3000", "http://localhost:3000").
				withSockJS()
		}
	}

	@EnableWebMvc
	static class WebMvcInitializer implements WebMvcConfigurer {
		override void addCorsMappings(CorsRegistry registry) {
			registry.addMapping("/**")
		}

		override void addResourceHandlers(ResourceHandlerRegistry registry) {
			registry.addResourceHandler("/**").addResourceLocations("classpath:/static/").setCachePeriod(0).
				resourceChain(true).addResolver(new PathResourceResolver())
		}

		override void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
			configurer.defaultContentType(MediaType.APPLICATION_JSON_UTF8)
		}
	}

	@RequestMapping("/")
	@ResponseBody
	static class WelcomeFileRedirector {
		@RequestMapping("/")
		def void redirect(HttpServletResponse response) throws IOException {
			// Alternativ: https://stackoverflow.com/questions/21123437/how-do-i-use-spring-boot-to-serve-static-content-located-in-dropbox-folder
			response.sendRedirect("index.html")
		}
	}
}
