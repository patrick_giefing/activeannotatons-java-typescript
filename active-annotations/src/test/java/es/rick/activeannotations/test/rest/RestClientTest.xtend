package es.rick.activeannotations.test.rest

import es.rick.activeannotations.test.CodeGenerationTest
import org.junit.Test
import static extension org.junit.Assert.*

class RestClientTest extends CodeGenerationTest {
	@Test
	def void defuncRestClientGeneration() {
		val sDef = '''
package bla.blubb
		
import es.rick.activeannotations.GenerateParameterizedServiceDelegates
import es.rick.activeannotations.GenerateRestClient
import es.rick.activeannotations.GenerateRestClients
import es.rick.activeannotations.GenerationTarget
import es.rick.activeannotations.NamePattern
import es.rick.activeannotations.ParameterizedDelegate
import es.rick.activeannotations.ParameterizedDelegates
import es.rick.activeannotations.RestClient
import javax.ws.rs.Path
import javax.ws.rs.GET
		
@RestClient( //
namePattern=@NamePattern(suffix="Client"), //
delegates=#[
	@ParameterizedDelegate( //
	attributes=#["idSession"], //
	namePattern=@NamePattern(replace="SessionRestService1a"), //
	generationTarget=INHERIT)
], //
generationTarget=INNERCLASS)
@Path("RestService1")
interface RestService1 {
	def String method1(String idSession)
}

@GenerateRestClient( //
interfaceClass=RestService1, // interfaceName,
restClient=@RestClient(namePattern=@NamePattern(suffix="Client1"), delegates=#[
	@ParameterizedDelegate( //
	attributes=#["idSession"], //
	namePattern=@NamePattern(replace="SessionRestService1b"), //
	generationTarget=INHERIT),
	@ParameterizedDelegate( //
	attributes=#["idSession"], //
	namePattern=@NamePattern(replace="SessionRestService1c"), //
	generationTarget=INHERIT)
], //
generationTarget=INNERCLASS))
class InterfaceGenerator1 {
}

@GenerateRestClients( //
	value=#[@GenerateRestClient( //
	//interfaceClass=RestService1, // 
	interfaceName="es.rick.activeannotations.test.rest.RestServiceJava",
	restClient=@RestClient(delegates=
		@ParameterizedDelegate( //
		attributes=#["idSession"], //
		namePattern=@NamePattern(replace="SessionRestService2"), //
		generationTarget=INHERIT)
	, //
	namePattern=@NamePattern(suffix="Client2"),
	generationTarget=INNERCLASS))],
	generationTarget=CLASS
)
class InterfaceGenerator2 {
}
		'''
		printAndCompile(sDef, [
			allProblems.nullOrEmpty.assertTrue
		])
	}
	
	@Test
	def void toStringAlreadyExists() {
		val sClass = '''
package es.rick.activeannotations.example.service

import javax.ws.rs.GET
import javax.ws.rs.Path
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import es.rick.activeannotations.RestClient

import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonSubTypes.Type
import com.fasterxml.jackson.annotation.JsonTypeInfo
import es.rick.activeannotations.GenerateEntity
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlRootElement
import javax.xml.bind.annotation.XmlSeeAlso
import java.util.Set
import java.util.List

@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.PROPERTY, property="typeDiscriminator")
@JsonSubTypes(#[
	@Type(Cat),
	@Type(Dog)
])
@XmlRootElement @XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso(#[Cat, Dog])
@GenerateEntity
class Pet {
	String name
	int age
}

@GenerateEntity
class Cat extends Pet {
	int lifes
}

@GenerateEntity
class Dog extends Pet {
	int postmenBitten
}

@GenerateEntity
class CatsAndDogs extends Pet {
	List<Cat> cats
	Set<Dog> dogs
}


@Path(PetService.PATH)
@RequestMapping(PetService.PATH)
@ResponseBody
@RestClient
interface PetService {
	String PATH = "pets"
	String PATH_PETS = "all"
	String PATH_CATS = "cats"
	String PATH_DOGS = "dogs"
	String PATH_CATS_AND_DOGS = "cat-and-dogs"

	@Path(PATH_PETS) @GET @GetMapping(PATH_PETS)
	def List<? extends Pet> getPets()

	@Path(PATH_CATS) @GET @GetMapping(PATH_CATS)
	def List<Cat> getCats()

	@Path(PATH_DOGS) @GET @GetMapping(PATH_DOGS)
	def List<Dog> getDogs()

	@Path(PATH_CATS_AND_DOGS) @GET @GetMapping(PATH_CATS_AND_DOGS)
	def CatsAndDogs getCatsAndDogs()
}
		'''
		printAndCompile(sClass, [
			val clazz = getCompiledClass("ClassWithExistingToString")
			val result = clazz.newInstance.toString
			"whoop".assertEquals(result)
		])
	}
}