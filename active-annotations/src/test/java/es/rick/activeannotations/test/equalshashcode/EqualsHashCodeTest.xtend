package es.rick.activeannotations.test.equalshashcode

import es.rick.activeannotations.GenerateEntity
import es.rick.activeannotations.test.CodeGenerationTest
import javassist.ClassClassPath
import javassist.ClassPool
import javassist.Modifier
import org.junit.Test

import static extension org.junit.Assert.*
import javax.management.MBeanPermission
import java.security.Permissions
import java.security.ProtectionDomain
import es.rick.activeannotations.ToStringConfig

class EqualsHashCodeTest extends CodeGenerationTest {
	val private static String GENERATE_ENTITY_EQUALS_HASHCODE_ONLY = '''@�GenerateEntity.name�(toStringConfig=@�ToStringConfig.name�(generateToString=false))'''
	
	@Test
	def void simpleNoParent() {
		val sClass = '''
		�GENERATE_ENTITY_EQUALS_HASHCODE_ONLY�
		class SimpleNoParent {}'''
		printAndCompile(sClass, [
			allProblems.empty.assertTrue
		])
	}

	@Test
	def void simpleParent() {
		val sClass = '''
		�GENERATE_ENTITY_EQUALS_HASHCODE_ONLY�
		class Parent {}
		
		�GENERATE_ENTITY_EQUALS_HASHCODE_ONLY�
		class Child extends Parent {}'''
		printAndCompile(sClass, [
			allProblems.empty.assertTrue
			val clazzParent = getCompiledClass("Parent")
			val clazzChild = getCompiledClass("Child")
			val parent1 = clazzParent.newInstance
			val parent2 = clazzParent.newInstance
			val child1 = clazzChild.newInstance
			val child2 = clazzChild.newInstance

			val pool = ClassPool.^default => [
				insertClassPath(new ClassClassPath(clazzParent))
				insertClassPath(new ClassClassPath(clazzChild))
			]
			val ctClazzDynChild = pool.makeClass("DynamicChild")
			ctClazzDynChild.superclass = pool.get(clazzChild.name)
			ctClazzDynChild.modifiers = Modifier.PUBLIC

			val protectionDomain = new ProtectionDomain(null, new Permissions => [
				add(new MBeanPermission("*", null, null, "getClassLoader"))
			])
			val clazzDynChild = ctClazzDynChild.toClass(clazzChild.classLoader, protectionDomain)
			val dynChild1 = clazzDynChild.newInstance
			val dynChild2 = clazzDynChild.newInstance

			child1.equals(parent1).assertFalse
			parent1.equals(child1).assertFalse
			child1.equals(parent1).assertFalse
			parent1.equals(parent2).assertTrue
			child1.equals(child2).assertTrue
			dynChild1.equals(dynChild2).assertTrue
			child1.class.isAssignableFrom(clazzDynChild).assertTrue
			child1.equals(dynChild1).assertTrue
			dynChild1.equals(child1).assertTrue
		])
	}
}
