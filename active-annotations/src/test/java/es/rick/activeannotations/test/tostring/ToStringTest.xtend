package es.rick.activeannotations.test.tostring

import es.rick.activeannotations.test.CodeGenerationTest
import org.junit.Test
import es.rick.activeannotations.GenerateEntity
import static extension org.junit.Assert.*

class ToStringTest extends CodeGenerationTest {
	val private static String GENERATE_ENTITY_EQUALS_HASHCODE_ONLY = '''@�GenerateEntity.name�(generateEqualsAndHashCode=false)'''
	
	/**
	 * If toString already exists, we don't replace it, so "whoop" should be returned from existing function.
	 */
	@Test
	def void toStringAlreadyExists() {
		val sClass = '''
		�GENERATE_ENTITY_EQUALS_HASHCODE_ONLY�
		class ClassWithExistingToString {
			override String toString() {return "whoop";}
		}'''
		printAndCompile(sClass, [
			val clazz = getCompiledClass("ClassWithExistingToString")
			val result = clazz.newInstance.toString
			"whoop".assertEquals(result)
		])
	}

	/**
	 * No attributes defined in class but as there is no predefined toString we should get the result of
	 * the ToStringBuilder.
	 */
	@Test
	def void toStringClassWithoutAttributes() {
		val sClass = '''
		�GENERATE_ENTITY_EQUALS_HASHCODE_ONLY�
		class ClassWithoutAttributes {}'''
		printAndCompile(sClass, [
			val clazz = getCompiledClass("ClassWithoutAttributes")
			val result = clazz.newInstance.toString.replaceWs
			"ClassWithoutAttributes [ ]".assertEquals(result)
		])
	}
}
