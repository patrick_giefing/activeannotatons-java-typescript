package es.rick.activeannotations.test

import org.eclipse.xtend.core.compiler.batch.XtendCompilerTester
import org.eclipse.xtext.util.IAcceptor
import org.eclipse.xtend.core.compiler.batch.XtendCompilerTester.CompilationResult
import java.util.regex.Pattern

abstract class CodeGenerationTest {
	val static Pattern REPLACE_WS = Pattern.compile('''\s+''')
	extension XtendCompilerTester compilerTester = XtendCompilerTester.newXtendCompilerTester(class.classLoader)

	def protected void printAndCompile(CharSequence source, IAcceptor<CompilationResult> acceptor) {
		println('''input: �source�''')
		compilerTester.compile(source, [
			println(allProblems?.map['''�severity�-�id�:�message�'''])
			println(singleGeneratedCode)
			acceptor.accept(it)
		])
	}

	def static String replaceWs(String s) {
		REPLACE_WS.matcher(s).replaceAll(" ")
	}
}
