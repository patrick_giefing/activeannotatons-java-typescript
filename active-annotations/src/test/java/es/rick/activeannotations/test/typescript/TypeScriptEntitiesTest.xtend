package es.rick.activeannotations.test.typescript

import es.rick.activeannotations.test.CodeGenerationTest
import org.junit.Test
import static extension org.junit.Assert.*

class TypeScriptEntitiesTest extends CodeGenerationTest {
	@Test
	def void toStringAlreadyExists() {
		printAndCompile(entities, [
			val clazz = getCompiledClass("es.rick.activeannotations.example.typescript.NodeWithParameter")
			clazz.assertNotNull
		])
	}
	
	val entities = '''
package es.rick.activeannotations.example.typescript

import es.rick.activeannotations.GenerateEntity
import java.util.Date
import java.util.List
import java.util.Set
import es.rick.activeannotations.TypeScriptEntities
import es.rick.activeannotations.Entities
import java.time.LocalDate
import java.time.LocalTime
import java.time.LocalDateTime
import com.fasterxml.jackson.annotation.JsonFormat
import java.util.Map

@TypeScriptEntities(projectRoot="src/main/resources/static/", filePath="test/entities.ts", entities=@Entities(entityClasses=#[
	Entity1,
	Entity2,
	S,
	Node,
	StringNode,
	IntegerNode
]))
class TypeScriptEntitiesExample {
}

class X1<X, Y> {}

interface X2<X> {}

@GenerateEntity
class Entity1<A, B, C extends X1<String, Number> & X2<X1<String, List<String>>>> {
	int i
	long l
	String s
	C c
	Entity2<A, B, String> entity2
	//List<Entity2<Integer, A, String>> entities2
	List<Set<Entity2[]>> listSetArrayEntities2
}

@GenerateEntity
class Entity2<X, Y, Z> {
	List<Node<?>> nodeList
	List<? extends Node<?>> nodeList2
	List<NodeWithParameter<String>> nodeList3
}

@GenerateEntity
abstract class Node<T> {
	T value
}

class S {}

@GenerateEntity
class StringNode extends Node<S> {
}

@GenerateEntity
class IntegerNode extends Node<Integer> {
}

@GenerateEntity
class NodeWithParameter<P> extends Node<P> {
}
'''
}