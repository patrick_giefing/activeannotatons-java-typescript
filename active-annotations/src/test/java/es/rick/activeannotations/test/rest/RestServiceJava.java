package es.rick.activeannotations.test.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("java")
public interface RestServiceJava {
	@Path("bla") @GET
	public void bla();
}
