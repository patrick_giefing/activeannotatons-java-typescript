package es.rick.activeannotations.processor.typescript.test

import org.junit.Test
import static extension org.junit.Assert.*
import static extension es.rick.activeannotations.processor.typescript.TypeScriptImporter.*

class TypeScriptImporterTest {
	@Test
	def void translateRelative1LevelBoth() {
		"./entities".assertEquals("test/entities".translateRelative("test/services"))
	}

	@Test
	def void translateRelative3LevelsBoth() {
		"./entities".assertEquals("a/b/c/entities".translateRelative("a/b/c/services"))
	}

	@Test
	def void translateRelative4Levels2Levels() {
		"./c/d/entities".assertEquals("a/b/c/d/entities".translateRelative("a/b/services"))
	}

	@Test
	def void translateRelative2Levels4Levels() {
		"../../entities".assertEquals("a/b/entities".translateRelative("a/b/c/d/services"))
	}

	@Test
	def void translateRelative4Levels2LevelsDiff() {
		"../../a1/b1/c/d/entities".assertEquals("a1/b1/c/d/entities".translateRelative("a2/b2/services"))
	}
}
