package es.rick.activeannotations

import es.rick.activeannotations.processor.delegate.ParameterizedDelegateProcessor
import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target
import org.eclipse.xtend.lib.macro.Active

@Target(ElementType.TYPE)
@Active(ParameterizedDelegateProcessor)
@Retention(RetentionPolicy.RUNTIME)
annotation GenerateParameterizedServicesDelegates {
	GenerateParameterizedServiceDelegates[] servicesDelegates
}

@Target(ElementType.TYPE)
@Active(ParameterizedDelegateProcessor)
@Retention(RetentionPolicy.RUNTIME)
annotation GenerateParameterizedServiceDelegates {
	/**
	 * Type that we generate parameterized delegates for. If this
	 * type exists in the same project, then interfaceClass must be used.
	 */
	Class<?> interfaceClass = Object
	/**
	 * If source type and delegate are in the same project then
	 * interfaceName must be used in order to register the new types
	 * because types in the same project cannot be found in this phase.
	 */
	String interfaceName = ""
	ParameterizedDelegates delegates
}

@Target(ElementType.TYPE)
@Active(ParameterizedDelegateProcessor)
@Retention(RetentionPolicy.RUNTIME)
annotation ParameterizedDelegates {
	GenerationTarget generationTarget = GenerationTarget.INNERCLASS
	ParameterizedDelegate[] delegates
}

@Target(ElementType.TYPE)
@Active(ParameterizedDelegateProcessor)
@Retention(RetentionPolicy.RUNTIME)
annotation ParameterizedDelegate {
	String[] attributes
	NamePattern namePattern = @NamePattern(suffix="Delegate")
	GenerationTarget generationTarget = GenerationTarget.INHERIT
	GenerateInterface[] generatedInterfaces = #[]
}

@Target(ElementType.ANNOTATION_TYPE)
@Retention(RetentionPolicy.RUNTIME)
annotation NamePattern {
	String prefix = ""
	String suffix = ""
	String regex = ""
	String replace = ""
}

enum GenerationTarget {
	CLASS,
	INNERCLASS,
	INHERIT
}
