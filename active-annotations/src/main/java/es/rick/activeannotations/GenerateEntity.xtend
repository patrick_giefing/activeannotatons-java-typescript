package es.rick.activeannotations

import es.rick.activeannotations.processor.DeleteTypeProcessor
import es.rick.activeannotations.processor.entity.EntityProcessor
import es.rick.activeannotations.processor.entity.GenerateInterfaceProcessor
import es.rick.activeannotations.processor.entity.group.EntityGroupProcessor
import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target
import org.eclipse.xtend.lib.macro.Active

@Target(ElementType.TYPE)
@Active(DeleteTypeProcessor)
@Retention(RetentionPolicy.RUNTIME)
annotation DeleteType {}

/**
 * @see IgnoreToString
 * @see IgnoreEqualsHashCode
 * @see Singular
 * @see DefaultType
 */
@Target(ElementType.TYPE)
@Active(EntityProcessor)
@Retention(RetentionPolicy.RUNTIME)
annotation GenerateEntity {
	boolean generateBuilder = false
	ToStringConfig toStringConfig = @ToStringConfig
	/**
	 * @see https://www.artima.com/lejava/articles/equality.html
	 * @return
	 */
	boolean generateEqualsAndHashCode = true
	boolean generateFieldsEnum = false
	CollectionMethodsConfig collectionMethodsConfig = @CollectionMethodsConfig
	boolean cloneable = true
	boolean serializable = true
	AccessorsConfig accessors = @AccessorsConfig
	EntityConstructor[] constructors = #[]
	JpaConfig jpaConfig = @JpaConfig
	JaxbConfig jaxbConfig = @JaxbConfig
	/**
	 * Sorts attributes for toString/jsonSerialization
	 */
	PropertyOrder[] propertyOrder = #[]
	GenerateInterface[] generateInterfaces = #[]
}

annotation AccessorsConfig {
	AccessorType getter = AccessorType.PUBLIC
	AccessorType setter = AccessorType.PUBLIC
}

annotation ToStringConfig {
	boolean generateToString = true
	boolean skipNulls = false
	boolean singleLine = false
	boolean hideFieldNames = false
	boolean verbatimValues = false
}

@Retention(RetentionPolicy.RUNTIME)
annotation EntityConstructor {
	String[] value
}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
annotation GenerateUuidOnPrePersist {
}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
annotation SetNowOnPrePersist {
	boolean onlySetIfNull = true
}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
annotation SetNowOnPreUpdate {
	boolean onlySetIfNull = true
}

@Retention(RetentionPolicy.RUNTIME)
annotation JpaConfig {
	CompositeIdType compositeIdType = CompositeIdType.EmbeddedId
}

@Retention(RetentionPolicy.RUNTIME)
annotation JaxbConfig {
	boolean xmlAttributes = false
	boolean xmlElementWrappers = false
}

enum PropertyOrder {
	ParentFirst,
	ParentLast,
	AlphabeticalAsc,
	AlphabeticalDesc,
	IdsFirst,
	IdsLast,
	PrimitiveFirst,
	PrimitiveLast
}

@Target(ElementType.TYPE)
@Active(EntityGroupProcessor)
@Retention(RetentionPolicy.RUNTIME)
annotation EntityGroup {
	Class<?>[] entities
	String[] attributeNameWhnitelist = #[]
	boolean appendCompilationUnitTypes = false
}

@Retention(RetentionPolicy.RUNTIME)
annotation Entities {
	Class<?>[] entityClasses = #[]
	EntityGroup[] entityGroups = #[]
	Class<?>[] entityGroupAnnotatedClasses = #[]
}

@Retention(RetentionPolicy.RUNTIME)
annotation IgnoreToString {
}

@Retention(RetentionPolicy.RUNTIME)
annotation IgnoreEqualsHashCode {
}

@Retention(RetentionPolicy.RUNTIME)
annotation DefaultType {
	Class<?> value
}

enum AccessorType {
	PUBLIC,
	PROTECTED,
	PACKAGE,
	PRIVATE,
	NONE
}

enum CompositeIdType {
	IdClass,
	EmbeddedId,
	NoCompositeId
}

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
annotation Singular {
	String value
}

annotation CollectionMethodsConfig {
	boolean generateCollectionMethods = true
	boolean recreateOnUnsupportedOperationException = true
}

@Target(ElementType.TYPE)
@Active(GenerateInterfaceProcessor)
@Retention(RetentionPolicy.RUNTIME)
annotation GenerateInterfaces {
	GenerateInterface[] value
}

@Target(ElementType.TYPE)
@Active(GenerateInterfaceProcessor)
@Retention(RetentionPolicy.RUNTIME)
annotation GenerateInterface {
	/**
	 * Extended interfaces. For interfaces which exist in the same project,
	 * interfaceNames must be used. You can mix both attributes.
	 */
	Class<?>[] interfaces = #[]
	/**
	 * If source type and delegate are in the same project then
	 * interfaceNames must be used in order to register the new types
	 * because types in the same project cannot be found in this phase.
	 */
	String[] interfaceNames = #[]
	NamePattern namePattern = @NamePattern(suffix="Definition")
	MethodPattern[] includeMethodPattern = #[]
	MethodPattern[] excludeMethodPattern = #[]
}

annotation MethodPattern {
	String prefix = ""
	String suffix = ""
	String pattern = ""
}
