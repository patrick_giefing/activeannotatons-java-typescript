package es.rick.activeannotations

import es.rick.activeannotations.processor.typescript.TypeScriptEntitiesProcessor
import es.rick.activeannotations.processor.typescript.TypeScriptServicesProcessor
import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target
import org.eclipse.xtend.lib.macro.Active

@Target(ElementType.TYPE)
@Active(TypeScriptEntitiesProcessor)
@Retention(RetentionPolicy.RUNTIME)
annotation TypeScriptEntities {
	String projectRoot
	String filePath
	TypeScriptImports entityImports = @TypeScriptImports
	Entities entities
	boolean omitJsonIgnoreFields = true
}

@Target(ElementType.TYPE)
@Active(TypeScriptServicesProcessor)
@Retention(RetentionPolicy.RUNTIME)
annotation TypeScriptServices {
	String projectRoot
	String filePath
	TypeScriptImports entityImports = @TypeScriptImports
	TypeScriptService[] restServices = #[]
	TypeScriptService[] webSocketSubscriberServices = #[]
	TypeScriptService[] webSocketRpcServices = #[]
}

annotation TypeScriptService {
	Class<?> value
	ParameterizedDelegate[] delegates = #[]
}

@Retention(RetentionPolicy.RUNTIME)
annotation TypeScriptImports {
	TypeScriptFileImport[] fileImports = #[]
	Class<?>[] annotatedTypes = #[]
}

@Retention(RetentionPolicy.RUNTIME)
annotation TypeScriptFileImport {
	String filePath
	Entities entities
}
