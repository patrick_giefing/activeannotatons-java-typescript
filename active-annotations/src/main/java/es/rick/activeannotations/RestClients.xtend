package es.rick.activeannotations

import es.rick.activeannotations.processor.rest.RestClientsProcessor
import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target
import org.eclipse.xtend.lib.macro.Active

@Target(ElementType.TYPE)
@Active(RestClientsProcessor)
@Retention(RetentionPolicy.RUNTIME)
annotation GenerateRestClients {
	GenerateRestClient[] value = #[]
	GenerationTarget generationTarget = GenerationTarget.INNERCLASS
}

@Target(ElementType.TYPE)
@Active(RestClientsProcessor)
@Retention(RetentionPolicy.RUNTIME)
annotation GenerateRestClient {
	Class<?> interfaceClass = Object
	String interfaceName = ""
	RestClient restClient
}

@Target(ElementType.TYPE)
@Active(RestClientsProcessor)
@Retention(RetentionPolicy.RUNTIME)
annotation RestClient {
	NamePattern namePattern = @NamePattern(suffix="Client")
	ParameterizedDelegate[] delegates = #[]
	GenerationTarget generationTarget = GenerationTarget.INNERCLASS
}
