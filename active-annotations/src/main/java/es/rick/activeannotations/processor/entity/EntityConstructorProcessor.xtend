package es.rick.activeannotations.processor.entity

import org.eclipse.xtend.lib.macro.declaration.MutableClassDeclaration
import org.eclipse.xtend.lib.macro.TransformationContext
import org.eclipse.xtext.xbase.lib.Functions.Function1
import org.eclipse.xtend.lib.macro.declaration.FieldDeclaration
import java.util.TreeMap
import java.util.Map
import org.eclipse.xtend.lib.macro.declaration.MethodDeclaration

class EntityConstructorProcessor extends EntityBaseProcessor {
	val static Function1<FieldDeclaration, Boolean> filterFields = [!static && !final && !transient]

	override transformEntity(extension MutableClassDeclaration clazz, extension TransformationContext context,
		extension EntityProcessorConfig entityConfig) {
		if (entityConfig.constructorConfigs.nullOrEmpty) {
			return
		}
		val createDefaultConstructor = true
		val createAllFieldsConstructor = false
		val constructorConfigs = entityConfig.constructorConfigs
		val allFields = constructorConfigs.map[properties.toList].flatten.toSet
		val publicMemberMethods = clazz.getAllPublicMemberMethods(context)
		val Map<String, MethodDeclaration> fieldSetterMap = new TreeMap
		allFields.forEach [ field |
			val setter = publicMemberMethods.findFirst [
				simpleName == '''set«field.toFirstUpper»'''.toString && parameters.size === 1
			]
			if (setter !== null) {
				fieldSetterMap.put(field, setter)
			}
		]
		val settersNotFound = allFields.filter[!fieldSetterMap.containsKey(it)]
		if (!settersNotFound.empty) {
			clazz.
				addError('''«EntityConstructorProcessor.simpleName»: No setters found for the following fields: «settersNotFound.join(", ")»''')
			return
		}
		// Search for constructors with identical field type list
		val constructorParametersGroups = constructorConfigs.groupBy [
			properties.map[clazz.findDeclaredField(it)]?.map[it?.type?.simpleName]?.filterNull?.join(", ")
		]
		val parameterListsWithMultipleConstructors = constructorParametersGroups.entrySet.filter[value.size > 1]
		if (!parameterListsWithMultipleConstructors.empty) {
			parameterListsWithMultipleConstructors.forEach [
				val message = '''«EntityConstructorProcessor.simpleName»: The parameter list [«key»] is used in multiple constuctors: «value.map['''[«properties.join(", ")»]'''].join(", ")»'''
				clazz.addError(message)
			]
			return
		}
		if (createAllFieldsConstructor) {
			clazz.addConstructor [ c |
				val fields = clazz.declaredFields.filter(filterFields)
				fields.forEach[c.addParameter(simpleName, type)]
				c.body = '''«fields.map['''this.«simpleName» = «simpleName»;'''].join("\r\n")»'''
			]
		}
		constructorConfigs.forEach [ constructor |
			clazz.addConstructor [ c |
				// If not field are set then generate a default constructor except it will be generated anyway.
				if (constructor.properties.empty && createDefaultConstructor) {
					return
				}
				constructor.properties.forEach [ property |
					val setter = fieldSetterMap.get(property)
					c.addParameter(property, setter.parameters.head.type)
				]
				c.body = '''«constructor.properties.map['''this.set«toFirstUpper»(«it»);'''].join("\r\n")»'''
			]
		]
		if (createDefaultConstructor) {
			clazz.addConstructor[]
		}
	}
}
