package es.rick.activeannotations.processor.entity

import es.rick.activeannotations.EntityGroup
import es.rick.activeannotations.processor.entity.group.EntityGroupProcessorConfig
import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtend.lib.annotations.ToString
import org.eclipse.xtend.lib.macro.declaration.AnnotationReference
import org.eclipse.xtend.lib.macro.declaration.ClassDeclaration
import org.eclipse.xtend.lib.macro.declaration.TypeReference

@ToString @Accessors
class EntitiesProcessorConfig {
	val TypeReference[] entityClasses
	val EntityGroupProcessorConfig[] entityGroups

	new(AnnotationReference ann) {
		entityClasses = ann.getClassArrayValue("entityClasses")
		val entityGroups = ann.getAnnotationArrayValue("entityGroups").map[new EntityGroupProcessorConfig(it)]
		val entityGroupAnnotatedClasses = ann.getClassArrayValue("entityGroupAnnotatedClasses")
		val entityGroupAnnotatedClassesConfigs = entityGroupAnnotatedClasses.filter[it instanceof ClassDeclaration].map [
			it as ClassDeclaration
		].map [
			val annEntityGroup = annotations.findFirst[EntityGroup.name == annotationTypeDeclaration.qualifiedName]
			if (annEntityGroup !== null) {
				return new EntityGroupProcessorConfig(annEntityGroup)
			}
			return null
		].filterNull
		this.entityGroups = #[
			entityGroups,
			entityGroupAnnotatedClassesConfigs
		].flatten
	}

	def List<TypeReference> getAllEntityClasses() {
		val entityGroupClasses = entityGroups.map[entities.toList].flatten
		#[
			entityClasses.toList,
			entityGroupClasses
		].flatten.toList
	}
}
