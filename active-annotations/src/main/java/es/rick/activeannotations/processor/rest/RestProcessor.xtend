package es.rick.activeannotations.processor.rest

import es.rick.activeannotations.processor.delegate.DelegateProcessor
import javax.ws.rs.Consumes
import javax.ws.rs.DELETE
import javax.ws.rs.GET
import javax.ws.rs.POST
import javax.ws.rs.PUT
import javax.ws.rs.Path
import javax.ws.rs.core.MediaType
import org.eclipse.xtend.lib.macro.declaration.AnnotationReference
import org.eclipse.xtend.lib.macro.declaration.AnnotationTarget
import org.eclipse.xtend.lib.macro.declaration.ParameterDeclaration
import org.eclipse.xtend.lib.macro.declaration.TypeReference
import org.eclipse.xtext.xbase.lib.Functions.Function1

class RestProcessor extends DelegateProcessor {
	def static boolean isPost(AnnotationTarget it) {
		annotations.exists[annotationTypeDeclaration.qualifiedName == POST.name]
	}

	def static boolean isPut(AnnotationTarget it) {
		annotations.exists[annotationTypeDeclaration.qualifiedName == PUT.name]
	}

	def static boolean isGet(AnnotationTarget it) {
		annotations.exists[annotationTypeDeclaration.qualifiedName == GET.name]
	}

	def static boolean isDelete(AnnotationTarget it) {
		annotations.exists[annotationTypeDeclaration.qualifiedName == DELETE.name]
	}

	def static String getRsPath(AnnotationTarget it) {
		annotations.findFirst [
			it.annotationTypeDeclaration.qualifiedName == Path.name
		]?.getStringValue("value")
	}

	def static boolean isMultiPart(AnnotationTarget it) {
		val contentTypes = annotations.findFirst [
			it.annotationTypeDeclaration.qualifiedName == Consumes.name
		]?.getStringArrayValue("value")
		return contentTypes?.contains(MediaType.MULTIPART_FORM_DATA)
	}

	val public static Function1<ParameterDeclaration, Boolean> filterParamsWithoutJaxRsAnnotations = [
		annotations.filter[annotationTypeDeclaration.qualifiedName.startsWith("javax.ws.rs.")].empty
	]

	/**
	 * Filtert s�mtliche <code>javax.rs.*</code> und Spring-ReST-Annotations raus.
	 */
	val public static Function1<AnnotationReference, Boolean> filterRestAnnotations = [!restAnnotation]

	def static boolean isRestAnnotation(AnnotationReference ann) {
		ann.annotationTypeDeclaration.qualifiedName.startsWith("javax.ws.rs.") ||
			ann.annotationTypeDeclaration.qualifiedName.startsWith("org.springframework.web.bind.annotation")
	}

	def static encodeUrlPartIfString(String paramName, TypeReference paramType) {
		if (paramType?.type?.qualifiedName == String.name) {
			return paramName
		}
		return '''�paramName� + ""'''
	}
}
