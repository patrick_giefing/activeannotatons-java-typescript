declare var SockJS: any;
declare var Stomp: any;
declare var autobahn: any;

export abstract class WebSocketClient {
  protected subscriptions: Array<WebSocketSubscription<any>> = [];
  protected endpoint: string;
  protected connected: boolean = false;
  protected onConnectListeners: Array<() => void> = [];
  protected onDisconnectListeners: Array<() => void> = [];

  constructor(endpoint: string) {
    this.endpoint = endpoint;
  }

  public registerSubscription(subscription: WebSocketSubscription<any>): void {
    this.subscriptions[this.subscriptions.length] = subscription
    if (this.connected) {
      this.bindSubscription(subscription);
    }
  }

  protected bindSubscriptions(): void {
    for (let subscription of this.subscriptions) {
      this.bindSubscription(subscription);
    }
  }

  protected abstract bindSubscription(subscription: WebSocketSubscription<any>): this;

  protected abstract unbindSubscription(subscription: WebSocketSubscription<any>): this;

  protected callCallback(subscription: WebSocketSubscription<any>, data: any): void {
    try {
      // TODO irgendwie anders feststellen ob es sich �berhaupt um ein JSON-Objekt handelt
      // W�re ung�nstig wenn ein JSON-Response als String erwartet wird und dann aber auch ein Objekt geliefert wird
      data = JSON.parse(data);
    } catch (_ex) {}
    if (subscription.entityConversion) {
      data = subscription.entityConversion(data);
    }
    if (subscription.serviceMethodName) {
      $(document).trigger(subscription.serviceMethodName, data);
    }
    if (subscription.callback) {
      subscription.callback(data);
    };
  }

  public setConnected(connected: boolean): void {
    this.connected = connected;
    console.info(`websocket connected: ${connected}`);
    if (connected) {
      try {
        this.callOnConnectListeners();
      } catch (_ex) {
        console.error("Error while alling onConnectListeners", _ex);
      }
      this.bindSubscriptions();
      $(document).trigger("basis.websocket.connected", {});
    } else {
      $(document).trigger("basis.websocket.disconnected", {});
      try {
        this.callOnDisconnectListeners();
      } catch (_ex) {
        console.error("Error while alling onDisconnectListeners", _ex);
      }
      if (!this.isAutoReconnect()) {
        this.connect();
      }
    }
  }

  protected callOnConnectListeners(): this {
    this.onConnectListeners.forEach(listener => listener());
    return this;
  }

  protected callOnDisconnectListeners(): this {
    this.onDisconnectListeners.forEach(listener => listener());
    return this;
  }

  public addOnConnectListener(listener: () => void): void {
    this.onConnectListeners[this.onConnectListeners.length] = listener;
  }

  public addOnDisconnectListener(listener: () => void): void {
    this.onDisconnectListeners[this.onDisconnectListeners.length] = listener;
  }

  public abstract connect(): void;

  public abstract isAutoReconnect(): boolean;
}

export class WampClient extends WebSocketClient {
  private wampClient: any = null;
  private wampSession: any = null;
  private realm: string = null;

  constructor(endpoint: string, realm: string = "realm1") {
    super(endpoint);
    this.realm = realm;
  }

  public connect(): void {
    let THIS = this;
    this.wampClient = new autobahn.Connection({
      url: THIS.endpoint,
      realm: THIS.realm
    });
    this.wampClient.onopen = (session): void => {
      this.wampSession = session;
      console.info("wamp: connected");
      THIS.setConnected(true);
    };
    this.wampClient.onclose = (reason, details) => {
      THIS.setConnected(false);
      console.info(`wamp close reason: ${reason} - ${JSON.stringify(details)}`);
    };
    this.wampClient.open();
  }

  protected bindSubscription(subscription: WebSocketSubscription<any>): this {
    this.wampSession.subscribe(subscription.topic, (args: Array<any>) => {
      this.callCallback(subscription, args[0]);
    }).then(
      function(subscription) {
        subscription.subscription = subscription;
      }
      );
    let THIS = this;
    subscription.funcCancel = (): void => {
      THIS.unbindSubscription(subscription);
    };
    return this;
  }

  protected unbindSubscription(subscription: WebSocketSubscription<any>): this {
    if (!subscription || !subscription.subscription) {return this;}
    // https://github.com/crossbario/autobahn-js/blob/master/doc/reference.md
    this.wampSession.unsubscribe(subscription.subscription).then(
      function(gone) {
        // successfully unsubscribed sub1
      },
      function(error) {
        // unsubscribe failed
      }
    );
    return this;
  }

  public getSession(): any {
    return this.wampSession;
  }

  public call(topic: string, args: any[], callback: (response: any) => void = null, errorCallback: (error: any) => void = null, afterCallback: () => void = null): this {
    if (!this.wampSession) {
      if (errorCallback) {
        errorCallback(new Error("wampSession not established"));
      }
      return this;
    }
    return this.wampSession.call(topic, args).then((response: any): void => {
      if (callback) {
        try {
          callback(response);
        } catch (ex) {
          console.error("Error while calling success callback", ex);
        }
      }
      if (afterCallback) {
        afterCallback();
      }
    }, (error: any): void => {
      if (errorCallback) {
        try {
          errorCallback(error);
        } catch (ex) {
          console.error("Error while calling error callback", ex);
        }
      }
      if (afterCallback) {
        afterCallback();
      }
    });
  }

  public isAutoReconnect(): boolean {
    return true;
  }
}

export class StompClient extends WebSocketClient {
  private stompClient: any = null;

  constructor(endpoint: string) {
    super(endpoint);
  }

  public connect(): void {
    let THIS = this;
    var socket: any = new SockJS(this.endpoint);
    this.stompClient = Stomp.over(socket);
    console.info(`SockJS: connect to ${this.endpoint}`);
    this.stompClient.connect({}, (frame: any) => {
      THIS.setConnected(true);
      console.info(`connected: ${frame}`);
      // subscribe
    }, (message: string) => {
      console.error(`stomp client disonnected: ${message}`);
      THIS.setConnected(false);
    });
  }

  public disconnect(): void {
    if (this.stompClient) {
      this.stompClient.disconnect();
    }
    this.setConnected(false);
    console.log("Disconnected");
  }

  public reconnect(): void {
    this.disconnect();
    this.connect();
  }

  protected bindSubscription(subscription: WebSocketSubscription<any>): this {
    let sockJsSubscription: any = this.stompClient.subscribe(subscription.topic, (response: any): void => {
      this.callCallback(subscription, response.body);
    });
    subscription.subscription = sockJsSubscription;
    let THIS = this;
    subscription.funcCancel = (): void => {
      THIS.unbindSubscription(subscription);
    };
    return this;
  }

  protected unbindSubscription(subscription: WebSocketSubscription<any>): this {
    if (!subscription || !subscription.subscription) {return this;}
    // https://github.com/stomp-js/ng2-stompjs/issues/2
    subscription.subscription.unsubscribe();
    return this;
  }

  public isAutoReconnect(): boolean {
    return false;
  }
}

/**
 * Wrapper da das unterliegende SocketJS-Objekt nach Neuverbinden ge�ndert wird.
 */
export class WebSocketSubscription<T> {
	/**
	 * z.B. /topic/status/
	 */
  public topic: string;
	/**
	 * SockJS-Subscription-Object
	 */
  public subscription: any;
	/**
	 * Für jQuery-Trigger, z.B. DEMO_SERVICE.BLA_BLUBB
	 */
  public serviceMethodName: string;
  public callback: (data: T) => void;
	/**
	 * Konvertiert das JSON-Response-Objekt in das vom #callback erwartete Entity
	 */
  public entityConversion: (data: any) => T;
  public funcCancel: () => void = null;

  constructor(topic: string = null, callback: (data: T) => void = null) {
    this.topic = topic;
    this.callback = callback;
  }

  public cancel(): this {
    if (this.funcCancel) {
      this.funcCancel();
    }
    return this;
  }
}