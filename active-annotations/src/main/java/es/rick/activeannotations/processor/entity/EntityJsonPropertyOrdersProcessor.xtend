package es.rick.activeannotations.processor.entity

import com.fasterxml.jackson.annotation.JsonPropertyOrder
import es.rick.activeannotations.PropertyOrder
import java.util.List
import org.eclipse.xtend.lib.macro.TransformationContext
import org.eclipse.xtend.lib.macro.declaration.ClassDeclaration
import org.eclipse.xtend.lib.macro.declaration.FieldDeclaration
import org.eclipse.xtend.lib.macro.declaration.MutableClassDeclaration

class EntityJsonPropertyOrdersProcessor extends EntityBaseProcessor {
	override transformEntity(extension MutableClassDeclaration clazz, extension TransformationContext context,
		extension EntityProcessorConfig entityConfig) {
		val PropertyOrder[] orders = entityConfig.propertyOrder
		var List<FieldDeclaration> fields = newArrayList
		fields.addAll(clazz.jsonFields)
		var extendedClazz = clazz.extendedClass?.type as ClassDeclaration
		while (extendedClazz !== null && extendedClazz != object) {
			fields.addAll(extendedClazz.jsonFields)
			extendedClazz = extendedClazz?.extendedClass?.type as ClassDeclaration
		}
		val sortedFields = sortFields(fields, clazz, orders)
		clazz.addAnnotation(JsonPropertyOrder.newAnnotationReference [
			setStringValue("value", sortedFields.map[simpleName])
		])
	}
}
