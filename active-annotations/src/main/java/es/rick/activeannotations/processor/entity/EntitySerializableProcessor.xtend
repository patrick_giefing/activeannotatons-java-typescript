package es.rick.activeannotations.processor.entity

import org.eclipse.xtend.lib.macro.declaration.MutableClassDeclaration
import org.eclipse.xtend.lib.macro.TransformationContext
import java.io.Serializable

class EntitySerializableProcessor extends EntityBaseProcessor {
	override transformEntity(extension MutableClassDeclaration clazz, extension TransformationContext context,
		extension EntityProcessorConfig entityConfig) {
		makeSerializable(clazz, context)
	}

	def static void makeSerializable(@Extension MutableClassDeclaration clazz,
		@Extension TransformationContext context) {
		// implements Serializable if not already implemented
		if (!clazz.implementedInterfaces.exists[qualifiedName == Serializable.name]) {
			clazz.implementedInterfaces = clazz.implementedInterfaces + #[Serializable.newTypeReference]
		}
		addSerialVersionUID(clazz, context)
	}

	def static void addSerialVersionUID(MutableClassDeclaration clazz, @Extension TransformationContext context) {
		clazz.addField("serialVersionUID", [
			static = true
			final = true
			type = long.newTypeReference
			initializer = '''�clazz.generateSerialVersionUID(context)�'''
		])
	}

	/**
	 * Berechnet die serialVersionUID anhand der Summe des hashCode der Namen
	 * der Felder.
	 */
	def static long generateSerialVersionUID(MutableClassDeclaration clazz, @Extension TransformationContext context) {
		val fields = clazz.allFields
		val v = fields.map[type.simpleName.hashCode + simpleName.hashCode].reduce[p1, p2|p1 + p2]
		(v ?: 0) as long
	}
}
