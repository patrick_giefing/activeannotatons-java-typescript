package es.rick.activeannotations.processor.entity

import es.rick.activeannotations.IgnoreToString
import java.util.List
import org.eclipse.xtend.lib.macro.TransformationContext
import org.eclipse.xtend.lib.macro.declaration.ClassDeclaration
import org.eclipse.xtend.lib.macro.declaration.FieldDeclaration
import org.eclipse.xtend.lib.macro.declaration.MutableClassDeclaration
import org.eclipse.xtext.xbase.lib.util.ToStringBuilder

/**
 * based on org.eclipse.xtend.lib.annotations.ToStringProcessor
 */
class EntityToStringProcessor extends EntityBaseProcessor {
	val static TO_STRING = "toString"

	override void transformEntity( //
	extension MutableClassDeclaration clazz, //
	extension TransformationContext context, //
	extension EntityProcessorConfig entityConfig) {
		val toStringMethodAlreadyExists = clazz.declaredMethods.exists[simpleName == TO_STRING]
		if (toStringMethodAlreadyExists) {
			return
		}
		clazz.addToStringMethod(context, entityConfig)
	}

	def void addToStringMethod( //
		extension MutableClassDeclaration clazz, //
		extension TransformationContext context, //
		extension EntityProcessorConfig entityConfig
	) {
		val toStringConfig = entityConfig.toStringConfig
		clazz.addMethod(TO_STRING) [
			primarySourceElement = clazz.primarySourceElement
			returnType = string
			addAnnotation(newAnnotationReference(Override))
			addAnnotation(newAnnotationReference(Pure))
			val sb = new StringBuffer
			sb.append('''�ToStringBuilder.name� b = new �ToStringBuilder.name�(this);''').append("\r\n")
			if (toStringConfig.skipNulls) {
				sb.append("b.skipNulls();\r\n")
			}
			if (toStringConfig.singleLine) {
				sb.append("b.singleLine();\r\n")
			}
			if (toStringConfig.hideFieldNames) {
				sb.append("b.hideFieldNames();\r\n")
			}
			if (toStringConfig.verbatimValues) {
				sb.append("b.verbatimValues();\r\n")
			}
			var List<FieldDeclaration> fields = newArrayList
			fields.addAll(clazz.toStringFields)
			var extendedClazz = clazz.extendedClass?.type as ClassDeclaration
			while (extendedClazz !== null && extendedClazz != object) {
				fields.addAll(extendedClazz.toStringFields)
				extendedClazz = extendedClazz?.extendedClass?.type as ClassDeclaration
			}
			fields = fields.sortFields(clazz, entityConfig.propertyOrder)
			fields.forEach [ field |
				var accessor = '''this.�field.simpleName�'''
				if (field.declaringType != clazz) {
					accessor = '''this.get�field.simpleName.toFirstUpper�()'''
					if (field.type.primitive && field.type.isAssignableFrom(boolean.newTypeReference)) {
						accessor = '''this.is�field.simpleName.toFirstUpper�()'''
					}
					if (field.simpleName.startsWith("is")) {
						accessor = '''this.�field.simpleName�()'''
					}
				}
				val isByteArray = !field.type.inferred && field.type.isArray &&
					field.type.arrayComponentType.isAssignableFrom(byte.newTypeReference)
				if (isByteArray) {
					sb.
						append('''b.add("�field.simpleName�", �accessor� != null ? ("byte[] - length: " + �accessor�.length + ", hashCode: " + �accessor�.hashCode()) : null);''').
						append("\r\n")
				} else {
					sb.append('''b.add("�field.simpleName�", �accessor�);''').append("\r\n")
				}
			]
			sb.append("return b.toString();")
			body = '''�sb�'''
		]
	}

	def Iterable<? extends FieldDeclaration> getToStringFields(ClassDeclaration clazz) {
		clazz.declaredFields.filter [
			!static && !transient && !annotations.exists[annotationTypeDeclaration.qualifiedName == IgnoreToString.name]
		]
	}
}
