package es.rick.activeannotations.processor.entity

import es.rick.activeannotations.CompositeIdType
import javax.persistence.EmbeddedId
import javax.persistence.Id
import javax.persistence.IdClass
import org.eclipse.xtend.lib.macro.RegisterGlobalsContext
import org.eclipse.xtend.lib.macro.TransformationContext
import org.eclipse.xtend.lib.macro.declaration.AnnotationReference
import org.eclipse.xtend.lib.macro.declaration.FieldDeclaration
import org.eclipse.xtend.lib.macro.declaration.MutableClassDeclaration
import org.eclipse.xtend.lib.macro.declaration.MutableFieldDeclaration
import org.eclipse.xtend.lib.macro.declaration.TypeDeclaration
import org.eclipse.xtext.xbase.lib.Functions.Function1

class EntityCompositeIdProcessor extends EntityBaseProcessor {
	val static String idFieldName = "id"
	val Function1<AnnotationReference, Boolean> funcIsIdAnnotation = [Id.name == annotationTypeDeclaration.qualifiedName]

	override doRegisterGlobals(TypeDeclaration decl, extension RegisterGlobalsContext context,
		extension EntityProcessorConfig entityConfig) {
		if (entityConfig.jpaConfig.compositeIdType == CompositeIdType.NoCompositeId) {
			return
		}
		if (decl.idFields.size < 2) {
			return // at least 2 fields needed for composite id
		}
		decl.compositeIdClassName.registerClass
	}

	override transformEntity(MutableClassDeclaration clazz, extension TransformationContext context,
		extension EntityProcessorConfig entityConfig) {
		if (entityConfig.jpaConfig.compositeIdType == CompositeIdType.NoCompositeId) {
			return
		}
		val idFields = clazz.idFields
		if (idFields.size < 2) {
			return // at least 2 fields needed for composite id
		}
		val idClazz = clazz.compositeIdClassName.findClass.addErrorIfNull(context, clazz, "idClass not found")
		idClazz.primarySourceElement = clazz.primarySourceElement
		idClazz.setXmlRootElementAndAccessorType(context)
		idClazz.static = true
		idClazz.addConstructor [ constructor |
			idFields.forEach [ idField |
				constructor.addParameter(idField.simpleName, idField.type)
			]
			constructor.body = '''�idFields.map['''this.�it.simpleName� = �it.simpleName�;'''].join("\r\n")�'''
		]
		idClazz.addConstructor[]
		idFields.forEach [ idField |
			idField.removeAnnotation(idField.annotations.findFirst(funcIsIdAnnotation))
			val idClazzField = idClazz.addField(idField.simpleName, [ field |
				field.primarySourceElement = idField.primarySourceElement
				field.type = idField.type
				idField.annotations.forEach [ ann |
					field.addAnnotation(ann.newAnnotationReference)
				]
			])
			idClazzField.markAsRead
		]
		if (entityConfig.jpaConfig.compositeIdType == CompositeIdType.IdClass) {
			clazz.addAnnotation(IdClass.newAnnotationReference [
				setClassValue("value", idClazz.newTypeReference)
			])
		} else {
			val compositeIdField = clazz.addField(idFieldName, [
				type = idClazz.newTypeReference
				addAnnotation(EmbeddedId.newAnnotationReference)
			])
			idFields.forEach[remove]
			idFields.forEach [ idField |
				clazz.addDelegateGetterMethod(idField, compositeIdField, context)
				clazz.addDelegateSetterMethod(idField, compositeIdField, context)
			]
		}
		/*idFields.forEach [ idField |
		 * 	idClazz.addGetterMethod(idField, context)
		 * 	idClazz.addSetterMethod(idField, context)
		 ]*/
		EntityProcessor.transformGeneratedEntity(idClazz, context, entityConfig)
	}

	def static String getCompositeIdClassName(TypeDeclaration decl) {
		'''�decl.qualifiedName�.�decl.simpleName�Id'''
	}

	def Iterable<? extends MutableFieldDeclaration> getIdFields(MutableClassDeclaration clazz) {
		clazz.declaredFields.filter[annotations.exists(funcIsIdAnnotation)].toList
	}

	def Iterable<? extends FieldDeclaration> getIdFields(TypeDeclaration decl) {
		decl.declaredFields.filter[annotations.exists(funcIsIdAnnotation)].toList
	}
}
