package es.rick.activeannotations.processor.rest

import com.google.common.net.UrlEscapers
import es.rick.activeannotations.GenerateRestClient
import es.rick.activeannotations.GenerateRestClients
import es.rick.activeannotations.GenerationTarget
import es.rick.activeannotations.RestClient
import es.rick.activeannotations.processor.delegate.DelegateProcessor.DelegateConfig
import es.rick.activeannotations.processor.delegate.NamePatternProcessorConfig
import es.rick.activeannotations.processor.delegate.ParameterizedDelegateConfig
import es.rick.activeannotations.processor.delegate.ParameterizedDelegateProcessor
import es.rick.activeannotations.processor.delegate.ParameterizedDelegateProcessorConfig
import java.net.URI
import java.net.URISyntaxException
import java.nio.charset.Charset
import java.util.ArrayList
import java.util.Collection
import java.util.Date
import java.util.List
import java.util.regex.Pattern
import javax.ws.rs.Consumes
import javax.ws.rs.PathParam
import javax.ws.rs.QueryParam
import javax.ws.rs.core.Context
import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtend.lib.annotations.ToString
import org.eclipse.xtend.lib.macro.RegisterGlobalsContext
import org.eclipse.xtend.lib.macro.RegisterGlobalsParticipant
import org.eclipse.xtend.lib.macro.TransformationContext
import org.eclipse.xtend.lib.macro.TransformationParticipant
import org.eclipse.xtend.lib.macro.declaration.AnnotationReference
import org.eclipse.xtend.lib.macro.declaration.InterfaceDeclaration
import org.eclipse.xtend.lib.macro.declaration.MethodDeclaration
import org.eclipse.xtend.lib.macro.declaration.MutableTypeDeclaration
import org.eclipse.xtend.lib.macro.declaration.TypeDeclaration
import org.eclipse.xtend.lib.macro.declaration.TypeReference
import org.eclipse.xtend.lib.macro.declaration.Visibility
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1
import org.eclipse.xtext.xbase.lib.Procedures.Procedure2
import org.eclipse.xtext.xbase.lib.Procedures.Procedure4
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import org.springframework.util.Base64Utils
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.web.client.RestTemplate

class RestClientsProcessor extends RestProcessor implements RegisterGlobalsParticipant<TypeDeclaration>, TransformationParticipant<MutableTypeDeclaration> {
	override doRegisterGlobals(List<? extends TypeDeclaration> decls, extension RegisterGlobalsContext context) {
		decls.forEach[doRegisterGlobals(context)]
	}

	def void doRegisterGlobals(TypeDeclaration decl, extension RegisterGlobalsContext context) {
		val configs = decl.readConfig
		doRegisterRestClients(configs, context)
	}

	def void doRegisterRestClients(List<RestClientConfig> configs, extension RegisterGlobalsContext context) {
		val parameterizedDelegateProcessor = new ParameterizedDelegateProcessor
		configs.forEach[
			qualifiedNameTarget.registerClass
			parameterizedDelegateProcessor.doRegisterParameterizedDelegates(parameterizedDelegates, context)
		]
	}

	override doTransform(List<? extends MutableTypeDeclaration> decls, extension TransformationContext context) {
		decls.forEach[doTransform(context)]
	}

	def void doTransform(MutableTypeDeclaration decl, extension TransformationContext context) {
		val configs = decl.readConfig
		configs.forEach[config|doTransform(decl, context, config)]
	}

	def void doTransform(MutableTypeDeclaration decl, extension TransformationContext context,
		RestClientConfig config) {
		val service = context.findTypeGlobally(config.qualifiedNameSource) as TypeDeclaration
		val isInterface = service instanceof InterfaceDeclaration
		val client = context.findClass(config.qualifiedNameTarget)
		if (service === null) {
			decl.addError('''Source type couldn't be found: �config.qualifiedNameSource�''')
			return
		}
		if (client === null) {
			decl.addError('''Target type couldn't be found: �config.qualifiedNameTarget�''')
			return
		}
		client.docComment = service.docComment
		val servicePath = service.rsPath
		if (servicePath.nullOrEmpty) {
			// TODO proceed with empty path?
			decl.addError('''Annotation @Path for �service.qualifiedName� not available or empty path: �service.qualifiedName� - �config.qualifiedNameSource�''')
			return
		}
		client.addField("REGEX_REPLACE_MULTIPLE_SLASHES", [
			visibility = Visibility.PRIVATE
			type = Pattern.newTypeReference
			static = true
			final = true
			initializer = '''�Pattern.name�.compile("/{2,}")'''
		])
		client.addField("servicePath", [
			visibility = Visibility.PUBLIC
			type = String.newTypeReference
			initializer = '''"�IF !servicePath.startsWith("/")�/�ENDIF��servicePath�"'''
		])
		client.addField("endpoint", [
			type = String.newTypeReference
		])
		client.addField("authUsername", [
			type = String.newTypeReference
		])
		client.addField("authPassword", [
			type = String.newTypeReference
		])
		client.addField("restTemplateInitializer", [
			visibility = Visibility.PROTECTED
			type = Procedure1.newTypeReference(RestTemplate.newTypeReference)
			addAnnotation(Autowired.newAnnotationReference [
				setBooleanValue("required", false)
			])
			addAnnotation(Qualifier.newAnnotationReference [
				setStringValue("value", "restTemplateInitializer")
			])
		])
		client.addField("restTemplateHeadersInitializer", [
			visibility = Visibility.PROTECTED
			type = Procedure2.newTypeReference(HttpHeaders.newTypeReference, String.newTypeReference)
			addAnnotation(Autowired.newAnnotationReference [
				setBooleanValue("required", false)
			])
			addAnnotation(Qualifier.newAnnotationReference [
				setStringValue("value", "restTemplateHeadersInitializer")
			])
		])
		client.addMethod("getEndpoint", [
			returnType = String.newTypeReference
			body = '''return endpoint;'''
		])
		client.addMethod("setEndpoint", [
			addParameter("endpoint", String.newTypeReference)
			body = '''this.endpoint = endpoint;'''
		])
		client.addMethod("getServicePath", [
			returnType = String.newTypeReference
			body = '''return servicePath;'''
		])
		client.addMethod("setServicePath", [
			addParameter("servicePath", String.newTypeReference)
			body = '''this.servicePath = servicePath;'''
		])
		client.addMethod("getAuthUsername", [
			returnType = String.newTypeReference
			body = '''return authUsername;'''
		])
		client.addMethod("setAuthUsername", [
			addParameter("authUsername", String.newTypeReference)
			body = '''this.authUsername = authUsername;'''
		])
		client.addMethod("getAuthPassword", [
			returnType = String.newTypeReference
			body = '''return authPassword;'''
		])
		client.addMethod("setAuthPassword", [
			addParameter("authPassword", String.newTypeReference)
			body = '''this.authPassword = authPassword;'''
		])
		client.addConstructor [
			addParameter("endpoint", String.newTypeReference)
			body = '''this.endpoint = endpoint;'''
		]
		client.addConstructor [
			addParameter("endpoint", String.newTypeReference)
			addParameter("authUsername", String.newTypeReference)
			addParameter("authPassword", String.newTypeReference)
			body = '''
			this.endpoint = endpoint;
			this.authUsername = authUsername;
			this.authPassword = authPassword;'''
		]
		client.addConstructor[]
		if (isInterface) {
			client.implementedInterfaces = client.implementedInterfaces + #[service.newTypeReference]
		} else {
			client.implementedInterfaces = client.implementedInterfaces
		}
		client.addMethod("setRestTemplateInitializer", [
			addParameter("restTemplateInitializer", Procedure1.newTypeReference(RestTemplate.newTypeReference))
			body = '''this.restTemplateInitializer = restTemplateInitializer;'''
		])
		client.addMethod("setRestTemplateHeadersInitializer", [
			addParameter("restTemplateHeadersInitializer",
				Procedure2.newTypeReference(HttpHeaders.newTypeReference, String.newTypeReference))
			body = '''this.restTemplateHeadersInitializer = restTemplateHeadersInitializer;'''
		])
		client.addMethod("buildRestTemplate", [
			returnType = RestTemplate.newTypeReference
			body = '''
			�RestTemplate.name� restTemplate = new �RestTemplate.name�();
			if(restTemplateInitializer != null) {
				restTemplateInitializer.apply(restTemplate);
			}
			return restTemplate;'''
		])
		val serviceMethods = service.allDeclaredMethods
		val restMethods = serviceMethods.filter[isGet || isPost || isPut || isDelete]
		val nonRestMethods = new ArrayList<MethodDeclaration> => [
			addAll(serviceMethods)
			removeAll(restMethods)
		]
		if (isInterface) {
			// if the source type is an interface, we have to declare the method
			nonRestMethods.forEach [ method |
				client.addMethod(method.simpleName, [ clientMethod |
					clientMethod.docComment = method.docComment
					clientMethod.returnType = method.returnType
					method.parameters.forEach [ param |
						// If it is not an interface, we don't need the context parameters in the generated client
						// It it is an interface we have to transfer it
						if (!isInterface && param.annotations.exists [
							annotationTypeDeclaration.qualifiedName == Context.name
						]) {
							return
						}
						val clientParam = clientMethod.addParameter(param.simpleName, param.type)
						param.annotations.forEach [ ann |
							clientParam.addAnnotation(ann)
						]
					]
					method.annotations.forEach [ ann |
						clientMethod.addAnnotation(ann)
					]
					clientMethod.body = '''throw new RuntimeException("Not a rest method");'''
				])
			]
		}
		restMethods.forEach [ method |
			val isMultiPart = method.isMultiPart
			client.addMethod(method.simpleName, [ clientMethod |
				clientMethod.docComment = method.docComment
				clientMethod.returnType = method.returnType
				method.parameters.forEach [ param |
					val clientParam = clientMethod.addParameter(param.simpleName, param.type)
					param.annotations.forEach [ ann |
						clientParam.addAnnotation(ann)
					]
				]
				method.annotations.forEach [ ann |
					clientMethod.addAnnotation(ann)
				]
				val paramsWithoutJaxRsAnnotations = method.parameters.filter(filterParamsWithoutJaxRsAnnotations)
				val isPost = method.isPost
				val isPut = method.isPut
				val isDelete = method.isDelete
				if (isPost || isPut) {
					if (paramsWithoutJaxRsAnnotations.size > 1) {
						decl.
							addError('''�service.qualifiedName�.�method.simpleName� contains more than 1 parameter without javax.ws.rs.*-annotation. Use @�Context.name� to ignore parameters for rest client generation.''')
						return
					} else if (paramsWithoutJaxRsAnnotations.size === 1 && isMultiPart) {
						decl.
							addError('''�service.qualifiedName�.�method.simpleName� contains 1 parameter without javax.ws.rs.*-annotation and is marked as MultiPart at the same time. Use @�Context.name� to ignore parameters for rest client generation.''')
						return
					}
				} else {
					if (!paramsWithoutJaxRsAnnotations.empty) {
						decl.
							addError('''�service.qualifiedName�.�method.simpleName� contains parameter without javax.ws.rs.*-annotation, but is not marked with @POST or @PUT. Use @�Context.name� to ignore parameters for rest client generation.''')
						return
					}
					if (isMultiPart) {
						decl.
							addError('''�service.qualifiedName�.�method.simpleName� is marked with @�Consumes.name�(MULTIPART) but is neither @POST nor @PUT''')
						return
					}
				}
				val sb = new StringBuilder
				sb.
					append('''	String sUrl = this.endpoint + this.servicePath + "/�method.rsPath�/?callTimestamp=" + (new �Date.name�()).getTime();''').
					nl
				sb.append('''	sUrl = REGEX_REPLACE_MULTIPLE_SLASHES.matcher(sUrl).replaceAll("/");''').nl

				val pathParamParams = clientMethod.parameters.filter [
					annotations.exists[annotationTypeDeclaration.qualifiedName == PathParam.name]
				]
				pathParamParams.forEach [ pathParam |
					val pathAnnotation = pathParam.annotations.findFirst [ ann |
						ann.annotationTypeDeclaration.qualifiedName == PathParam.name
					]
					val pathName = pathAnnotation.getStringValue("value")
					sb.
						append('''		sUrl = sUrl.replace("{�pathName�}", �IF !pathParam.type.primitive��pathParam.simpleName� == null ? "" : (�ENDIF��pathParam.simpleName.encodeUrlPartIfString(pathParam.type)��IF !pathParam.type.primitive�)�ENDIF�);''').
						append("\r\n")
				]
				val queryParamParams = method.parameters.filter [
					annotations.exists[annotationTypeDeclaration.qualifiedName == QueryParam.name]
				]
				if (isMultiPart) {
					sb.
						append('''	�MultiValueMap.name�<String, Object> values = new �LinkedMultiValueMap.name�<String, Object>();''').
						append("\r\n")
				}
				queryParamParams.forEach [ queryParam |
					val queryAnnotation = queryParam.annotations.findFirst [ ann |
						ann.annotationTypeDeclaration.qualifiedName == QueryParam.name
					]
					val queryName = queryAnnotation.getStringValue("value")
					val primitive = queryParam.type.primitive
					if (!primitive) {
						sb.append('''if(�queryParam.simpleName� != null) {''').append("\r\n")
					}
					if (Collection.newTypeReference.isAssignableFrom(queryParam.type)) {
						sb.
							append('''	for(�queryParam.type.actualTypeArguments?.head?.name ?: queryParam.type.arrayComponentType ?: "Object"� �queryParam.simpleName�Entry : �queryParam.simpleName�) {''').
							append("\r\n")
						sb.append('''		if(�queryParam.simpleName�Entry != null) {''').append("\r\n")
						if (isMultiPart) {
							sb.append('''			values.add("�queryName�", �queryParam.simpleName�Entry);''').
								append("\r\n")
						} else {
							sb.
								append('''			sUrl += "&�queryName�=" + �(queryParam.simpleName + "Entry").encodeUrlPartIfString(queryParam.type)�;''').
								append("\r\n")
						}
						sb.append('''		}''').append("\r\n")
						sb.append('''	}''').append("\r\n")
					} else {
						if (isMultiPart) {
							sb.append('''	values.add("�queryName�", �queryParam.simpleName�);''').append("\r\n")
						} else {
							sb.
								append('''	sUrl += "&�queryName�=" + �queryParam.simpleName.encodeUrlPartIfString(queryParam.type)�;''').
								append("\r\n")
						}
					}
					if (!primitive) {
						sb.append("}\r\n")
					}
				]
				sb.append('''	�RestTemplate.name� template = buildRestTemplate();''').append("\r\n")
				sb.append('''	�HttpHeaders.name� headers = new �HttpHeaders.name�();''').append("\r\n")
				sb.
					append('''	if(this.authUsername != null && !this.authUsername.isEmpty() && this.authPassword != null && !this.authPassword.isEmpty()) {''').
					append("\r\n")
				sb.
					append('''		headers.set("Authorization", "Basic " + new String(�Base64Utils.name�.encode((this.authUsername + ":" + this.authPassword).getBytes(�Charset.name�.forName("US-ASCII")))));''').
					append("\r\n")
				sb.append('''	}''').append("\r\n")
				sb.append('''	sUrl = �UrlEscapers.name�.urlFragmentEscaper().escape(sUrl);''').append("\r\n")
				sb.append('''	if(this.restTemplateHeadersInitializer != null) {''').append("\r\n")
				sb.append('''		this.restTemplateHeadersInitializer.apply(headers, sUrl);''').append("\r\n")
				sb.append('''	}''').append("\r\n")
				if (isMultiPart) {
					sb.
						append('''	�HttpEntity.name�<Object> httpEntity = new �HttpEntity.name�<Object>(values, headers);''')
				} else if (paramsWithoutJaxRsAnnotations.size === 1) {
					sb.
						append('''	�HttpEntity.name�<Object> httpEntity = new �HttpEntity.name�<Object>(�paramsWithoutJaxRsAnnotations.head.simpleName�, headers);''')
				} else {
					sb.append('''	�HttpEntity.name�<Object> httpEntity = new �HttpEntity.name�<Object>(headers);''')
				}
				sb.append("\r\n")
				val httpEntity = switch 0 {
					case isPost: HttpMethod.POST
					case isPut: HttpMethod.PUT
					case isDelete: HttpMethod.DELETE
					default: HttpMethod.GET
				}
				var responseEntityTypeName = method.returnType.wrapperIfPrimitive.name
				if (method.returnType.void) {
					responseEntityTypeName = Void.newTypeReference.name
				}
				sb.append('''	�URI.name� uri;''').append("\r\n")
				sb.append('''	try {''').append("\r\n")
				sb.append('''		uri = new �URI.name�(sUrl);''').append("\r\n")
				sb.append('''	} catch(�URISyntaxException.name� ex) {''').append("\r\n")
				sb.append('''		throw new RuntimeException(ex);''').append("\r\n")
				sb.append('''	}''').append("\r\n")
				sb.
					append('''	�ResponseEntity.name�<�responseEntityTypeName�> responseEntity = template.<�responseEntityTypeName�>exchange(uri, �HttpMethod.name�.�httpEntity.name�, httpEntity, new �ParameterizedTypeReference.name�<�responseEntityTypeName�>(){});''').
					append("\r\n")
				sb.append('''	Object response = responseEntity.getBody();''').append("\r\n")
				// Bei void kann null zur�ckkommen
				sb.
					append('''	if(�IF method.returnType.void�response == null || �ENDIF�response instanceof �method.returnType.wrapperIfPrimitive.type.qualifiedName�) {''').
					append("\r\n")
				if (method.returnType.void) {
					sb.append('''		return;''').append("\r\n")
				} else {
					sb.append('''		return (�responseEntityTypeName�) response;''').append("\r\n")
				}
				sb.append('''	}''').append("\r\n")
				// TODO responseObject irgendwie mitgeben
				sb.
					append('''	throw new RuntimeException("Unexpected result class: " + (response != null ? response.getClass().getName() : "null"));''').
					append("\r\n")
				clientMethod.body = '''�sb�'''
			])
		]
	}

	def static List<RestClientConfig> readConfig(TypeDeclaration decl) {
		val List<RestClientConfig> configs = new ArrayList
		val annGenerateRestClients = decl.annotations.findFirst [
			annotationTypeDeclaration.qualifiedName == GenerateRestClients.name
		]
		val annGenerateRestClient = decl.annotations.findFirst [
			annotationTypeDeclaration.qualifiedName == GenerateRestClient.name
		]
		val annRestClient = decl.annotations.findFirst [
			annotationTypeDeclaration.qualifiedName == RestClient.name
		]
		val Procedure4<RestClientProcessorConfig, String, String, GenerationTarget> procAddConfig = [ config, sourceName, targetName, generationTargetParent |
			configs.add(new RestClientConfig => [
				qualifiedNameSource = sourceName
				qualifiedNameTarget = targetName
				parameterizedDelegates = config.delegatesConfig.map [
					ParameterizedDelegateProcessor.create(it, sourceName, generationTargetParent)
				]
			])
		]
		if (annGenerateRestClients !== null) {
			val config = new GenerateRestClientsProcessorConfig(annGenerateRestClients)
			config.restClientsConfig.forEach [ clientsConfig |
				val targetName = decl.qualifiedName.generateTargetName(config.generationTarget, clientsConfig.restClientConfig.namePattern)
				procAddConfig.apply(clientsConfig.restClientConfig, clientsConfig.sourceName, targetName, config.generationTarget)
			]
		}
		if (annGenerateRestClient !== null) {
			val config = new GenerateRestClientProcessorConfig(annGenerateRestClient)
			val targetName = decl.qualifiedName.generateTargetName(config.restClientConfig.generationTarget, config.restClientConfig.namePattern)
			procAddConfig.apply(config.restClientConfig, config.sourceName, targetName, config.restClientConfig.generationTarget)
		}
		if (annRestClient !== null) {
			val config = new RestClientProcessorConfig(annRestClient)
			val targetName = decl.qualifiedName.generateTargetName(config.generationTarget.inherit(config.generationTarget), config.namePattern)
			procAddConfig.apply(config, decl.qualifiedName, targetName, config.generationTarget)
		}
		return configs
	}
}

@ToString @Accessors
class RestClientConfig extends DelegateConfig {
	List<ParameterizedDelegateConfig> parameterizedDelegates
}

@ToString @Accessors
class GenerateRestClientsProcessorConfig {
	val GenerateRestClientProcessorConfig[] restClientsConfig
	val GenerationTarget generationTarget

	new(AnnotationReference ann) {
		restClientsConfig = ann.getAnnotationArrayValue("value").map[new GenerateRestClientProcessorConfig(it)]
		generationTarget = GenerationTarget.valueOf(ann.getEnumValue("generationTarget").simpleName)
	}
}

@ToString @Accessors
class GenerateRestClientProcessorConfig {
	val TypeReference interfaceClass
	val String interfaceName
	val RestClientProcessorConfig restClientConfig

	new(AnnotationReference ann) {
		interfaceClass = ann.getClassValue("interfaceClass")
		interfaceName = ann.getStringValue("interfaceName")
		restClientConfig = new RestClientProcessorConfig(ann.getAnnotationValue("restClient"))
	}

	def String getSourceName() {
		if (interfaceClass !== null && interfaceClass.type.qualifiedName != Object.name) {
			return interfaceClass.name
		}
		return interfaceName
	}
}

@ToString @Accessors
class RestClientProcessorConfig {
	val NamePatternProcessorConfig namePattern
	val ParameterizedDelegateProcessorConfig[] delegatesConfig
	val GenerationTarget generationTarget

	new(AnnotationReference ann) {
		namePattern = new NamePatternProcessorConfig(ann.getAnnotationValue("namePattern"))
		delegatesConfig = ann.getAnnotationArrayValue("delegates").map[new ParameterizedDelegateProcessorConfig(it)]
		generationTarget = GenerationTarget.valueOf(ann.getEnumValue("generationTarget").simpleName)
	}
}
