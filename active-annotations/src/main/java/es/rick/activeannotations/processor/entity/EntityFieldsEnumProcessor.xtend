package es.rick.activeannotations.processor.entity

import org.eclipse.xtend.lib.macro.RegisterGlobalsContext
import org.eclipse.xtend.lib.macro.TransformationContext
import org.eclipse.xtend.lib.macro.declaration.ClassDeclaration
import org.eclipse.xtend.lib.macro.declaration.MutableClassDeclaration
import org.eclipse.xtend.lib.macro.declaration.FieldDeclaration
import org.eclipse.xtend.lib.macro.declaration.TypeDeclaration

class EntityFieldsEnumProcessor extends EntityBaseProcessor {
	val static String FIELDS = "Fields"

	override void doRegisterGlobals(TypeDeclaration decl, extension RegisterGlobalsContext context,
		extension EntityProcessorConfig entityConfig) {
		context.registerEnumerationType('''�decl.qualifiedName�.�FIELDS�''')
	}

	override transformEntity(extension MutableClassDeclaration clazz, extension TransformationContext context,
		extension EntityProcessorConfig entityConfig) {
		val columnFields = clazz.columnFields
		val columnsEnum = context.findEnumerationType('''�clazz.qualifiedName�.�FIELDS�''')
		if (columnsEnum !== null && clazz.primarySourceElement !== null) {
			columnsEnum.primarySourceElement = clazz.primarySourceElement
		}
		columnFields?.forEach [
			columnsEnum?.addValue(simpleName, [])
		]
	}

	def static Iterable<? extends FieldDeclaration> getColumnFields(ClassDeclaration clazz) {
		clazz.declaredFields.filter[!final && !static]
	}
}
