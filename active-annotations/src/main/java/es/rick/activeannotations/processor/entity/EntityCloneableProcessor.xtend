package es.rick.activeannotations.processor.entity

import com.google.common.annotations.GwtIncompatible
import java.util.List
import java.util.Set
import org.eclipse.xtend.lib.macro.TransformationContext
import org.eclipse.xtend.lib.macro.declaration.MethodDeclaration
import org.eclipse.xtend.lib.macro.declaration.MutableClassDeclaration
import org.eclipse.xtend.lib.macro.declaration.TypeReference

class EntityCloneableProcessor extends EntityBaseProcessor {
	val static String CLONE = "clone"

	override transformEntity(extension MutableClassDeclaration clazz, extension TransformationContext context,
		extension EntityProcessorConfig entityConfig) {
		val cloneMethodExists = clazz.declaredMethods.exists[simpleName == CLONE]
		if (cloneMethodExists) {
			return // clone method already exists - nothing to do here
		}
		clazz.createCloneMethod(context)
	}

	def void createCloneMethod(@Extension MutableClassDeclaration annotatedClass,
		@Extension TransformationContext context) {
		if (!annotatedClass.implementedInterfaces.exists[it.isAssignableFrom(Cloneable.newTypeReference)]) {
			annotatedClass.implementedInterfaces = annotatedClass.implementedInterfaces + #[Cloneable.newTypeReference]
		}
		val definedCloneMethod = findCloneMethod(annotatedClass.newTypeReference)
		val boolean catchException = definedCloneMethod === null || !definedCloneMethod.exceptions.nullOrEmpty
		addMethod(CLONE, [
			returnType = declaringType.newTypeReference
			addAnnotation(Override.newAnnotationReference)
			if (GwtIncompatible.newAnnotationReference !== null) {
				val annGwtIncompatible = GwtIncompatible.newAnnotationReference [
					setStringValue("value", "clone() not defined in GWT's java.lang.Object")
				]
				addAnnotation(annGwtIncompatible)
			}
			val entityType = declaringType.simpleName
			val collFields = declaringType.declaredFields.filter[!type.array].filter [
				List.newTypeReference.isAssignableFrom(type) || Set.newTypeReference.isAssignableFrom(type)
			]
			val cloneableFields = declaringType.declaredFields.filter[!type.array].filter [
				!Iterable.newTypeReference.isAssignableFrom(type) && Cloneable.newTypeReference.isAssignableFrom(type)
			]
			body = '''
			�IF catchException�try {
				�ENDIF�
				�entityType� entity = (�entityType�)super.clone();
				�FOR collField : collFields�
					if(entity.�collField.simpleName� != null) {
						�IF Cloneable.newTypeReference.isAssignableFrom(collField.type.actualTypeArguments.head)�
							�collField.type� oldValues = entity.�collField.simpleName�;
							�getCollectionTypeNew(collField, context)� newValues = new �getCollectionTypeNew(collField, context)�();
							for(�collField.type.actualTypeArguments.head.upperBound� oldValue : oldValues) {
								if(oldValue != null) {
									newValues.add(oldValue.clone());
								}
							}
							entity.�collField.simpleName� = newValues;
						�ELSE�
							entity.�collField.simpleName� = new �getCollectionTypeNew(collField, context)�(entity.�collField.simpleName�);
						�ENDIF�
					}
				�ENDFOR�
				�FOR cloneableField : cloneableFields�
					entity.�cloneableField.simpleName� = entity.�cloneableField.simpleName� != null ? (�cloneableField.type.simpleName�)entity.�cloneableField.simpleName�.clone() : null;
				�ENDFOR�
				return entity;
				�IF catchException�} catch (CloneNotSupportedException th) {
								throw new �RuntimeException.name�(th);
				}�ENDIF�'''
		])
	}

	def MethodDeclaration findCloneMethod(TypeReference typeRef) {
		val MethodDeclaration method = typeRef.declaredResolvedMethods.map[declaration].findFirst [
			CLONE.equals(simpleName) && parameters.nullOrEmpty
		]
		if (method !== null) {
			return method
		}
		val cloneMethods = typeRef.declaredSuperTypes.map[findCloneMethod].filterNull
		cloneMethods?.head
	}
}
