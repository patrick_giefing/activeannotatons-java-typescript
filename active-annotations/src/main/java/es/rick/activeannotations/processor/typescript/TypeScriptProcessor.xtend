package es.rick.activeannotations.processor.typescript

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import es.rick.activeannotations.processor.ProcessorBase
import java.util.List
import java.util.regex.Pattern
import org.eclipse.xtend.lib.macro.CodeGenerationContext
import org.eclipse.xtend.lib.macro.CodeGenerationParticipant
import org.eclipse.xtend.lib.macro.TransformationContext
import org.eclipse.xtend.lib.macro.TransformationParticipant
import org.eclipse.xtend.lib.macro.ValidationContext
import org.eclipse.xtend.lib.macro.ValidationParticipant
import org.eclipse.xtend.lib.macro.declaration.AnnotationReference
import org.eclipse.xtend.lib.macro.declaration.AnnotationTarget
import org.eclipse.xtend.lib.macro.declaration.ClassDeclaration
import org.eclipse.xtend.lib.macro.declaration.FieldDeclaration
import org.eclipse.xtend.lib.macro.declaration.MutableTypeDeclaration
import org.eclipse.xtend.lib.macro.declaration.TypeDeclaration
import org.eclipse.xtend.lib.macro.declaration.TypeParameterDeclaration
import org.eclipse.xtend.lib.macro.declaration.TypeParameterDeclarator
import org.eclipse.xtend.lib.macro.declaration.TypeReference
import org.eclipse.xtext.xbase.lib.Functions.Function1
import org.eclipse.xtend.lib.macro.declaration.MethodDeclaration
import org.eclipse.xtend.lib.macro.declaration.ParameterDeclaration
import org.eclipse.xtend.lib.macro.file.FileLocations
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1
import com.google.common.io.ByteStreams
import org.eclipse.xtend.lib.macro.file.MutableFileSystemSupport

/**
 * TODO JsonIgnoreProperties
 * TODO @JsonIgnoreType
 */
abstract class TypeScriptProcessor extends ProcessorBase implements TransformationParticipant<MutableTypeDeclaration>, ValidationParticipant<MutableTypeDeclaration>, CodeGenerationParticipant<TypeDeclaration> {
	val static PATTERN_NEWLINE = Pattern.compile("\r\n?")
	val static WEBENTITIES_PATH = "webentities"
	val public static Function1<FieldDeclaration, Boolean> filterJsonFields = [
		!static && !final && !transient && annotations.findFirst [
			annotationTypeDeclaration.qualifiedName == JsonIgnore.name
		] === null
	]

	def static <F extends MutableFileSystemSupport & FileLocations> void copyTypeScriptResources(TypeDeclaration decl,
		extension F fileLocations, TypeScriptProcessorConfig typeScriptProcessorConfig) {

		val Procedure1<String> procCopyResource = [ file |
			val s = new String(ByteStreams.toByteArray(TypeScriptProcessor.getResourceAsStream('''resources/�file�''')),
				"UTF-8")
			val tsFile = fileLocations.getProjectFolder(decl.compilationUnit.filePath).append(
				typeScriptProcessorConfig.projectRoot).append(WEBENTITIES_PATH).append(file)
			if (!tsFile.exists) {
				tsFile.contents = s
			}
		]
		procCopyResource.apply("entity.utils.ts")

	}

	def static void appendJavaDoc(StringBuilder sb, String doc) {
		appendJavaDoc(sb, doc, 0)
	}

	def static void appendJavaDoc(StringBuilder sb, String doc, int tabs) {
		if (doc.nullOrEmpty) {
			return
		}
		var sTabs = ""
		for (var i = 0; i < tabs; i++) {
			sTabs += "\t"
		}
		val tabString = sTabs
		sb.append(tabString).append("/**").append("\r\n")
		PATTERN_NEWLINE.split(doc).forEach[sb.append(tabString).append(" * ").append(it).append("\r\n")]
		sb.append(tabString).append(" */").append("\r\n")
	}

	def static String getConvertDataExpression(TypeReference type, extension TransformationContext context) {
		var String convertDataExpression = ""
		var String setNullIfEmptyExpression = ""
		if (type == boolean.newTypeReference) {
			convertDataExpression = '''data == "true"'''
		} else if (type == Boolean.newTypeReference) {
			convertDataExpression = '''data == "true" ? true : data == "false" ? false : null'''
		} else if (Number.newTypeReference.isAssignableFrom(type.wrapperIfPrimitive)) {
			convertDataExpression = '''isNaN(parseFloat(data)) ? �IF type.primitive�0�ELSE�null�ENDIF� : parseFloat(data);'''
		} else if (type == String.newTypeReference) {
			convertDataExpression = '''data'''
		} else if (!type.void) {
			setNullIfEmptyExpression = '''if(!data || typeof data == "string" && data.length == 0) { entity = null; }'''
			if (type.type instanceof AnnotationTarget) {
				val jsonSubTypes = getJsonSubTypes(type.type as AnnotationTarget)
				if (!jsonSubTypes.nullOrEmpty) {
					convertDataExpression = '''�type�.fromPojoSub(data)'''
				}
			}
			if (convertDataExpression.nullOrEmpty) {
				convertDataExpression = '''(new �type�()).fromPojo(data)'''
			}
		}
		return '''
		�IF !type.primitiveIfWrapper.void�let entity : �type.getTypeString(true, true)� = �convertDataExpression�;�ENDIF�
		�setNullIfEmptyExpression�'''
	}

	override final doTransform(List<? extends MutableTypeDeclaration> decls, extension TransformationContext context) {
		decls.forEach[doTransform(context)]
	}

	override final doValidate(List<? extends MutableTypeDeclaration> decls, extension ValidationContext context) {
		decls.forEach[doValidate(context)]
	}

	override final doGenerateCode(List<? extends TypeDeclaration> decls, extension CodeGenerationContext context) {
		decls.forEach[doGenerateCode(context)]
	}

	def abstract void doTransform(extension TypeDeclaration decl, extension TransformationContext context)

	def abstract void doValidate(extension TypeDeclaration decl, extension ValidationContext context)

	def abstract void doGenerateCode(extension TypeDeclaration decl, extension CodeGenerationContext context)

	def static ClassDeclaration getClassDeclarationWithJsonTypeInfoProperty(ClassDeclaration classDef) {
		if (classDef === null) {
			return null
		}
		if (classDef.getJsonTypeInfoProperty !== null) {
			return classDef
		}
		val superClass = classDef.extendedClass
		if (superClass?.type instanceof ClassDeclaration) {
			return (superClass.type as ClassDeclaration).getClassDeclarationWithJsonTypeInfoProperty
		}
		null
	}

	def static AnnotationReference getJsonTypeInfoProperty(AnnotationTarget annTarget) {
		annTarget.annotations.findFirst[annotationTypeDeclaration.qualifiedName == JsonTypeInfo.name]
	}

	def static String getDiscriminatorProperty(AnnotationTarget annTarget) {
		annTarget?.jsonTypeInfoProperty?.getStringValue("property")
	}

	def static List<TypeReference> getJsonSubTypes(AnnotationTarget annTarget) {
		val ann = annTarget.annotations.findFirst[annotationTypeDeclaration.qualifiedName == JsonSubTypes.name]
		ann?.getAnnotationArrayValue("value")?.map[getClassValue("value")]?.toList ?: #[]
	}

	def static String getTypeScriptTypeName(TypeReference _t) {
		var t = _t
		var boolean a = t.array
		if (a) {
			t = t.arrayComponentType
			if (t.primitiveIfWrapper?.type?.qualifiedName == byte.name) {
				return "string" // byte[] -> string
			}
		} else if (t.collection) {
			t = t.actualTypeArguments?.head
			a = true
		}
		var String sType = t.type?.simpleName
		if (sType.nullOrEmpty) {
			if (t.wildCard && t.upperBound !== null /*&& t.upperBound.type !== null*/ ) {
				sType = t.upperBound.typeScriptTypeName
			} else {
				sType = "any"
			}
		}
		if (Object.simpleName == sType) {
			sType = "any"
		} else if (String.simpleName == sType || Boolean.simpleName == sType) {
			sType = sType.toLowerCase
		} else if (t.isNumber) {
			sType = "number"
		} else if (t.date || t.dateVariant) {
			sType = "Date"
		}
		/*if (t.type instanceof EnumerationTypeDeclaration) {
		 * 	// JSON.stringify macht sonst eine number
		 * 	sType = "string"
		 }*/
		if (_t.collection) {
			sType = t.getTypeString(true, true)
		}
		return if (a) '''�sType�[]''' else sType
	}

	def static String getTypeString(TypeParameterDeclarator t, boolean typeName, boolean bounds) {
		if (t.typeParameters.nullOrEmpty) {
			return if(typeName) t.simpleName else ""
		}
		return '''�IF (typeName)��t.simpleName��ENDIF��t.typeParameters.getTypeParametersString(bounds)�'''
	}

	def static String getTypeParametersString(Iterable<? extends TypeParameterDeclaration> typeParameters,
		boolean bounds) {
		val s = typeParameters.map [ typeParam |
			val upperBounds = typeParam.upperBounds.filter[name != Object.name]
			if (!upperBounds.nullOrEmpty && bounds) {
				return '''�typeParam.simpleName� extends �upperBounds.map[getTypeString(true, true)].join(" & ")�'''
			} else {
				return typeParam.simpleName
			}
		].join(", ")
		if (s.nullOrEmpty) {
			return ""
		}
		return '''<�s�>'''
	}

	def static String getTypeString(TypeReference tref, boolean typeName, boolean bounds) {
		if (tref.array) {
			val arrayComponentTypeString = tref.arrayComponentType.getTypeString(typeName, bounds)
			if (typeName) {
				return '''�arrayComponentTypeString�[]'''
			} else {
				return ""
			}
		}
		if (tref.actualTypeArguments.nullOrEmpty) {
			if (typeName) {
				return tref.typeScriptTypeName
			} else {
				return ""
			}
		}
		if (tref.map) {
			val keyType = tref.actualTypeArguments.get(0)
			val valueType = tref.actualTypeArguments.get(1)
			if (!typeName) {
				return ""
			}
			return '''{[key: �keyType.getTypeString(true, true)�]: �valueType.getTypeString(true, true)�}'''
		}
		// let documentsLockedByUser: {[idDocument: string]: User} = {};
		val argStrings = tref.actualTypeArguments.map [ typeParam |
			if (typeParam.upperBound !== null) {
				return typeParam.upperBound.getTypeString(true, true)
			} else {
				return typeParam.getTypeString(true, true)
			}
		]
		if (!typeName) {
			return '''<�argStrings.join(", ")�>'''
		}
		if (tref.collection) {
			return '''�argStrings.join(", ")�[]'''
		}
		return '''�tref.type?.simpleName ?: tref.simpleName�<�argStrings.join(", ")�>'''
	}

	@Pure def static String getJsonFormat(AnnotationTarget annTarget) {
		val pattern = annTarget.annotations.findFirst [
			annotationTypeDeclaration.qualifiedName == JsonFormat.name
		]?.getStringValue("pattern")
		return pattern
	}

	/**
	 * Lists all fields without @JsonIgnore
	 */
	@Pure def static Iterable<? extends FieldDeclaration> getJsonFields(Iterable<? extends FieldDeclaration> fields) {
		fields.filter [
			!static && !transient && !annotations.exists[annotationTypeDeclaration.qualifiedName == JsonIgnore.name]
		]
	}

	@Pure def static String getJsonFieldName(FieldDeclaration it) {
		val jsonFieldName = annotations.findFirst [
			annotationTypeDeclaration.qualifiedName == JsonProperty.name
		]?.getStringValue("value")
		if (jsonFieldName.nullOrEmpty) {
			return simpleName
		}
		return jsonFieldName
	}

	def static String getConvertResponseDataExpression(MethodDeclaration method, String propertyName) {
		getConvertResponseDataExpression(method, method.returnType, method.simpleName, propertyName)
	}

	def static String getConvertResponseDataExpression(ParameterDeclaration param, String propertyName) {
		getConvertResponseDataExpression(param, param.type, param.simpleName, propertyName)
	}

	def static String getConvertResponseDataExpression(AnnotationTarget target, TypeReference tref, String fieldName,
		String propertyName) {
		var String convertDataExpression = ""
		var String setNullIfEmptyExpression = ""
		if (tref.boolean) {
			if (tref.primitive) {
				convertDataExpression = '''data == "true"'''
			} else {
				convertDataExpression = '''data == "true" ? true : data == "false" ? false : null'''
			}
		} else if (tref.number) {
			convertDataExpression = '''isNaN(parseFloat(data)) ? �IF tref.primitive�0�ELSE�null�ENDIF� : parseFloat(data);'''
		} else if (tref.string) {
			convertDataExpression = '''data'''
		} else if (!tref.void) {
			setNullIfEmptyExpression = '''if(!data || typeof data == "string" && data.length == 0) { entity = null; }'''
			if (convertDataExpression.nullOrEmpty) {
				// val entitySimpleName = tref.type?.simpleName ?: "INVALID_TYPE"
				// convertDataExpression = '''�entitySimpleName�.create�entitySimpleName�fromPojo(data)'''
				return '''let �TypeScriptEntitiesProcessor.getLoadFromPojoString(target, tref, fieldName, propertyName, 1)�
				let entity = �fieldName�;'''
			}
		}
		return '''
		�IF !tref.primitiveIfWrapper.void�let entity : �tref.getTypeString(true, true)� = �convertDataExpression�;�ENDIF�
		�setNullIfEmptyExpression�'''
	}
}
