declare var Date: any;

export class HashCodeUtils {
	public static hashCode(obj: any): number {
		if (!obj) {
			return -1;
		}
		if (obj.hashCode) {
			return obj.hashCode();
		}
		if (obj instanceof Date) {
			return (obj as Date).getTime();
		}
		if (typeof obj == "boolean") {
			return obj ? 31 : 23;
		}
		if (typeof obj == "number") {
			return obj as number;
		}
		if (typeof obj == "string") {
			let s: string = obj as string;
			let hash = 0;
			if (s.length == 0) return hash;
			for (let i = 0; i < s.length; i++) {
				let char = s.charCodeAt(i);
				hash = ((hash << 5) - hash) + char;
				hash = hash & hash;
			}
			return hash;
		}
		if (obj instanceof Array) {
			let a: Array<any> = obj as Array<any>;
			let hashCode = 0;
			for (let item of a) {
				hashCode = (hashCode * 31 + this.hashCode(item)) % 1000000;
			}
			return hashCode;
		}
		return -31;
	}
}

export class PojoBuilder {
	public static toPojo(obj: any): any {
		if(!obj) {
			return null;
		}
		if (obj instanceof Date) {
			return (obj as Date).getTime();
		}
		if(obj.toPojo) {
			return obj.toPojo();
		}
		if (obj instanceof Array) {
			let a: Array<any> = obj as Array<any>;
			let arr = {};
			for (let item of a) {
				arr[item] = PojoBuilder.toPojo(a[item]);
			}
			return arr;
		}
		return obj;
	}
}