package es.rick.activeannotations.processor.delegate

import es.rick.activeannotations.GenerateParameterizedServiceDelegates
import es.rick.activeannotations.GenerateParameterizedServicesDelegates
import es.rick.activeannotations.GenerationTarget
import es.rick.activeannotations.ParameterizedDelegate
import es.rick.activeannotations.ParameterizedDelegates
import es.rick.activeannotations.processor.delegate.DelegateProcessor.DelegateConfig
import es.rick.activeannotations.processor.entity.GenerateInterfaceConfig
import es.rick.activeannotations.processor.entity.GenerateInterfaceProcessor
import es.rick.activeannotations.processor.entity.GenerateInterfaceProcessorConfig
import java.util.ArrayList
import java.util.HashSet
import java.util.List
import java.util.Map
import java.util.Set
import java.util.TreeMap
import java.util.TreeSet
import java.util.regex.Pattern
import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtend.lib.annotations.ToString
import org.eclipse.xtend.lib.macro.RegisterGlobalsContext
import org.eclipse.xtend.lib.macro.RegisterGlobalsParticipant
import org.eclipse.xtend.lib.macro.TransformationContext
import org.eclipse.xtend.lib.macro.TransformationParticipant
import org.eclipse.xtend.lib.macro.declaration.AnnotationReference
import org.eclipse.xtend.lib.macro.declaration.ClassDeclaration
import org.eclipse.xtend.lib.macro.declaration.MethodDeclaration
import org.eclipse.xtend.lib.macro.declaration.MutableClassDeclaration
import org.eclipse.xtend.lib.macro.declaration.MutableTypeDeclaration
import org.eclipse.xtend.lib.macro.declaration.ParameterDeclaration
import org.eclipse.xtend.lib.macro.declaration.TypeDeclaration
import org.eclipse.xtend.lib.macro.declaration.TypeReference
import org.eclipse.xtend.lib.macro.declaration.Visibility
import org.eclipse.xtext.xbase.lib.Functions.Function1
import org.eclipse.xtend.lib.macro.services.ProblemSupport
import java.util.function.Supplier
import java.util.Objects

class ParameterizedDelegateProcessor extends DelegateProcessor implements RegisterGlobalsParticipant<TypeDeclaration>, TransformationParticipant<MutableTypeDeclaration> {
	val public static Function1<MethodDeclaration, Boolean> wrapperMethodsFilter = [
		!static && visibility === Visibility.PUBLIC
	]
	val public static Pattern ARG_NO_PARAM_NAME_SET = Pattern.compile("arg\\d+")

	override doRegisterGlobals(List<? extends TypeDeclaration> decls, extension RegisterGlobalsContext context) {
		decls.forEach[doRegisterGlobals(context)]
	}

	def private void doRegisterGlobals(TypeDeclaration decl, extension RegisterGlobalsContext context) {
		decl.readConfig.doRegisterParameterizedDelegates(context)
	}

	def void doRegisterParameterizedDelegates(List<ParameterizedDelegateConfig> configs,
		extension RegisterGlobalsContext context) {
		configs.map[qualifiedNameTarget].forEach[registerClass]
		new GenerateInterfaceProcessor => [
			doRegisterInterfaces(configs.map[generateInterfaceConfigs].flatten.toList, context)
		]
	}

	override doTransform(List<? extends MutableTypeDeclaration> decls, extension TransformationContext context) {
		decls.forEach[doTransform(context)]
	}

	def void doTransform(MutableTypeDeclaration decl, extension TransformationContext context) {
		val configs = decl.readConfig
		configs.forEach [ config |
			if (config.attributes.nullOrEmpty) {
				decl.addError('''�config.qualifiedNameSource� doesn't contain any attributes''')
				return
			}
			val source = context.findTypeGlobally(config.qualifiedNameSource)
			val target = context.findClass(config.qualifiedNameTarget)
			if (!(source instanceof TypeDeclaration)) {
				decl.addError('''�config.qualifiedNameSource� not a class/interface''')
				return
			}
			val propTypes = getParamPropertyTypes(decl, context, source as TypeDeclaration, config.attributes.toSet)
			if (!propTypes.empty) {
				generateWrapperClass(decl, context, source as TypeDeclaration, target, propTypes)
			} else {
				decl.addError('''No parameter found in any parameter list''')
			}
			new GenerateInterfaceProcessor => [
				config.generateInterfaceConfigs.forEach [ generateInterfaceConfig |
					doTransform(decl, context, generateInterfaceConfig)
				]
			]
		]
	}

	def static void generateWrapperClass(MutableTypeDeclaration decl, extension TransformationContext context,
		TypeDeclaration target, MutableClassDeclaration wrapperClazz, Map<String, TypeReference> propTypes) {
		val variables = new TreeMap<String, TypeReference>
		variables.put("target", target.newTypeReference)
		variables.putAll(propTypes)
		variables.entrySet.forEach [ entry |
			wrapperClazz.addField(entry.key, [
				it.visibility = Visibility.PRIVATE
				it.type = Supplier.newTypeReference(entry.value)
			])
		]
		var originalDocComment = target.docComment ?: ""
		if (!originalDocComment.nullOrEmpty) {
			originalDocComment = "\r\n\r\n" + originalDocComment
		}
		wrapperClazz.docComment = '''Wrapper-class for properties �propTypes.keySet.join(", ")� for class �target.qualifiedName��originalDocComment�'''
		wrapperClazz.addConstructor [ constructor |
			constructor.addParameter("target", Supplier.newTypeReference(target.newTypeReference))
			propTypes.entrySet.forEach [ entry |
				constructor.addParameter(entry.key, Supplier.newTypeReference(entry.value))
			]
			constructor.body = '''
			this.target = target;
			�FOR entry : propTypes.entrySet�
				this.�entry.key� = �entry.key�;
			�ENDFOR�'''
		]
		wrapperClazz.addConstructor [ constructor |
			constructor.addParameter("target", target.newTypeReference)
			propTypes.entrySet.forEach [ entry |
				constructor.addParameter(entry.key, entry.value)
			]
			constructor.body = '''
			this.target = () -> target;
			�FOR entry : propTypes.entrySet�
				this.�entry.key� = () -> �entry.key�;
			�ENDFOR�'''
		]
		wrapperClazz.addConstructor[]
		variables.entrySet.forEach [ entry |
			wrapperClazz.addMethod('''get�entry.key.toFirstUpper�''', [
				it.visibility = Visibility.PUBLIC
				it.returnType = entry.value
				it.body = '''�Objects.name�.requireNonNull(this.�entry.key�, "�entry.key� not set");
				return this.�entry.key�.get();'''
			])
			wrapperClazz.addMethod('''set�entry.key.toFirstUpper�''', [
				it.visibility = Visibility.PUBLIC
				it.returnType = void.newTypeReference
				it.addParameter(entry.key, entry.value)
				it.body = '''this.set�entry.key.toFirstUpper�(�entry.key�);'''
			])
			wrapperClazz.addMethod('''set�entry.key.toFirstUpper�''', [
				it.visibility = Visibility.PUBLIC
				it.returnType = void.newTypeReference
				it.addParameter(entry.key, Supplier.newTypeReference(entry.value))
				it.body = '''this.set�entry.key.toFirstUpper�(�entry.key�);'''
			])
		]
		val wrapperMethods = target.allDeclaredMethods.filter(wrapperMethodsFilter)
		wrapperMethods.forEach [ method |
			wrapperClazz.addMethod(
				method.simpleName,
				[ wrapperMethod |
					wrapperMethod.docComment = method.docComment
					wrapperMethod.visibility = method.visibility
					wrapperMethod.returnType = method.returnType
					method.annotations.forEach[ann|wrapperMethod.addAnnotation(ann)]
					val paramSetterStatements = new StringBuilder
					method.parameters.forEach [ param |
						if (!propTypes.containsKey(param.simpleName)) {
							val wrapperParam = wrapperMethod.addParameter(param.simpleName, param.type)
							param.annotations.forEach[ann|wrapperParam.addAnnotation(ann)]
						}
						val propsWithSetters = param.getParamPropertySetter(propTypes)
						propsWithSetters.forEach [ propWithSetter |
							paramSetterStatements.append('''
							if(�param.simpleName� != null) {
								�param.simpleName�.set�propWithSetter.toFirstUpper�(this.get�propWithSetter.toFirstUpper�());
							}''').append("\r\n")
						]
					]
					wrapperMethod.body = '''�paramSetterStatements��IF !method.returnType.void�return �ENDIF�this.getTarget().�method.simpleName�(�method.parameters.map['''�IF propTypes.containsKey(simpleName)�this.get�simpleName.toFirstUpper()�()�ELSE��simpleName��ENDIF�'''].join(", ")�);'''
				]
			)
		]
	}

	def static Set<String> getParamPropertySetter(ParameterDeclaration param, Map<String, TypeReference> propTypes) {
		val propsWithValidSetters = new TreeSet<String>
		/*
		 * Nun nachsehen ob es auch Objekte mit Settern f�r die Properties gibt.
		 * z.B. k�nnte in POST-Requests die Property lediglich im Objekt und nicht
		 * auch als zus�tzlicher URL-Parameter gesetzt sein
		 */
		val paramType = param.type.type
		if (paramType instanceof ClassDeclaration) {
			val paramMethods = paramType.allDeclaredMethods
			propTypes.entrySet.forEach [ entry |
				val propertySetter = paramMethods.findFirst [ paramMethod |
					paramMethod.simpleName == '''set�entry.key.toFirstUpper�'''.toString &&
						paramMethod.parameters?.head?.type?.name == entry.value.name
				]
				if (propertySetter !== null) {
					propsWithValidSetters.add(entry.key)
				}
			]
		}
		return propsWithValidSetters
	}

	def static Map<String, TypeReference> getParamPropertyTypes(TypeDeclaration clazz,
		extension ProblemSupport problemSupport, TypeDeclaration decl, Iterable<String> wrappedProperties) {
		val paramsTypes = new TreeMap<String, Set<TypeReference>>
		decl.allDeclaredMethods.filter(wrapperMethodsFilter).forEach [ method |
			method.parameters.forEach [ param |
				var paramTypes = paramsTypes.get(param.simpleName)
				if (paramTypes === null) {
					paramTypes = new HashSet<TypeReference>
					paramsTypes.put(param.simpleName, paramTypes)
				}
				paramTypes.add(param.type)
			]
		]
		val paramsWithoutValidParamNames = paramsTypes.keySet.filter[ARG_NO_PARAM_NAME_SET.matcher(it).matches]
		if (!paramsWithoutValidParamNames.nullOrEmpty) {
			if (problemSupport !== null) {
				clazz.
					addError('''Some parameter names don't seem to be obtained successfully. You may need to set compiler argument "-parameters" (see https://www.concretepage.com/java/jdk-8/java-8-reflection-access-to-parameter-names-of-method-and-constructor-with-maven-gradle-and-eclipse-using-parameters-compiler-argument#compiler-argument). Invalid parameter names: �paramsWithoutValidParamNames.join(", ")�''')
			}
		}
		val propTypes = new TreeMap<String, TypeReference>
		val errors = wrappedProperties.map [ prop |
			val paramTypes = paramsTypes.get(prop)
			if (paramTypes.nullOrEmpty) {
				clazz.addError('''Property �prop� not used in any parameter list''')
				return 1
			} else if (paramTypes.size > 1) {
				if (problemSupport !== null) {
					clazz.
						addError('''Multiple type definitions found in parameter lists for property �prop�: �paramTypes.map[name].join(", ")�''')
				}
				return 1
			}
			propTypes.put(prop, paramTypes.head)
			return 0
		].reduce[p1, p2|p1 + p2] ?: 0
		if (errors > 0) {
			propTypes.clear
		}
		return propTypes
	}

	def static List<ParameterizedDelegateConfig> readConfig(TypeDeclaration decl) {
		val List<ParameterizedDelegateConfig> l = new ArrayList
		val annGenerateParameterizedServicesDelegates = decl.annotations.findFirst [
			annotationTypeDeclaration.qualifiedName == GenerateParameterizedServicesDelegates.name
		]
		val annGenerateParameterizedServiceDelegates = decl.annotations.findFirst [
			annotationTypeDeclaration.qualifiedName == GenerateParameterizedServiceDelegates.name
		]
		val annParameterizedDelegates = decl.annotations.findFirst [
			annotationTypeDeclaration.qualifiedName == ParameterizedDelegates.name
		]
		val annParameterizedDelegate = decl.annotations.findFirst [
			annotationTypeDeclaration.qualifiedName == ParameterizedDelegate.name
		]
		if (annGenerateParameterizedServicesDelegates !== null) {
			val config = new GenerateParameterizedServicesDelegatesProcessorConfig(
				annGenerateParameterizedServicesDelegates)
			config.servicesDelegates.forEach [ configs |
				configs.delegates.delegates.forEach [ delegateConfig |
					l.add(create(delegateConfig, configs.sourceName, configs.delegates.generationTarget))
				]
			]
		}
		if (annGenerateParameterizedServiceDelegates !== null) {
			val config = new GenerateParameterizedServiceDelegatesProcessorConfig(
				annGenerateParameterizedServiceDelegates)
			config.delegates.delegates.forEach [ delegateConfig |
				l.add(create(delegateConfig, config.sourceName, config.delegates.generationTarget))
			]
		}
		if (annParameterizedDelegates !== null) {
			val delegatesConfig = new ParameterizedDelegatesProcessorConfig(annParameterizedDelegates)
			delegatesConfig.delegates.forEach [ delegateConfig |
				l.add(create(delegateConfig, decl.qualifiedName, delegatesConfig.generationTarget))
			]
		}
		if (annParameterizedDelegate !== null) {
			val delegateConfig = new ParameterizedDelegateProcessorConfig(annParameterizedDelegate)
			l.add(create(delegateConfig, decl.qualifiedName, null))
		}
		// TODO warning for target duplicates
		return l
	}

	def static ParameterizedDelegateConfig create(ParameterizedDelegateProcessorConfig config, String sourceName,
		GenerationTarget generationTargetParent) {
		val generationTarget = config.generationTarget.inherit(generationTargetParent)
		val targetName = sourceName.generateTargetName(generationTarget, config.namePattern)

		val generateInterfaceConfigs = config.generateInterfaces.map [
			GenerateInterfaceProcessor.create(it, targetName, generationTarget)
		]
		new ParameterizedDelegateConfig => [
			it.qualifiedNameSource = sourceName
			it.qualifiedNameTarget = targetName
			it.attributes = config.attributes
			it.generateInterfaceConfigs = generateInterfaceConfigs
		]
	}
}

@ToString @Accessors
class ParameterizedDelegateConfig extends DelegateConfig {
	List<String> attributes
	List<GenerateInterfaceConfig> generateInterfaceConfigs
}

@ToString @Accessors
class GenerateParameterizedServicesDelegatesProcessorConfig {
	val GenerateParameterizedServiceDelegatesProcessorConfig[] servicesDelegates

	new(AnnotationReference ann) {
		servicesDelegates = ann.getAnnotationArrayValue("servicesDelegates").map [
			new GenerateParameterizedServiceDelegatesProcessorConfig(it)
		]
	}
}

@ToString @Accessors
class GenerateParameterizedServiceDelegatesProcessorConfig {
	val TypeReference interfaceClass
	val String interfaceName
	val ParameterizedDelegatesProcessorConfig delegates

	new(AnnotationReference ann) {
		interfaceClass = ann.getClassValue("interfaceClass")
		interfaceName = ann.getStringValue("interfaceName")
		delegates = new ParameterizedDelegatesProcessorConfig(ann.getAnnotationValue("delegates"))
	}

	def String getSourceName() {
		if (interfaceClass !== null && interfaceClass.type.qualifiedName != Object.name) {
			return interfaceClass.name
		}
		return interfaceName
	}
}

@ToString @Accessors
class ParameterizedDelegatesProcessorConfig {
	val GenerationTarget generationTarget
	val ParameterizedDelegateProcessorConfig[] delegates

	new(AnnotationReference ann) {
		generationTarget = GenerationTarget.valueOf(ann.getEnumValue("generationTarget").simpleName)
		delegates = ann.getAnnotationArrayValue("delegates").map[new ParameterizedDelegateProcessorConfig(it)]
	}
}

@ToString @Accessors
class ParameterizedDelegateProcessorConfig {
	val String[] attributes
	val NamePatternProcessorConfig namePattern
	val GenerationTarget generationTarget
	val GenerateInterfaceProcessorConfig[] generateInterfaces

	new(AnnotationReference ann) {
		attributes = ann.getStringArrayValue("attributes")
		namePattern = new NamePatternProcessorConfig(ann.getAnnotationValue("namePattern"))
		generationTarget = GenerationTarget.valueOf(ann.getEnumValue("generationTarget").simpleName)
		generateInterfaces = ann.getAnnotationArrayValue("generatedInterfaces").map [
			new GenerateInterfaceProcessorConfig(it)
		]
	}
}

@ToString @Accessors
class NamePatternProcessorConfig {
	val String prefix
	val String suffix
	val String regex
	val String replace

	new(AnnotationReference ann) {
		prefix = ann.getStringValue("prefix")
		suffix = ann.getStringValue("suffix")
		regex = ann.getStringValue("regex")
		replace = ann.getStringValue("replace")
	}
}
