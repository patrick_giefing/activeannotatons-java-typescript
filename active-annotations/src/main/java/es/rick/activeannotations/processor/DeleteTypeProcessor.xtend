package es.rick.activeannotations.processor

import org.eclipse.xtend.lib.macro.TransformationParticipant
import org.eclipse.xtend.lib.macro.declaration.MutableTypeDeclaration
import java.util.List
import org.eclipse.xtend.lib.macro.TransformationContext

class DeleteTypeProcessor implements TransformationParticipant<MutableTypeDeclaration> {
	override doTransform(List<? extends MutableTypeDeclaration> decls, extension TransformationContext context) {
		decls.forEach[remove]
	}
}
