package es.rick.activeannotations.processor.typescript

import java.time.LocalDate
import java.time.LocalDateTime
import java.util.ArrayList
import java.util.Date
import java.util.HashSet
import java.util.Map
import java.util.Set
import java.util.TreeMap
import org.eclipse.xtend.lib.macro.declaration.ClassDeclaration
import org.eclipse.xtend.lib.macro.declaration.TypeDeclaration
import org.eclipse.xtend.lib.macro.declaration.TypeParameterDeclarator
import org.eclipse.xtend.lib.macro.declaration.TypeReference
import org.eclipse.xtext.xbase.lib.Functions.Function1

import static extension es.rick.activeannotations.processor.ProcessorBase.*
import org.eclipse.xtend.lib.macro.services.ProblemSupport
import org.eclipse.xtend.lib.macro.declaration.MutableTypeDeclaration
import java.util.regex.Pattern
import static extension com.google.common.base.Strings.*

class TypeScriptImporter {
	/**
	 * Filtert primitive/Wrapper-Typen sowie Object/String/Date/Number
	 */
	val public static Function1<TypeReference, Boolean> filterTypeFields = [
		!primitive && !wrapper && !void && Object.name != name && String.name != name && Date.name != name &&
			Number.name != name && LocalDate.name != name && LocalDateTime.name != name
	]

	/**
	 * Sucht die in der Klasse verwendeten Typen welche, wenn sie nicht im selben File definiert werden, importiert werden müssen.
	 * Folgende Typen werden gesucht:
	 * 1) Member-Typen (nur von verwendeten Membern, also nicht @JsonIgnore, static, final)
	 * 2) Übergeordnete Member-Klasse mit @JsonSubTypes-Annotation. Der Import ist notwendig da vom Klassen-Root aus die Felder befüllt werden.
	 * 3) Methoden-Rückgabewerte sowie Parameter-Typen
	 * 4) Übergeordnete Klasse
	 * Wenn einer der Typen eine Iterable (oder abgeleitete Klasse ist) so wird der 1. Typ-Parameter herangezogen.
	 */
	def static Set<TypeReference> getUsedTypes(TypeDeclaration clazz) {
		val fieldTypes = clazz.declaredFields.filter(TypeScriptProcessor.filterJsonFields).map[type]
		val methodTypes = clazz.declaredMethods.map[#[returnType] + parameters.map[type]].flatten
		val jsonSubTypes = TypeScriptProcessor.getJsonSubTypes(clazz)
		var types = fieldTypes + methodTypes + jsonSubTypes
		if (clazz instanceof ClassDeclaration) {
			if (clazz.extendedClass !== null) {
				types = types + #[clazz.extendedClass]
			}
		}
		// Wenn einer der Typen eine Iterable (oder abgeleitete Klasse ist) so wird der 1. Typ-Parameter herangezogen.
		types = types.map [ type |
			if (type.iterable) {
				return type.actualTypeArguments.head
			}
			return type
		].filterNull.filter(filterTypeFields)
		types = types.map [ type |
			val t = type.type
			if (t instanceof TypeParameterDeclarator) {
				// TODO Type-Parameter rekursiv durchsuchen
				return #[#[type], type.actualTypeArguments].flatten
			}
			return #[type]
		].flatten.toSet
		// TODO do we still need the following information?
		/*val fieldJsonSubTypesRoot = fieldTypes.filter[type instanceof ClassDeclaration].map[
		 * 	TypeScriptProcessor.getClassDeclarationWithJsonTypeInfoProperty(type as ClassDeclaration)
		 ].filterNull*/
		// filter primitives, wrappers and other basic types which don't need import statements
		types = types.filter [
			!primitiveIfWrapper.primitive && // int, Integer, float, Float ...
			!name.startsWith("java.util.") && // Map, List, Date
			!name.startsWith("java.lang.") && // String
			!name.startsWith("java.time.") // LocalTime, LocalDate, LocalDateTime
		]
		return types.toSet
	}

	/**
	 * Liefert eine Map mit
	 * <dl>
	 * 	<dd>key</dd><dt>TypeScript-File</dt>
	 * 	<dd>value</dd><dt>Aus diesem File zu importierende Typen</dt>
	 * </dl>
	 */
	def static Map<String, Set<String>> getTypeScriptLibraryTypes(TypeScriptProcessorConfig config,
		TypeDeclaration clazz) {
		val m = new TreeMap<String, Set<String>>
		val imports = config.entityImports
		imports.fileImports.forEach [ fileImports |
			m.put(fileImports.filePath, fileImports.entities.allEntityClasses.map[type.qualifiedName].toSet)
		]
		return m
	}

	def static String createTypeScriptImports(TypeScriptProcessorConfig config,
		Iterable<TypeReference> entityAndServiceClasses, Iterable<TypeReference> parentEntityClasses,
		TypeDeclaration clazz, ProblemSupport problemSupport) {
		var usedTypeNames = entityAndServiceClasses.filter[type instanceof TypeDeclaration].
			map[type as TypeDeclaration].map[usedTypes].flatten.map[type?.qualifiedName].filterNull.toSet
		usedTypeNames.removeAll(entityAndServiceClasses.map[name]) // Die in diesem .ts definierten Typen von den zu importierenden Typen entfernen
//		if(problemSupport !== null) {
//			problemSupport.addWarning(clazz, entityAndServiceClasses.map[name].join("\r\n"))
//		}
		// Typen ohne Punkt herausfiltern, dabei handelt es sich wohl um Type-Parameter-Namen
		usedTypeNames = usedTypeNames.filter[it.contains(".")].toSet
		val typeScriptLibrary = config.getTypeScriptLibraryTypes(clazz)
		val importedTypeNames = typeScriptLibrary.values.flatten.toSet
//		if(problemSupport !== null) {
//			problemSupport.addWarning(clazz, importedTypeNames.join("\r\n"))
//		}
		val usedTypeNamesWithoutImport = new ArrayList<String>(usedTypeNames)
		usedTypeNamesWithoutImport.removeAll(importedTypeNames)
		if (!usedTypeNamesWithoutImport.empty) {
			if (problemSupport !== null && clazz instanceof MutableTypeDeclaration) {
				problemSupport.addError(clazz, '''
				Some imports couldn't be resolved, define attribute entityImports: 
				�usedTypeNamesWithoutImport.join("\r\n")�''')
			}
		}
		val importFileMap = new TreeMap<String, String>
		typeScriptLibrary.entrySet.forEach [ entry |
			val file = entry.key
			val imports = entry.value.filterNull
			imports.forEach [ imp |
				val existingImport = importFileMap.get(imp)
				if (!existingImport.nullOrEmpty) {
					if (problemSupport !== null && clazz instanceof MutableTypeDeclaration) {
						problemSupport.
							addError(clazz, '''Import �imp.simpleName� from �file� also exists in �existingImport�''')
					}
				}
				importFileMap.put(imp, file)
			]
		]
		val importMap = new TreeMap<String, Set<String>>
		usedTypeNames.filterNull.forEach [ usedType |
			val importFile = importFileMap.get(usedType)
			if (importFile !== null) {
				var typeSet = importMap.get(importFile)
				if (typeSet === null) {
					typeSet = new HashSet<String>
					importMap.put(importFile, typeSet)
				}
				typeSet.add(usedType)
			}
		]
		val parentEntityClassesSimpleNames = parentEntityClasses.map[simpleName].toSet
		val s = importMap.entrySet.map [
			var tsFile = key
			if (tsFile.endsWith(".ts")) {
				tsFile = key.substring(0, key.length - 3)
			}
			'''import {�value.map[
				if(parentEntityClassesSimpleNames.contains(simpleName)) {
					return #[simpleName, '''�simpleName�Accessors''']
				} else {
					return #[simpleName]
				}
			].flatten.join(", ")�} from "�tsFile.translateRelative(config.filePath)�";'''
		].join("\r\n")
		return s
	}

	val static PATTERN_PATH_SPLIT = Pattern.compile('''[\/]+''')

	def static String translateRelative(String path, String basePath) {
		if (path.nullOrEmpty || basePath.nullOrEmpty) {
			return path
		}
		var pathParts = PATTERN_PATH_SPLIT.split(path).filter[!nullOrEmpty]
		var basePathParts = PATTERN_PATH_SPLIT.split(basePath).filter[!nullOrEmpty]
		var int commonParts = 0
		var b = false
		for (; !b && commonParts < Math.min(pathParts.size, basePathParts.size); commonParts++) {
			if (pathParts.get(commonParts) != basePathParts.get(commonParts)) {
				b = true
				commonParts--
			}
		}
		pathParts = pathParts.drop(commonParts)
		basePathParts = basePathParts.drop(commonParts)
		val goUp = basePathParts.drop(1).map["../"].join
		#[goUp.emptyToNull ?: "./", pathParts.join("/")].join
	}
}
