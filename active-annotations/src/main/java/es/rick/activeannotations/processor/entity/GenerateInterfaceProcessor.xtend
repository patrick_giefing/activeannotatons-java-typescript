package es.rick.activeannotations.processor.entity

import es.rick.activeannotations.GenerateInterface
import es.rick.activeannotations.GenerateInterfaces
import es.rick.activeannotations.GenerationTarget
import es.rick.activeannotations.processor.delegate.NamePatternProcessorConfig
import java.util.ArrayList
import java.util.List
import java.util.Set
import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtend.lib.annotations.ToString
import org.eclipse.xtend.lib.macro.RegisterGlobalsContext
import org.eclipse.xtend.lib.macro.RegisterGlobalsParticipant
import org.eclipse.xtend.lib.macro.TransformationContext
import org.eclipse.xtend.lib.macro.TransformationParticipant
import org.eclipse.xtend.lib.macro.declaration.AnnotationReference
import org.eclipse.xtend.lib.macro.declaration.InterfaceDeclaration
import org.eclipse.xtend.lib.macro.declaration.MutableClassDeclaration
import org.eclipse.xtend.lib.macro.declaration.MutableInterfaceDeclaration
import org.eclipse.xtend.lib.macro.declaration.MutableTypeDeclaration
import org.eclipse.xtend.lib.macro.declaration.TypeDeclaration
import org.eclipse.xtend.lib.macro.declaration.TypeReference
import org.eclipse.xtend.lib.macro.declaration.Visibility

import static extension es.rick.activeannotations.processor.ProcessorBase.*
import static extension es.rick.activeannotations.processor.delegate.DelegateProcessor.*
import org.eclipse.xtend.lib.macro.declaration.MutableMethodDeclaration

class GenerateInterfaceProcessor implements RegisterGlobalsParticipant<TypeDeclaration>, TransformationParticipant<MutableTypeDeclaration> {
	override doRegisterGlobals(List<? extends TypeDeclaration> decls, extension RegisterGlobalsContext context) {
		decls.forEach[doRegisterGlobals(context)]
	}

	def void doRegisterGlobals(TypeDeclaration decl, extension RegisterGlobalsContext context) {
		decl.readConfig.doRegisterInterfaces(context)
	}

	def void doRegisterInterfaces(List<GenerateInterfaceConfig> configs, extension RegisterGlobalsContext context) {
		configs.map[qualifiedNameTarget].forEach[registerInterface]
	}

	override doTransform(List<? extends MutableTypeDeclaration> decls, extension TransformationContext context) {
		decls.forEach[doTransform(context)]
	}

	def void doTransform(MutableTypeDeclaration decl, extension TransformationContext context) {
		decl.readConfig.forEach[doTransform(decl, context, it)]
	}

	def void doTransform(MutableTypeDeclaration decl, extension TransformationContext context,
		GenerateInterfaceConfig config) {
		val copyAnnotations = false
		val interfaze = context.findInterface(config.qualifiedNameTarget)
		if (interfaze === null) {
			decl.addError('''Target interface not found: «config.qualifiedNameTarget»''')
		}
		val extendInterfaces = config.extendInterfaceNames.map[context.findTypeGlobally(it)]
		val notInterfaces = extendInterfaces.filter[!(it instanceof InterfaceDeclaration)]
		if (!notInterfaces.nullOrEmpty) {
			decl.addError('''Not an interface: «notInterfaces.map[qualifiedName].join(", ")»''')
			return
		}
		interfaze.extendedInterfaces = extendInterfaces.map[newSelfTypeReference]
		if (decl instanceof MutableClassDeclaration) {
			decl.implementedInterfaces = decl.implementedInterfaces + #[interfaze.newSelfTypeReference]
		} else if (decl instanceof MutableInterfaceDeclaration) {
			decl.extendedInterfaces = decl.extendedInterfaces + #[interfaze.newSelfTypeReference]
		}
		val includePatterns = config.includeMethodPattern
		val excludePatterns = config.excludeMethodPattern
		val interfaceMethods = decl.allDeclaredMethods.filter[!static && visibility === Visibility.PUBLIC].filter [ method |
			var positiveMatch = true
			if (!includePatterns.nullOrEmpty) {
				positiveMatch = includePatterns.exists[method.simpleName.isPatternMatch(it)]
			}
			var negativeMatch = false
			if (!excludePatterns.nullOrEmpty) {
				negativeMatch = excludePatterns.exists[method.simpleName.isPatternMatch(it)]
			}
			return positiveMatch && !negativeMatch
		]
		interfaceMethods.forEach [ method |
			interfaze.addMethod(method.simpleName, [ newMethod |
				newMethod.returnType = method.returnType
				if (copyAnnotations) {
					method.annotations.filter[annotationTypeDeclaration.qualifiedName != Override.name].forEach [
						newMethod.addAnnotation(it)
					]
				}
				method.parameters.forEach [ param |
					val newParam = newMethod.addParameter(param.simpleName, param.type)
					if (copyAnnotations) {
						param.annotations.forEach[newParam.addAnnotation(it)]
					}
				]
			])
			// Not sure why the following lines produce compiler errors
			/*if (method instanceof MutableMethodDeclaration) {
				method.addAnnotation(Override.newAnnotationReference)
			}*/
		]
	}

	def static boolean isPatternMatch(String name, MethodPatternProcessorConfig pattern) {
		if (pattern === null) {
			return true
		}
		if (!pattern.prefix.nullOrEmpty) {
			return name.startsWith(pattern.prefix)
		}
		if (!pattern.suffix.nullOrEmpty) {
			return name.endsWith(pattern.suffix)
		}
		if (!pattern.regex.nullOrEmpty) {
			return name.matches(pattern.regex)
		}
		return true
	}

	def static List<GenerateInterfaceConfig> readConfig(TypeDeclaration decl) {
		val annGenerateInterface = decl.annotations.findFirst [
			annotationTypeDeclaration.qualifiedName == GenerateInterface.name
		]
		val annGenerateInterfaces = decl.annotations.findFirst [
			annotationTypeDeclaration.qualifiedName == GenerateInterfaces.name
		]
		val List<GenerateInterfaceProcessorConfig> configs = new ArrayList
		if (annGenerateInterface !== null) {
			configs.add(new GenerateInterfaceProcessorConfig(annGenerateInterface))
		}
		if (annGenerateInterfaces !== null) {
			val config = new GenerateInterfacesProcessorConfig(annGenerateInterfaces)
			configs.addAll(config.configs)
		}
		return configs.map[create(it, decl.qualifiedName, null)]
	}

	def static GenerateInterfaceConfig create(GenerateInterfaceProcessorConfig config, String sourceName,
		GenerationTarget generationTargetParent) {
		val targetName = sourceName.generateTargetName(GenerationTarget.CLASS, config.namePattern)
		new GenerateInterfaceConfig => [
			it.qualifiedNameTarget = targetName
			it.extendInterfaceNames = config.extendInterfaceNames
			it.includeMethodPattern = config.includeMethodPattern
			it.excludeMethodPattern = config.excludeMethodPattern
		]
	}
}

@ToString @Accessors
class GenerateInterfaceConfig {
	String qualifiedNameTarget
	Set<String> extendInterfaceNames
	MethodPatternProcessorConfig[] includeMethodPattern
	MethodPatternProcessorConfig[] excludeMethodPattern
}

@ToString @Accessors
class GenerateInterfacesProcessorConfig {
	val GenerateInterfaceProcessorConfig[] configs

	new(AnnotationReference ann) {
		configs = ann.getAnnotationArrayValue("value").map[new GenerateInterfaceProcessorConfig(it)]
	}
}

@ToString @Accessors
class GenerateInterfaceProcessorConfig {
	val TypeReference[] interfaces
	val String[] interfaceNames
	val NamePatternProcessorConfig namePattern
	val MethodPatternProcessorConfig[] includeMethodPattern
	val MethodPatternProcessorConfig[] excludeMethodPattern

	new(AnnotationReference ann) {
		interfaces = ann.getClassArrayValue("interfaces")
		interfaceNames = ann.getStringArrayValue("interfaceNames")
		namePattern = new NamePatternProcessorConfig(ann.getAnnotationValue("namePattern"))
		includeMethodPattern = ann.getAnnotationArrayValue("includeMethodPattern").map [
			new MethodPatternProcessorConfig(it)
		]
		excludeMethodPattern = ann.getAnnotationArrayValue("excludeMethodPattern").map [
			new MethodPatternProcessorConfig(it)
		]
	}

	def Set<String> getExtendInterfaceNames() {
		return (interfaceNames + interfaces.map[name]).toSet
	}
}

@ToString @Accessors
class MethodPatternProcessorConfig {
	val String prefix
	val String suffix
	val String regex

	new(AnnotationReference ann) {
		prefix = ann.getStringValue("prefix")
		suffix = ann.getStringValue("suffix")
		regex = ann.getStringValue("regex")
	}
}
