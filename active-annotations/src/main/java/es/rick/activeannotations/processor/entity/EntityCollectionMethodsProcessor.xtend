package es.rick.activeannotations.processor.entity

import java.util.ArrayList
import java.util.Collection
import java.util.HashSet
import java.util.List
import java.util.Set
import javax.persistence.Transient
import org.eclipse.xtend.lib.macro.TransformationContext
import org.eclipse.xtend.lib.macro.declaration.AnnotationTarget
import org.eclipse.xtend.lib.macro.declaration.Element
import org.eclipse.xtend.lib.macro.declaration.FieldDeclaration
import org.eclipse.xtend.lib.macro.declaration.MutableClassDeclaration
import org.eclipse.xtend.lib.macro.declaration.MutableFieldDeclaration
import org.eclipse.xtend.lib.macro.declaration.TypeReference
import es.rick.activeannotations.Singular

class EntityCollectionMethodsProcessor extends EntityBaseProcessor {
	override transformEntity(extension MutableClassDeclaration clazz, extension TransformationContext context,
		extension EntityProcessorConfig entityConfig) {
		val recreateOnUnsupportedOperationException = entityConfig.collectionMethodsConfig.
			recreateOnUnsupportedOperationException
		declaredFields.filter [
			!final && !static && !type.isArray && (
					List.newTypeReference.isAssignableFrom(type) || Set.newTypeReference.isAssignableFrom(type))
		].forEach [
			transform(it, context, recreateOnUnsupportedOperationException)
		]
	}

	def void transform(extension MutableFieldDeclaration field, extension TransformationContext context,
		boolean recreateOnUnsupportedOperationException) {
		addCollectionMethods(findClass(field.declaringType.qualifiedName), field, context,
			recreateOnUnsupportedOperationException)
	}

	def static void addCollectionMethods(extension MutableClassDeclaration clazz,
		extension MutableFieldDeclaration field, extension TransformationContext context,
		boolean recreateOnUnsupportedOperationException) {
		addCollectionMethods(clazz, field, context, null, null, recreateOnUnsupportedOperationException)
	}

	def static void addCollectionMethods(extension MutableClassDeclaration clazz, extension FieldDeclaration field,
		extension TransformationContext context, MutableClassDeclaration returnType, String builderEntity,
		boolean recreateOnUnsupportedOperationException) {
		val String singular = getSingular(field)
		if (field.simpleName.equals(singular)) {
			field.addError('''Couldn't build singular from field name �field.simpleName�, use @�Singular.name�''')
		}
		val _type = getType(field, context)
		val isSet = Set.newTypeReference.isAssignableFrom(_type)
		val String collType = _type.name
		val collectionEntityType = if (_type.actualTypeArguments.head.wildCard) //
				_type.actualTypeArguments.head.upperBound
			else
				_type.actualTypeArguments.head
		val TypeReference collectionTypeNew = getCollectionTypeNew(field, context)
		val boolean isBuilder = builderEntity !== null && returnType !== null

		if (!_type.actualTypeArguments.head.wildCard) {
			clazz.addMethod('''add�singular.toFirstUpper�''', [
				it.primarySourceElement = field.primarySourceElement
				addParameter(singular, collectionEntityType)
				if (isBuilder) {
					returnType = clazz.newSelfTypeReference
					body = '''
					�collType� coll = this.�builderEntity�.get�field.simpleName.toFirstUpper�();
					if(null == coll){
						coll = new �collectionTypeNew�();
						this.�builderEntity�.set�field.simpleName.toFirstUpper�(coll);
					}
					coll.add(�singular�);
					return this;'''
				} else {
					body = '''
						if(null == this.�field.simpleName�) {
							this.�field.simpleName� = new �collectionTypeNew�();
						}
						�IF recreateOnUnsupportedOperationException�
							try {
								this.�field.simpleName�.add(�singular�);
							} catch (�UnsupportedOperationException.name� ex) {
								this.�field.simpleName� = new �collectionTypeNew�(this.�field.simpleName�);
								this.�field.simpleName�.add(�singular�);
							}
						�ELSE�
							this.�field.simpleName�.add(�singular�);
						�ENDIF�
					'''

				}
			])

			clazz.addMethod('''add�field.simpleName.toFirstUpper�''', [
				it.primarySourceElement = field.primarySourceElement
				addParameter(field.simpleName, Collection.newTypeReference(collectionEntityType))
				if (isBuilder) {
					returnType = clazz.newSelfTypeReference
					body = '''
					if(�field.simpleName� == null || �field.simpleName�.isEmpty()) {
						return this;
					}
					�collType� coll = this.�builderEntity�.get�field.simpleName.toFirstUpper�();
					if(null == coll){
						coll = new �collectionTypeNew�();
						this.�builderEntity�.set�field.simpleName.toFirstUpper�(coll);
					}
					coll.addAll(�field.simpleName�);
					return this;'''
				} else {
					body = '''
						if(�field.simpleName� == null || �field.simpleName�.isEmpty()) {
							return;
						}
						if(null == this.�field.simpleName�){
							this.�field.simpleName� = new �collectionTypeNew�();
						}
						�IF recreateOnUnsupportedOperationException�
							try	{
								this.�field.simpleName�.addAll(�field.simpleName�);
							} catch (�UnsupportedOperationException.name� ex) {
								this.�field.simpleName� = new �collectionTypeNew�(this.�field.simpleName�);
								this.�field.simpleName�.addAll(�field.simpleName�);
							}
						�ELSE�
							this.�field.simpleName�.addAll(�field.simpleName�);
						�ENDIF�
					'''
				}
			])
		}
		clazz.addMethod('''clear�field.simpleName.toFirstUpper�''', [
			it.primarySourceElement = field.primarySourceElement
			if (isBuilder) {
				returnType = clazz.newSelfTypeReference
				body = '''
				�collType� coll = this.�builderEntity�.get�field.simpleName.toFirstUpper�();
				if(null == coll){
					return this;
				}
				coll.clear();
				return this;'''
			} else {
				body = '''
				if(null == this.�field.simpleName�) {
					return;
				}
				try{
					this.�field.simpleName�.clear();
				} catch (�UnsupportedOperationException.name� ex) {
					this.�field.simpleName� = new �collectionTypeNew�();
				}'''
			}
		])
		if (!isBuilder) {
			clazz.addMethod('''�field.simpleName�Size''', [
				it.primarySourceElement = field.primarySourceElement
				addAnnotation(Transient.newAnnotationReference)
				returnType = int.newTypeReference
				body = '''
				if(null == this.�field.simpleName�) {
					return 0;
				}
				return �field.simpleName�.size();'''
			])
			if (!isSet) {
				clazz.addMethod('''first�singular.toFirstUpper�''', [
					it.primarySourceElement = field.primarySourceElement
					addAnnotation(Transient.newAnnotationReference)
					returnType = collectionEntityType
					body = '''
					if(null == this.�field.simpleName� || this.�field.simpleName�.isEmpty()) {
						return null;
					}
					return �field.simpleName�.get(0);'''
				])
				clazz.addMethod('''�singular�''', [
					it.primarySourceElement = field.primarySourceElement
					addAnnotation(Transient.newAnnotationReference)
					addParameter("index", int.newTypeReference)
					returnType = collectionEntityType
					body = '''
					if(null == this.�field.simpleName� || index < 0 || this.�field.simpleName�.size() <= index) {
						return null;
					}
					return �field.simpleName�.get(index);'''
				])
			}
			clazz.addMethod('''�field.simpleName�Empty''', [
				it.primarySourceElement = field.primarySourceElement
				addAnnotation(Transient.newAnnotationReference)
				returnType = boolean.newTypeReference
				body = '''
				if(null == this.�field.simpleName�) {
					return true;
				}
				return this.�field.simpleName�.isEmpty();'''
			])
			// remove(int index) only for List, not for Set
			if (List.newTypeReference.isAssignableFrom(field.type)) {
				clazz.addMethod('''remove�singular.toFirstUpper�''', [
					it.primarySourceElement = field.primarySourceElement
					addAnnotation(Transient.newAnnotationReference)
					addParameter("index", int.newTypeReference)
					returnType = collectionEntityType
					body = '''
					if(null == this.�field.simpleName� || index < 0 || index >= this.�field.simpleName�.size()) {
						return null;
					}
					�IF recreateOnUnsupportedOperationException�
						try{
							return this.�field.simpleName�.remove(index);
						} catch (�UnsupportedOperationException.name� ex) {
							this.�field.simpleName� = new �collectionTypeNew�(this.�field.simpleName�);
							return this.�field.simpleName�.remove(index);
						}
					�ELSE�
						return this.�field.simpleName�.remove(index);
					�ENDIF�'''
				])
			}
			if (collectionEntityType === null) {
				return
			}
			if (!collectionEntityType.equals(Integer.newTypeReference) &&
				!collectionEntityType.equals(int.newTypeReference)) {
				clazz.addMethod('''remove�singular.toFirstUpper�''', [
					it.primarySourceElement = field.primarySourceElement
					addAnnotation(Transient.newAnnotationReference)
					addParameter(singular, collectionEntityType)
					returnType = boolean.newTypeReference
					body = '''
					if(null == this.�field.simpleName�) {
						return false;
					}
					�IF recreateOnUnsupportedOperationException�
						try{
							return this.�field.simpleName�.remove(�singular�);
						} catch (�UnsupportedOperationException.name� ex) {
							this.�field.simpleName� = new �collectionTypeNew�(this.�field.simpleName�);
							return this.�field.simpleName�.remove(�singular�);
						}
					�ELSE�
						return this.�field.simpleName�.remove(�singular�);
					�ENDIF�'''
				])
			}
		}
	}

	def static String getSingular(@Extension AnnotationTarget annTarget) {
		var String _singular = annTarget.annotations.findFirst [
			Singular.name.equals(annotationTypeDeclaration.qualifiedName)
		]?.getStringValue("value")
		if (!_singular.nullOrEmpty) {
			return _singular
		}
		return '''�annTarget.simpleName�Entry'''
	}

	def static TypeReference getCollectionTypeNew(@Extension AnnotationTarget annTarget,
		@Extension TransformationContext context) {
		getCollectionTypeNew(annTarget, annTarget, context)
	}

	def static TypeReference getCollectionTypeNew(@Extension AnnotationTarget annTarget, Element errorElement,
		@Extension TransformationContext context) {
		val _type = getType(annTarget, errorElement, context)
		val defaultType = getDefaultType(annTarget, errorElement, context)
		if (defaultType !== null) {
			return defaultType
		}
		if (_type.array) {
			return _type
		}
		if (List.name.equals(_type.type?.qualifiedName)) {
			return ArrayList.newTypeReference(_type.actualTypeArguments.head.upperBound)
		} else if (Set.name.equals(_type.type?.qualifiedName)) {
			return HashSet.newTypeReference(_type.actualTypeArguments.head.upperBound)
		}
		return _type
	}
}
