package es.rick.activeannotations.processor.typescript

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import es.rick.activeannotations.EntityGroup
import es.rick.activeannotations.TypeScriptEntities
import es.rick.activeannotations.TypeScriptFileImport
import es.rick.activeannotations.TypeScriptImports
import java.util.List
import java.util.Map
import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtend.lib.annotations.ToString
import org.eclipse.xtend.lib.macro.CodeGenerationContext
import org.eclipse.xtend.lib.macro.TransformationContext
import org.eclipse.xtend.lib.macro.ValidationContext
import org.eclipse.xtend.lib.macro.declaration.AnnotationReference
import org.eclipse.xtend.lib.macro.declaration.AnnotationTarget
import org.eclipse.xtend.lib.macro.declaration.ClassDeclaration
import org.eclipse.xtend.lib.macro.declaration.EnumerationTypeDeclaration
import org.eclipse.xtend.lib.macro.declaration.FieldDeclaration
import org.eclipse.xtend.lib.macro.declaration.InterfaceDeclaration
import org.eclipse.xtend.lib.macro.declaration.TypeDeclaration
import org.eclipse.xtend.lib.macro.declaration.TypeReference
import org.eclipse.xtend.lib.macro.services.GlobalTypeLookup
import es.rick.activeannotations.processor.entity.EntitiesProcessorConfig
import java.util.ArrayList

/**
 * TODO Validation: Non-abstract classes with type arguments
 * TODO Generic field has to be loaded in sub classes
 */
class TypeScriptEntitiesProcessor extends TypeScriptProcessor {
	override doTransform(extension TypeDeclaration decl, extension TransformationContext context) {
		// decl.addWarning(Date.inheritedTypeNames.join(", "))
		/*val tsEntitiesConfig = decl.readConfig
		 * val entities = tsEntitiesConfig.entities.allEntityClasses.filter[type instanceof ClassDeclaration]
		 * val listType = entities.map [ entity |
		 * 	val entityType = entity.type as ClassDeclaration
		 * 	val jsonFields = entityType.declaredFields.jsonFields
		 * 	return jsonFields
		 * ].flatten.filter[it.type.name.startsWith(List.name)].head
		 * if(listType !== null) {
		 * 	decl.addWarning(listType.type.type.inheritedTypeNames.join(", "))
		 }*/
	}

	override doValidate(extension TypeDeclaration decl, extension ValidationContext context) {
		val tsEntitiesConfig = decl.readConfig
		val allEntityClasses = tsEntitiesConfig.entities.allEntityClasses
		val entities = allEntityClasses.filter[type instanceof ClassDeclaration]
		entities.forEach [ entity |
			val entityType = entity.type as ClassDeclaration

			val discriminator = entityType.discriminatorProperty
			val subTypes = entityType.jsonSubTypes
//			if((discriminator.nullOrEmpty || subTypes.nullOrEmpty) && entityType.abstract) {
//				decl.addError('''Entity �entityType.simpleName�: Type is abstract and doesn't define JsonTypeInfo.property or JsonSubTypes.value''')
//			}
			val jsonFields = entityType.declaredFields.jsonFields
			jsonFields.forEach [ field |
				val fieldPrefix = '''�entity.simpleName�.�field.simpleName�: '''
				if (field.type.isLocalDate || field.type.isLocalTime || field.type.isLocalDateTime) {
					val pattern = field.jsonFormat
					if (pattern.nullOrEmpty) {
						decl.addError('''�fieldPrefix�@�JsonFormat.name� doesn't exist''')
					}
				}
				if (Map.isAssignableFrom(field.type.type)) {
					val keyType = field.type.actualTypeArguments.get(0).type
					if (!keyType.string && !keyType.number) {
						decl.addError('''�fieldPrefix�key type has to be either string or number''')
					}
				}
			]
		]
		TypeScriptImporter.createTypeScriptImports(tsEntitiesConfig, allEntityClasses, #[], decl, context)
	}

	override doGenerateCode(TypeDeclaration decl, extension CodeGenerationContext context) {
		val tsEntitiesConfig = decl.readConfig
		val sb = new StringBuilder
		val allEntityClasses = tsEntitiesConfig.entities.allEntityClasses
		val entities = allEntityClasses.filter[type instanceof ClassDeclaration]
		val interfaces = allEntityClasses.filter[type instanceof InterfaceDeclaration]
		val imports = TypeScriptImporter.createTypeScriptImports(tsEntitiesConfig, allEntityClasses, #[], decl, null)
		sb.append(imports).append("\r\n\r\n")
		sb.append('''declare var Date : any;''').nl(2)
		val enums = allEntityClasses.filter[type instanceof EnumerationTypeDeclaration]

		enums.forEach [ en |
			val enumType = en.type as EnumerationTypeDeclaration
			sb.append('''export enum �enumType.simpleName� {''').nl
			sb.append(enumType.declaredValues.map['''	�simpleName�'''].join(",\r\n"))
			sb.append("\r\n}").nl
		]
		interfaces.forEach [ interfaze |
			val interfaceType = interfaze.type as InterfaceDeclaration
			sb.append('''export interface �interfaceType.getTypeString(true, true)�''')
			if (!interfaceType.extendedInterfaces.nullOrEmpty) {
				sb.append(''' extends �interfaceType.extendedInterfaces.map[type.simpleName]�''')
			}
			sb.append(" {").nl
			interfaceType.declaredMethods.forEach [ method |
				val sParams = method.parameters.map [ param |
					'''�param.simpleName�: �param.type.getTypeString(true, false)�'''
				].join(", ")
				val sTypes = method.typeParameters.getTypeParametersString(true)
				sb.append('''�method.simpleName��sTypes�(�sParams�): �method.returnType.getTypeString(true, false)�;''')
			]
			sb.append("}").nl
		]
		entities.forEach [ entity |
			val entityType = entity.type as ClassDeclaration
			sb.append("export ")
			if (entityType.abstract) {
				sb.append("abstract ")
			}
			sb.append('''class �entityType.getTypeString(true, true)� ''')
			if (entityType.hasParent) {
				sb.append('''extends �entityType.extendedClass.getTypeString(true, false)� ''')
			}
			sb.append("{").nl
			sb.appendFields(entityType, context) //
			.appendCreateInstanceMethod(entityType) //
			.appendCreateAndLoadFromPojo(entityType) //
			.appendCreatePojo(entityType) //
			.appendGettersAndSetters(entityType, context) //
			.appendEqualsHashCode(entityType)
			sb.append("}").nl(2)
		]
		val projectFolder = context.getProjectFolder(decl.compilationUnit.filePath)
		val tsFile = projectFolder.append(tsEntitiesConfig.projectRoot).append(tsEntitiesConfig.filePath)
		tsFile.contents = sb.toString
		decl.copyTypeScriptResources(context, tsEntitiesConfig)
	}

	def private static StringBuilder appendCreateAndLoadFromPojo(StringBuilder sb, ClassDeclaration clazz) {
		val discriminator = clazz.discriminatorProperty
		var subTypes = clazz.jsonSubTypes
		subTypes = subTypes.filter[clazz.isAssignableFrom(type)].toList

		sb.
			append('''	public static create�clazz.simpleName�FromPojo�clazz.getTypeString(false, true)�(pojo: any): �clazz.getTypeString(true, false)� {''').
			nl
		sb.append('''		if(!pojo) return null;''').nl
		sb.
			append('''		let instance = �clazz.simpleName�.createSpecific�clazz.getTypeString(true, false)�(pojo);''').
			nl
		sb.append('''		instance.loadFromPojo(pojo);''').nl
		sb.append('''		return instance;''').nl
		sb.append('''	}''').nl(2)

		sb.
			append('''	public static createSpecific�clazz.getTypeString(true, true)�(pojo: any): �clazz.getTypeString(true, false)� {''').
			nl
		if (!discriminator.nullOrEmpty && !subTypes.nullOrEmpty) {
			sb.append('''		let �discriminator� = pojo.�discriminator�;''').nl
			// TODO only necessary if type parameters differ
			val typeCast = if (!clazz.typeParameters.empty) ''' as any as �clazz.getTypeString(true, true)�''' else ""
			sb.append(subTypes.map [ subType |
				'''		if (�discriminator� == '�subType.type.simpleName�') {return �subType.type.simpleName�.create�subType.type.simpleName�FromPojo�subType.getTypeString(false, false)�(pojo)�typeCast�;}'''
			].join("\r\n		else ")).nl
			if (clazz.abstract) {
				sb.
					append('''		throw new Error("Can't create instance for �discriminator� " + �discriminator�);''').
					nl
			}
		} else if (clazz.abstract) {
			sb.
				append('''		throw new Error("Can't create instance. Class is abstract but �JsonTypeInfo.name� or �JsonSubTypes.name� not set.");''').
				nl
		}
		if (!clazz.abstract) {
			sb.
				append('''		return �clazz.simpleName�.create�clazz.simpleName�FromPojo�clazz.getTypeString(false, false)�(pojo);''').
				nl
		}
		sb.append('''	}''').nl(2)

		sb.append('''	public loadFromPojo(pojo: any): this {''').nl
		sb.append('''		if(!pojo) return this;''').nl

		if (clazz.hasParent) {
			sb.append("		super.loadFromPojo(pojo);").nl
		}
		val fields = clazz.declaredFields.jsonFields
		sb.append(fields.map [ field |
			val jsonFieldName = '''pojo['�field.jsonFieldName�']'''
			val fieldName = '''this.�field.simpleName�'''
			return getLoadFromPojoString(field, field.type, fieldName, jsonFieldName)
		].join("\r\n"))
		sb.append("\r\n		return this;").nl

		sb.append('''	}''').nl(2)
	}

	def static String getLoadFromPojoString(FieldDeclaration field, TypeReference tref, String fieldName,
		String propertyName) {
		return getLoadFromPojoString(field, tref, fieldName, propertyName, 1)
	}

	def static String getLoadFromPojoString(AnnotationTarget target, TypeReference tref, String fieldName,
		String propertyName, int level) {
		if (tref.date) {
			return '''�fieldName� = �propertyName� ? new Date(�propertyName�) : null;'''
		} else if (tref.dateVariant) {
			var errorComment = ""
			val pattern = target.jsonFormat
			if (pattern.nullOrEmpty) {
				errorComment = '''�fieldName�@�JsonFormat.name� doesn't exist'''
			}
			return '''�fieldName� = �propertyName� ? Date.parseString(�propertyName�, '�pattern�') : null;�errorComment�'''
		// this.�fieldName� = date ? (date as any).format('�pattern�') : null;�errorComment�
		} else if (tref.array || tref.type.collection) {
			return getLoadCollectionString(target, tref, '''�fieldName�''', propertyName, level)
		} else if (tref.map) {
			return getLoadMapString(target, tref, '''�fieldName�''', propertyName, level, false)
		} else if (tref.type instanceof EnumerationTypeDeclaration) {
			return '''�fieldName� = �propertyName� ? �tref.simpleName�[�propertyName� as string] : null;'''
		} else if (tref.type instanceof ClassDeclaration && !tref.name.startsWith("java.")) {
			return '''�fieldName� = �tref.type.simpleName�.create�tref.type.simpleName�FromPojo�tref.getTypeString(false, false)�(�propertyName�);'''
		} else {
			return '''�fieldName� = �propertyName�;'''
		}
	}

	def static String getWriteToPojoString(AnnotationTarget target, TypeReference tref, String fieldName,
		String propertyName) {
		if (tref.date) {
			return '''�propertyName� = �fieldName� ? �fieldName�.getTime() : null;'''
		} else if (tref.dateVariant) {
			var errorComment = ""
			val pattern = target.jsonFormat
			if (pattern.nullOrEmpty) {
				errorComment = '''�fieldName�@�JsonFormat.name� doesn't exist'''
			}
			return '''�propertyName� = �fieldName� ? Date.toString(�fieldName�, '�pattern�') : null;�errorComment�'''
		// this.�fieldName� = date ? (date as any).format('�pattern�') : null;�errorComment�
//		} else if (tref.array || tref.type.collection) {
//		} else if (tref.map) {
		} else if (tref.type instanceof EnumerationTypeDeclaration) {
			return '''�propertyName� ? �fieldName� ? �tref.simpleName�[�fieldName� as number] : null;'''
		} else if (tref.type instanceof ClassDeclaration && !tref.name.startsWith("java.")) {
			return '''�propertyName� = �fieldName� ? �fieldName�.toPojo() : null;'''
		} else {
			return '''�propertyName� = PojoBuilder.toPojo(�fieldName�);'''
		}
	}

	def static String getLoadCollectionString(AnnotationTarget target, TypeReference tref, String fieldName,
		String propertyName) {
		return getLoadCollectionString(target, tref, fieldName, propertyName, 1)
	}

	def static String getLoadCollectionString(AnnotationTarget target, TypeReference tref, String fieldName,
		String propertyName, int level) {
		var TypeReference collEntityType = null
		if (tref.array) {
			collEntityType = tref.arrayComponentType
		} else if (tref.collection) {
			collEntityType = tref.actualTypeArguments.head.upperBound
		} else {
			return '''// field �fieldName� neither array nor collection'''.nl
		}
		val collType = if(collEntityType.array) collEntityType.arrayComponentType.type else collEntityType.type
		val elemName = '''pojo�level�'''
		if ((collType instanceof ClassDeclaration || collType instanceof InterfaceDeclaration) &&
			collEntityType.mapablePojoType) {
			var elemInitString = '''�collType.simpleName�.create�collType.simpleName�FromPojo�collEntityType.getTypeString(false, false)�(�elemName�)'''
			if (collEntityType.array || collType.collection) {
				elemInitString = getLoadCollectionString(target, collEntityType, elemName, elemName, level + 1)
			} else if (collType.map) {
				elemInitString = getLoadMapString(target, collEntityType, elemName, elemName, level + 1, true)
			}
			return tabs(level) +
				'''�fieldName� = �propertyName� ? �propertyName�.map(�elemName� => �elemInitString�) : []'''
		} else if (collEntityType.date) {
			return tabs(level) +
				'''�fieldName� = �propertyName� ? �propertyName�.map(it => it ? new Date(it) : null) : []'''
		} else if (collEntityType.dateVariant) {
			var errorComment = ""
			val pattern = target.jsonFormat
			if (pattern.nullOrEmpty) {
				errorComment = '''�fieldName�@�JsonFormat.name� doesn't exist'''
			}
			return tabs(level) +
				'''�fieldName� = �propertyName� ? �propertyName�.map(it => it ? Date.parseString(it, '�pattern�') : null) : []�errorComment�'''
		}
		// TODO if (type instanceof InterfaceDeclaration) {
		return tabs(level) + '''�fieldName� = �propertyName� ? �propertyName�.map(it => it) : []'''
	}

//	def static String getLoadMapString(AnnotationTarget target, TypeReference tref, String fieldName, String propertyName) {
//		return getLoadMapString(target, tref, fieldName, propertyName, 1)
//	}
	def static String getLoadMapString(AnnotationTarget target, TypeReference tref, String fieldName,
		String propertyName, int level, boolean isInColection) {
		val tabs = tabs(level)
		if (!tref.map) {
			return tabs + '''// field �fieldName� not a map'''.nl
		}
		val keyType = tref.actualTypeArguments.get(0)
		if (!keyType.string && !keyType.number) {
			return tabs + '''// field �fieldName�: key must be string or number'''
		}
		val valueType = tref.actualTypeArguments.get(1)
		val keyName = '''key�level�'''
		val objName = '''obj�level�'''
		return (if(isInColection) tabs + "{".nl else "") + tabs + '''if(�propertyName�) {'''.nl + tabs +
			'''	let �objName� = �propertyName�;'''.nl + tabs + '''	�fieldName� = {};'''.nl + tabs +
			'''	for(let �keyName� in �objName�) {'''.nl + tabs +
			'''		�getLoadFromPojoString(target, valueType, '''�fieldName�[�keyName�]''', '''�objName�[�keyName�]''', level + 1)�'''.
				nl + tabs + '''	}'''.nl + tabs + '''}''' +
			(if(isInColection) "".nl + tabs + '''return �propertyName�;'''.nl + "}" else "")
	}

	def static boolean mapablePojoType(TypeReference typeRef) {
		if (typeRef.primitive || typeRef.number || typeRef.string || typeRef.boolean || typeRef.date ||
			typeRef.dateVariant) {
			return false
		}
		return true
	}

	def private static StringBuilder appendCreatePojo(StringBuilder sb, ClassDeclaration clazz) {
		val fields = clazz.declaredFields.jsonFields
		sb.append('''public toPojo(): any {''').nl
		if (clazz.hasParent) {
			sb.append('''var pojo: any = super.toPojo();''').nl
		} else {
			sb.append('''var pojo: any = {};''').nl
		}
		sb.append(fields.map [ field |
			val jsonFieldName = '''pojo['�field.jsonFieldName�']'''
			val fieldName = '''this.�field.simpleName�'''
			return getWriteToPojoString(field, field.type, fieldName, jsonFieldName)
		].join("\r\n"))
		sb.append('''	return pojo;''').nl
		sb.append('''}''').nl
		return sb
	}

	def private static StringBuilder appendEqualsHashCode(StringBuilder sb, ClassDeclaration clazz) {
		// TODO
		return sb
	}

	def private static boolean hasParent(ClassDeclaration clazz) {
		return clazz.extendedClass !== null && clazz.extendedClass.type.qualifiedName != Object.name
	}

	def static String getJsonFieldName(FieldDeclaration field) {
		val jsonFieldName = field.annotations.findFirst [
			annotationTypeDeclaration.qualifiedName == JsonProperty.name
		]?.getStringValue("value")
		if (jsonFieldName.nullOrEmpty) {
			return field.simpleName
		}
		return jsonFieldName
	}

	def static AnnotationReference getAnnotation(AnnotationTarget annTarget, String qualifiedName) {
		annTarget.getAnnotation(qualifiedName, true)
	}

	def static AnnotationReference getAnnotation(AnnotationTarget annTarget, String qualifiedName,
		boolean includeParents) {
		val ann = annTarget.annotations.findFirst[annotationTypeDeclaration.qualifiedName == qualifiedName]
		if (ann !== null) {
			return ann
		}
		if (annTarget instanceof ClassDeclaration) {
			if (annTarget.extendedClass !== null && annTarget.extendedClass.type instanceof AnnotationTarget) {
				return (annTarget.extendedClass.type as AnnotationTarget).getAnnotation(qualifiedName, includeParents)
			}
		}
		return null
	}

	def private static StringBuilder appendFields(StringBuilder sb, ClassDeclaration clazz,
		extension CodeGenerationContext context) {
		val fields = clazz.declaredFields.jsonFields
		fields.forEach [ field |
			val fieldName = field.simpleName
			val fieldTypeName = field.type.getTypeString(true, true)
			sb.append('''	public �fieldName�: �fieldTypeName�;''').nl
		]
		return sb.nl
	}

	def private static StringBuilder appendCreateInstanceMethod(StringBuilder sb, ClassDeclaration clazz) {
		if (clazz.abstract) {
			return sb
		}
		sb.
			append('''	public static create�clazz.simpleName��clazz.getTypeString(false, true)�(): �clazz.getTypeString(true, false)� {''').
			nl
		sb.append('''		return new �clazz.getTypeString(true, false)�();''').nl
		sb.append('''	}''').nl
		return sb.nl
	}

	def static StringBuilder appendGettersAndSetters(StringBuilder sb, ClassDeclaration clazz,
		GlobalTypeLookup globalTypeLookup) {
		val fields = clazz.declaredFields.jsonFields
		fields.forEach [ field |
			sb.appendGettersAndSetters(field, globalTypeLookup)
		]
		return sb
	}

	def static StringBuilder appendGettersAndSetters(StringBuilder sb, FieldDeclaration field,
		GlobalTypeLookup globalTypeLookup) {
		val fieldName = field.simpleName
		val fieldTypeName = field.type.getTypeString(true, true)
		sb.append('''	public �field.type.getterPrefix��field.simpleName.toFirstUpper�(): �fieldTypeName� {''').nl
		sb.append('''		return this.�fieldName�;''').nl
		sb.append('''	}''').nl
		sb.nl
		sb.append('''	public set�field.simpleName.toFirstUpper�(�fieldName�: �fieldTypeName�): this {''').nl
		sb.append('''		this.�fieldName� = �fieldName�;''').nl
		sb.append('''		return this;''').nl
		sb.append('''	}''').nl
		return sb.nl
	}

	def static TypeScriptEntitiesProcessorConfig readConfig(TypeDeclaration decl) {
		new TypeScriptEntitiesProcessorConfig(decl.annotations.findFirst [
			annotationTypeDeclaration.qualifiedName == TypeScriptEntities.name
		])
	}
}

@ToString @Accessors
abstract class TypeScriptProcessorConfig {
	val String projectRoot
	val String filePath
	val TypeScriptImportsProcessorConfig entityImports

	new(AnnotationReference ann) {
		projectRoot = ann.getStringValue("projectRoot")
		filePath = ann.getStringValue("filePath")
		entityImports = new TypeScriptImportsProcessorConfig(ann.getAnnotationValue("entityImports"))
	}
}

@ToString @Accessors
class TypeScriptEntitiesProcessorConfig extends TypeScriptProcessorConfig {
	val EntitiesProcessorConfig entities
	val boolean omitJsonIgnoreFields

	new(AnnotationReference ann) {
		super(ann)
		entities = new EntitiesProcessorConfig(ann.getAnnotationValue("entities"))
		omitJsonIgnoreFields = ann.getBooleanValue("omitJsonIgnoreFields")
	}
}

@ToString @Accessors
class TypeScriptImportsProcessorConfig {
	val TypeScriptFileImportProcessorConfig[] fileImports

	new(AnnotationReference ann) {
		val fileImports = ann.getAnnotationArrayValue("fileImports").map[new TypeScriptFileImportProcessorConfig(it)]
		val annotatedTypes = ann.getClassArrayValue("annotatedTypes")
		val types = annotatedTypes.map[type].filter[it instanceof TypeDeclaration].map[it as TypeDeclaration]

		val typeScriptEntitiesProcessorConfigsFileImports = types.map [
			val annotations = new ArrayList<AnnotationReference>(it.annotations.toList) // Xtend compiler problem
			annotations.findFirst[annotationTypeDeclaration.qualifiedName == TypeScriptEntities.name]
		].filterNull.map [
			new TypeScriptEntitiesProcessorConfig(it)
		].map [
			new TypeScriptFileImportProcessorConfig(it.filePath, it.entities)
		]
		val typeScriptImportsAnnotatedTypesFileImports = types.map [
			val annotations = new ArrayList<AnnotationReference>(it.annotations.toList) // Xtend compiler problem
			annotations.findFirst[annotationTypeDeclaration.qualifiedName == TypeScriptImports.name]
		].filterNull.map [
			new TypeScriptImportsProcessorConfig(it)
		].map [
			it.fileImports.toList
		].flatten
		val typeScriptFileImportAnnotatedTypesFileImports = types.map [
			val annotations = new ArrayList<AnnotationReference>(it.annotations.toList) // Xtend compiler problem
			annotations.findFirst[annotationTypeDeclaration.qualifiedName == TypeScriptFileImport.name]
		].filterNull.map [
			new TypeScriptFileImportProcessorConfig(it)
		]
		this.fileImports = #[
			fileImports,
			typeScriptEntitiesProcessorConfigsFileImports,
			typeScriptImportsAnnotatedTypesFileImports,
			typeScriptFileImportAnnotatedTypesFileImports
		].flatten
	}
}

@ToString @Accessors
class TypeScriptFileImportProcessorConfig {
	val String filePath
	val EntitiesProcessorConfig entities

	new(AnnotationReference ann) {
		filePath = ann.getStringValue("filePath")
		entities = new EntitiesProcessorConfig(ann.getAnnotationValue("entities"))
	}

	new(String filePath, EntitiesProcessorConfig entities) {
		this.filePath = filePath
		this.entities = entities
	}
}
