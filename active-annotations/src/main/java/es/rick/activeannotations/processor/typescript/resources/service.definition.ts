export class ServiceDefinitionWrapper {
  public wrappedFields: Array<string>;
}

export class ServiceDefinitionJavaWrapper extends ServiceDefinitionWrapper {
  public javaWrapperName: string;
}

export class ServiceDefinitionTypeScriptWrapper extends ServiceDefinitionWrapper {
  public tsWrapperName: string;
  public tsFileName: string;
}

export class ServiceDefinition {
  public tsFileName: string;
  public javaInterfaceName: string;
  public javaClientName: string;
  public javaWrappers: Array<ServiceDefinitionJavaWrapper>;
  public typeScriptWrappers: Array<ServiceDefinitionTypeScriptWrapper>;
}

export class TypeDefinition {
  public tsType: string;
  public javaType: string;
}

export class ParamDefinition {
  public simpleName: string;
  public type: TypeDefinition;
}

export class MethodDefinition {
  public simpleName: string;
  public returnType: TypeDefinition;
  public params: Array<ParamDefinition>;
}

class DefinitionBuilder {
  public static getSimpleName(qualifiedName: string): string {
    if (!qualifiedName) {
      return qualifiedName;
    }
    let ind = qualifiedName.lastIndexOf(".");
    if (ind < 0) {
      return qualifiedName;
    }
    return qualifiedName.substring(ind + 1);
  }

  public static toFirstLower(s: string): string {
    if (!s || s.length < 1) {
      return s;
    }
    return s.substring(0, 1).toLowerCase() + s.substring(1);
  }

  public static removeTsExtensions(s: string): string {
    if (s && s.lastIndexOf(".ts") == s.length - 3) {
      return s.substring(0, s.length - 3);
    }
    return s;
  }
}

export class JavaDefinitionBuilder {
  public static build(serviceDefinition: ServiceDefinition, methodDefinition: MethodDefinition, endpoint: string = "http://..."): string {
    if (!serviceDefinition.javaInterfaceName || serviceDefinition.javaInterfaceName.length == 0 || !serviceDefinition.javaClientName || serviceDefinition.javaClientName.length == 0) {
      return null;
    }
    let simpleName = DefinitionBuilder.getSimpleName(serviceDefinition.javaInterfaceName);
    let client = DefinitionBuilder.toFirstLower(simpleName);
    let returnVar = "";
    if (methodDefinition.returnType && methodDefinition.returnType.javaType && methodDefinition.returnType.javaType.toLowerCase().indexOf("void") < 0) {
      returnVar = `${methodDefinition.returnType.javaType} response = `;
      if (returnVar.length > 40) {
        returnVar += "\r\n\t";
      }
    }
    let paramDeclarations = methodDefinition.params.map(param => `${param.type.javaType} ${param.simpleName} = ...;`).join("\r\n");
    let paramList = methodDefinition.params.map(param => param.simpleName).join(", ");
    let s = `
import ${serviceDefinition.javaInterfaceName};
import ${serviceDefinition.javaClientName};

${simpleName} ${client} = new ${DefinitionBuilder.getSimpleName(serviceDefinition.javaClientName)}("${endpoint}");
${paramDeclarations}
${returnVar}${client}.${methodDefinition.simpleName}(${paramList});
`;
    return s.trim();
  }
}

export class TypeScriptDefinitionBuilder {
  public static build(serviceDefinition: ServiceDefinition, methodDefinition: MethodDefinition, endpoint: string = "http://..."): string {
    if (!serviceDefinition.javaInterfaceName || serviceDefinition.javaInterfaceName.length == 0) {
      return null;
    }
    let simpleName = DefinitionBuilder.getSimpleName(serviceDefinition.javaInterfaceName);
    let client = DefinitionBuilder.toFirstLower(simpleName);
    let returnVar = "";
    if (methodDefinition.returnType && methodDefinition.returnType.tsType && methodDefinition.returnType.tsType.toLowerCase().indexOf("void") < 0) {
      returnVar = `let response : ${methodDefinition.returnType.tsType} = `;
    }
    let paramDeclarations = methodDefinition.params.map(param => `let ${param.simpleName} : ${param.type.tsType} = ...;`).join("\r\n");
    let paramList = methodDefinition.params.map(param => param.simpleName).join(", ");
    let s = `
import {${simpleName}} from "${DefinitionBuilder.removeTsExtensions(serviceDefinition.tsFileName)}";

let ${client} = new ${simpleName}();
${client}.contextPath = "${endpoint}";
${paramDeclarations}
${returnVar}${client}.${methodDefinition.simpleName}(${paramList});
`;

    return s.trim();
  }
}
