package es.rick.activeannotations.processor

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.temporal.Temporal
import java.util.Collection
import java.util.Date
import java.util.Map
import java.util.Set
import org.eclipse.xtend.lib.macro.declaration.ClassDeclaration
import org.eclipse.xtend.lib.macro.declaration.InterfaceDeclaration
import org.eclipse.xtend.lib.macro.declaration.MethodDeclaration
import org.eclipse.xtend.lib.macro.declaration.MutableTypeDeclaration
import org.eclipse.xtend.lib.macro.declaration.Type
import org.eclipse.xtend.lib.macro.declaration.TypeDeclaration
import org.eclipse.xtend.lib.macro.declaration.TypeReference
import org.eclipse.xtext.xbase.lib.Functions.Function1
import org.eclipse.xtend.lib.macro.declaration.Visibility

abstract class ProcessorBase {
	def static Iterable<? extends MethodDeclaration> getAllDeclaredMethods(TypeDeclaration clazz) {
		if (clazz === null || clazz.qualifiedName == Object.name) {
			return #[]
		}
		if (clazz instanceof ClassDeclaration) {
			val Function1<MethodDeclaration, Boolean> filterOverridenObjectMethods = [ method |
				if ((method.simpleName == "hashCode" || method.simpleName == "toString" ||
					method.simpleName == "finalize") && method.parameters.empty) {
					return false
				}
				if (method.simpleName == "equals" && method.parameters.size === 1) {
					return false
				}
				return true
			]
			val Function1<MethodDeclaration, Boolean> filterMemberMethods = [!static && visibility === Visibility.PUBLIC]
			if (clazz.extendedClass !== null) {
				return clazz.declaredMethods.filter(filterMemberMethods).filter(filterOverridenObjectMethods) +
					(clazz.extendedClass.type as TypeDeclaration).allDeclaredMethods
			} else {
				return clazz.declaredMethods.filter(filterMemberMethods).filter(filterOverridenObjectMethods)
			}
		} else if (clazz instanceof InterfaceDeclaration) {
			return clazz.declaredMethods +
				clazz.extendedInterfaces.map[type as TypeDeclaration].map[allDeclaredMethods].flatten
		}
		return #[]
	}
	
	@Pure def static String addTrailingSlash(String path) {
		if (path.nullOrEmpty) {
			return "/" // TODO hm, was tun?
		}
		if (!path.endsWith("/") && !path.endsWith("\\")) {
			return path + "/"
		}
		return path
	}

	@Pure def static boolean isIterable(TypeReference t) {
		return isIterable(t.type)
	}

	@Pure def static boolean isIterable(Type t) {
		return Iterable.isAssignableFrom(t)
	}

	@Pure def static boolean isCollection(TypeReference t) {
		return isCollection(t.type)
	}

	@Pure def static boolean isCollection(Type t) {
		return Collection.isAssignableFrom(t)
	}

	@Pure def static boolean isMap(TypeReference t) {
		return isMap(t.type)
	}

	@Pure def static boolean isMap(Type t) {
		return Map.isAssignableFrom(t)
	}

	@Pure def static boolean isNumber(TypeReference t) {
		return isNumber(t.wrapperIfPrimitive.type)
	}

	@Pure def static boolean isNumber(Type t) {
		return Number.isAssignableFrom(t)
	}

	@Pure def static boolean isString(TypeReference t) {
		return isString(t.wrapperIfPrimitive.type)
	}

	@Pure def static boolean isString(Type t) {
		return String.isAssignableFrom(t)
	}

	@Pure def static boolean isBoolean(TypeReference t) {
		return isBoolean(t.wrapperIfPrimitive.type)
	}

	@Pure def static boolean isBoolean(Type t) {
		return Boolean.isAssignableFrom(t)
	}

	@Pure def static boolean isDateVariant(TypeReference t) {
		return isDateVariant(t.wrapperIfPrimitive.type)
	}

	@Pure def static boolean isDateVariant(Type t) {
		return Temporal.isAssignableFrom(t)
	}

	@Pure def static boolean isDate(TypeReference t) {
		return isDate(t.type)
	}

	@Pure def static boolean isDate(Type t) {
		return Date.isAssignableFrom(t)
	}

	@Pure def static boolean isLocalDate(TypeReference t) {
		return isLocalDate(t.type)
	}

	@Pure def static boolean isLocalDate(Type t) {
		return LocalDate.isAssignableFrom(t)
	}

	@Pure def static boolean isLocalTime(TypeReference t) {
		return isLocalTime(t.type)
	}

	@Pure def static boolean isLocalTime(Type t) {
		return LocalTime.isAssignableFrom(t)
	}

	@Pure def static boolean isLocalDateTime(TypeReference t) {
		return isLocalDateTime(t.type)
	}

	@Pure def static boolean isLocalDateTime(Type t) {
		return LocalDateTime.isAssignableFrom(t)
	}

	@Pure def static boolean isAssignableFrom(TypeReference t, Class<?> clazz) {
		return isAssignableFrom(clazz, t.type)
	}

	@Pure def static boolean isAssignableFrom(Class<?> clazz, Type t) {
		val typeNames = t.inheritedTypeNames
		return typeNames.contains(clazz.name)
	}

	@Pure def static Set<String> getInheritedTypeNames(Type t) {
		if (t instanceof InterfaceDeclaration) {
			return #[
				#[t.qualifiedName],
				t.extendedInterfaces.map[#{name} + type.inheritedTypeNames].flatten
			].flatten.removeTypeInformation.toSet
		} else if (t instanceof ClassDeclaration) {
			return #[
				#[t.qualifiedName],
				t.extendedClass?.type.inheritedTypeNames,
				t.implementedInterfaces.map[#{name} + type.inheritedTypeNames].flatten
			].flatten.removeTypeInformation.toSet
		} else {
			return #{t?.qualifiedName}.filterNull.removeTypeInformation.toSet
		}
	}

	@Pure def static Set<String> getInheritedTypeNames(Class<?> c) {
		#[
			#[c.name],
			c.interfaces.map[#{name} + inheritedTypeNames].flatten,
			#[c.superclass].filterNull.map[inheritedTypeNames].flatten
		].flatten.removeTypeInformation.toSet
	}

	@Pure def static Iterable<String> removeTypeInformation(Iterable<String> iter) {
		iter.map [
			val ind = indexOf("<")
			if (ind < 0) {
				return it
			}
			return substring(0, ind)
		]
	}

	@Pure def static String getGetterPrefix(TypeReference t) {
		return t.type.getterPrefix
	}

	@Pure def static String getGetterPrefix(Type t) {
		return if(t?.qualifiedName == boolean.name) "is" else "get"
	}

	def static StringBuilder nl(StringBuilder sb) {
		return nl(sb, 1)
	}

	def static StringBuilder nl(StringBuilder sb, int count) {
		for (var i = 0; i < count; i++) {
			sb.append("\r\n")
		}
		return sb
	}

	def static StringBuilder tab(StringBuilder sb) {
		return tab(sb, 1)
	}

	def static StringBuilder tab(StringBuilder sb, int count) {
		for (var i = 0; i < count; i++) {
			sb.append("\t")
		}
		return sb
	}

	def static String tabs(int count) {
		var s = ""
		for (var i = 0; i < count; i++) {
			s += "\t"
		}
		return s
	}

	def static String nl(CharSequence s) {
		return (s ?: "") + "\r\n"
	}

	@Pure def static String getSimpleName(String className) {
		if (className.nullOrEmpty) {
			return className
		}
		val lastInd = className.lastIndexOf(".")
		if (lastInd < 0) {
			return className
		}
		val simpleName = className.substring(lastInd + 1)
		return simpleName
	}

	@Pure def static String getPackageName(String className) {
		if (className.nullOrEmpty) {
			return "root"
		}
		val lastInd = className.lastIndexOf(".")
		if (lastInd < 0) {
			return className
		}
		val packageName = className.substring(0, lastInd)
		return packageName
	}

	def static void removeAnnotation(MutableTypeDeclaration decl, String qualifiedName) {
		val ann = decl.annotations.findFirst[annotationTypeDeclaration.qualifiedName == qualifiedName]
		if (ann !== null) {
			decl.removeAnnotation(ann)
		}
	}
}
