package es.rick.activeannotations.processor.delegate

import es.rick.activeannotations.GenerationTarget
import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtend.lib.annotations.ToString

import static extension com.google.common.base.Strings.*
import es.rick.activeannotations.processor.ProcessorBase

abstract class DelegateProcessor extends ProcessorBase {
	def static String generateTargetName(String sourceClassName, GenerationTarget generationTarget,
		NamePatternProcessorConfig namePattern) {
		val simpleName = sourceClassName.simpleName
		val classNameBase = if (generationTarget === GenerationTarget.CLASS)
				sourceClassName.packageName
			else
				sourceClassName
		val targetClassName = simpleName.generateTargetName(classNameBase, namePattern)
		return targetClassName
	}

	def static String generateTargetName(String simpleName, String classNameBase,
		NamePatternProcessorConfig namePattern) {
		val targetName = simpleName.generateTargetName(namePattern)
		return '''«classNameBase».«targetName»'''
	}

	def static String generateTargetName(String simpleName, NamePatternProcessorConfig namePattern) {
		if (namePattern === null) {
			return simpleName
		}
		if (!namePattern.replace.nullOrEmpty) {
			if (!namePattern.regex.nullOrEmpty) {
				return simpleName.replace(namePattern.regex, namePattern.replace)
			}
			return namePattern.replace
		}
		if (!namePattern.prefix.nullOrEmpty || !namePattern.suffix.nullOrEmpty) {
			return '''«namePattern.prefix.nullToEmpty»«simpleName»«namePattern.suffix.nullToEmpty»'''
		}
		return simpleName
	}

	def static GenerationTarget inherit(GenerationTarget child, GenerationTarget parent) {
		if (parent === null) {
			return child
		}
		if (child == GenerationTarget.INHERIT) {
			return parent
		}
		return child
	}

	@ToString @Accessors
	static abstract class DelegateConfig {
		String qualifiedNameSource
		String qualifiedNameTarget
	}
}
