package es.rick.activeannotations.processor.entity

import org.eclipse.xtend.lib.macro.declaration.MutableClassDeclaration
import org.eclipse.xtend.lib.macro.TransformationContext
import static extension org.eclipse.xtext.xbase.lib.BooleanExtensions.*
import org.eclipse.xtend.lib.macro.declaration.ClassDeclaration
import es.rick.activeannotations.IgnoreEqualsHashCode
import org.eclipse.xtend.lib.macro.declaration.FieldDeclaration
import org.eclipse.xtend2.lib.StringConcatenationClient
import java.util.Arrays
import org.eclipse.xtend.lib.macro.declaration.TypeReference

class EntityEqualsHashCodeProcessor extends EntityBaseProcessor {
	val static EQUALS = "equals"
	val static HASHCODE = "hashCode"
	/**
	 * based on https://www.artima.com/lejava/articles/equality.html
	 */
	val static CANEQUAL = "canEqual"

	override transformEntity(extension MutableClassDeclaration clazz, extension TransformationContext context,
		extension EntityProcessorConfig entityConfig) {
		val extension util = new Util(context)
		val equalsMethodAlreadyExists = clazz.hasEquals
		val hashCodeMethodAlreadyExists = clazz.hasHashCode
		if (equalsMethodAlreadyExists && hashCodeMethodAlreadyExists) {
			return // both methods exist - nothing to do here
		}
		if (xor(equalsMethodAlreadyExists, hashCodeMethodAlreadyExists)) {
			clazz.addError('''Only �EQUALS� or �HASHCODE� are defined but not both''')
			return
		}
		val fields = declaredFields.filter [
			!static && !transient && !annotations.exists [
				annotationTypeDeclaration.qualifiedName == IgnoreEqualsHashCode
			]
		]
		addEquals(clazz, fields, clazz.hasSuperEquals)
		addHashCode(clazz, fields, clazz.hasSuperHashCode)
	}

	static class Util {
		extension TransformationContext context

		new(TransformationContext context) {
			this.context = context
		}

		def hasHashCode(ClassDeclaration it) {
			findDeclaredMethod(HASHCODE) !== null
		}

		def hasEquals(ClassDeclaration it) {
			declaredMethods.exists [
				simpleName == EQUALS && parameters.size == 1 && parameters.head.type == object
			]
		}

		def hasCanEqual(ClassDeclaration it) {
			declaredMethods.exists [
				simpleName == CANEQUAL && parameters.size == 1 && parameters.head.type == object
			]
		}

		def boolean hasSuperEquals(ClassDeclaration cls) {
			val superClass = (cls.extendedClass.type as ClassDeclaration)
			if (superClass.newTypeReference.equals(object)) {
				false
			} else if (superClass.hasEquals) {
				true
			} else {
				superClass.hasSuperEquals
			}
		}

		def boolean hasSuperHashCode(ClassDeclaration cls) {
			val superClass = (cls.extendedClass.type as ClassDeclaration)
			if (superClass.newTypeReference.equals(object)) {
				false
			} else if (superClass.hasHashCode) {
				true
			} else {
				superClass.hasSuperHashCode
			}
		}

		def boolean hasSuperCanEqual(ClassDeclaration cls) {
			val superClass = (cls.extendedClass.type as ClassDeclaration)
			if (superClass.newTypeReference.equals(object)) {
				false
			} else if (superClass.hasCanEqual) {
				true
			} else {
				superClass.hasSuperHashCode
			}
		}

		def void addEquals(MutableClassDeclaration cls, Iterable<? extends FieldDeclaration> includedFields,
			boolean includeSuper) {
			cls.addMethod(EQUALS) [
				primarySourceElement = cls.primarySourceElement
				returnType = primitiveBoolean
				addAnnotation(newAnnotationReference(Override))
				addAnnotation(newAnnotationReference(Pure))
				addParameter("obj", object)
				body = '''
					if (this == obj)
					  return true;
					if (obj == null)
					  return false;
					�IF includeSuper�
						if (!super.equals(obj))
						  return false;
					�ENDIF�
					if(!(obj instanceof �cls.newWildCardSelfTypeReference�)) {
						return false;
					}
					�cls.newWildCardSelfTypeReference� other = (�cls.newWildCardSelfTypeReference�) obj;
					if(!other.canEqual(this)) {
						return false;
					}
					�FOR field : includedFields�
						�field.contributeToEquals�
					�ENDFOR�
					return true;
				'''
			]
			cls.addMethod(CANEQUAL, [
				returnType = primitiveBoolean
				if (cls.hasSuperCanEqual) {
					addAnnotation(newAnnotationReference(Override))
				}
				addParameter("other", object)
				body = '''return other instanceof �cls.newWildCardSelfTypeReference�;'''
			])
		}

		private def newWildCardSelfTypeReference(ClassDeclaration cls) {
			cls.newTypeReference(cls.typeParameters.map[object.newWildcardTypeReference])
		}

		def StringConcatenationClient contributeToEquals(FieldDeclaration it) {
			switch (type.orObject.name) {
				case Double.TYPE.name: '''
					if (�Double�.doubleToLongBits(other.�simpleName�) != �Double�.doubleToLongBits(this.�simpleName�))
					  return false; 
				'''
				case Float.TYPE.name: '''
					if (�Float�.floatToIntBits(other.�simpleName�) != �Float�.floatToIntBits(this.�simpleName�))
					  return false; 
				'''
				case Boolean.TYPE.name,
				case Integer.TYPE.name,
				case Character.TYPE.name,
				case Byte.TYPE.name,
				case Short.TYPE.name,
				case Long.TYPE.name: '''
					if (other.�simpleName� != this.�simpleName�)
					  return false;
				'''
				default: '''
					if (this.�simpleName� == null) {
					  if (other.�simpleName� != null)
					    return false;
					} else if (!�deepEquals�)
					  return false;
				'''
			}
		}

		def StringConcatenationClient deepEquals(FieldDeclaration it) {
			if (type.isArray) {
				if (type.arrayComponentType.isPrimitive) {
					'''�Arrays�.equals(this.�simpleName�, other.�simpleName�)'''
				} else {
					'''�Arrays�.deepEquals(this.�simpleName�, other.�simpleName�)'''
				}
			} else {
				'''this.�simpleName�.equals(other.�simpleName�)'''
			}
		}

		def void addHashCode(MutableClassDeclaration cls, Iterable<? extends FieldDeclaration> includedFields,
			boolean includeSuper) {
			cls.addMethod(HASHCODE) [
				primarySourceElement = cls.primarySourceElement
				returnType = primitiveInt
				addAnnotation(newAnnotationReference(Override))
				addAnnotation(newAnnotationReference(Pure))
				body = '''
					final int prime = 31;
					int result = �IF includeSuper�super.hashCode()�ELSE�1�ENDIF�;
					�FOR field : includedFields�
						�field.contributeToHashCode�
					�ENDFOR�
					return result;
				'''
			]
		}

		def StringConcatenationClient contributeToHashCode(FieldDeclaration it) {
			switch (type.orObject.name) {
				case Double.TYPE.
					name: '''result = prime * result + (int) (�Double�.doubleToLongBits(this.�simpleName�) ^ (�Double�.doubleToLongBits(this.�simpleName�) >>> 32));'''
				case Float.TYPE.name: '''result = prime * result + �Float�.floatToIntBits(this.�simpleName�);'''
				case Boolean.TYPE.name: '''result = prime * result + (this.�simpleName� ? 1231 : 1237);'''
				case Integer.TYPE.name,
				case Character.TYPE.name,
				case Byte.TYPE.name,
				case Short.TYPE.name: '''result = prime * result + this.�simpleName�;'''
				case Long.TYPE.
					name: '''result = prime * result + (int) (this.�simpleName� ^ (this.�simpleName� >>> 32));'''
				default: '''result = prime * result + ((this.�simpleName�== null) ? 0 : �deepHashCode�);'''
			}
		}

		def StringConcatenationClient deepHashCode(FieldDeclaration it) {
			val type = type.orObject
			if (type.isArray) {
				if (type.arrayComponentType.isPrimitive) {
					'''�Arrays�.hashCode(this.�simpleName�)'''
				} else {
					'''�Arrays�.deepHashCode(this.�simpleName�)'''
				}
			} else {
				'''this.�simpleName�.hashCode()'''
			}
		}

		private def orObject(TypeReference ref) {
			ref ?: object
		}
	}
}
