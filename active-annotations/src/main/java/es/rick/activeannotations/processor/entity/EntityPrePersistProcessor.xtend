package es.rick.activeannotations.processor.entity

import com.google.common.annotations.GwtIncompatible
import es.rick.activeannotations.GenerateUuidOnPrePersist
import es.rick.activeannotations.SetNowOnPrePersist
import es.rick.activeannotations.SetNowOnPreUpdate
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.util.Date
import javax.persistence.PrePersist
import javax.persistence.PreUpdate
import org.eclipse.xtend.lib.macro.TransformationContext
import org.eclipse.xtend.lib.macro.declaration.FieldDeclaration
import org.eclipse.xtend.lib.macro.declaration.MutableAnnotationTarget
import org.eclipse.xtend.lib.macro.declaration.MutableClassDeclaration
import org.eclipse.xtend.lib.macro.declaration.TypeReference

/**
 * TODO check if only valid fields are marked
 */
class EntityPrePersistProcessor extends EntityBaseProcessor {
	override transformEntity(extension MutableClassDeclaration clazz, extension TransformationContext context,
		extension EntityProcessorConfig entityConfig) {
		createOnPrePersist(clazz, context, entityConfig)
		createOnPreUpdate(clazz, context, entityConfig)
	}

	def private static void createOnPrePersist(extension MutableClassDeclaration clazz,
		extension TransformationContext context, extension EntityProcessorConfig entityConfig) {
		val fieldsGenerateUuid = clazz.declaredFields.filter [
			annotations.exists[annotationTypeDeclaration.qualifiedName == GenerateUuidOnPrePersist.name]
		]
		val fieldsSetNowOnPrePersist = clazz.declaredFields.filter [
			annotations.exists[annotationTypeDeclaration.qualifiedName == SetNowOnPrePersist.name]
		]
		if (fieldsGenerateUuid.nullOrEmpty && fieldsSetNowOnPrePersist.nullOrEmpty) {
			return
		}
		fieldsGenerateUuid.forEach[removeAnnotation(GenerateUuidOnPrePersist.newAnnotationReference)]
		fieldsSetNowOnPrePersist.forEach[removeAnnotation(SetNowOnPrePersist.newAnnotationReference)]
		/*
		 * There's only one @PrePersist annotated method allowed. 
		 * If there's already one, encapsulate it
		 */
		val prePersistMethods = clazz.declaredMethods.filter [
			annotations.exists[annotationTypeDeclaration.qualifiedName == PrePersist.name]
		].toList
		prePersistMethods.forEach [
			removeAnnotation(annotations.findFirst[annotationTypeDeclaration.qualifiedName == PrePersist.name])
		]
		clazz.addMethod(
			'''__prePersist''', [
			setGwtIncompatible(context)
			body = '''
				�FOR field : fieldsGenerateUuid�
					if(�field.simpleName� == null || �field.simpleName�.trim().isEmpty()) {
						�field.simpleName� = java.util.UUID.randomUUID().toString();
					}
				�ENDFOR�
				�FOR field : fieldsSetNowOnPrePersist�
					�IF field.isOnlySetIfNull(PreUpdate.name)�
					if(�field.simpleName� == null) {
						�field.simpleName� = �field.type.createNowExpression�;
					}
					�ELSE�
						�field.simpleName� = �field.type.createNowExpression�;
					�ENDIF�
				�ENDFOR�
				�FOR prePersistMethod : prePersistMethods�
					�prePersistMethod.simpleName�();
				�ENDFOR�
			'''
			addAnnotation(PrePersist.newAnnotationReference)
		])
	}

	def private static void createOnPreUpdate(extension MutableClassDeclaration clazz,
		extension TransformationContext context, extension EntityProcessorConfig entityConfig) {
		val fieldsSetNowOnPreUpdate = clazz.declaredFields.filter [
			annotations.exists[annotationTypeDeclaration.qualifiedName == SetNowOnPreUpdate.name]
		]
		if (fieldsSetNowOnPreUpdate.nullOrEmpty) {
			return
		}
		fieldsSetNowOnPreUpdate.forEach[removeAnnotation(SetNowOnPreUpdate.newAnnotationReference)]
		val preUpdateMethods = clazz.declaredMethods.filter [
			annotations.exists[annotationTypeDeclaration.qualifiedName == PreUpdate.name]
		].toList
		preUpdateMethods.forEach [
			removeAnnotation(annotations.findFirst[annotationTypeDeclaration.qualifiedName == PreUpdate.name])
		]
		clazz.addMethod(
			'''__preUpdate''', [
			setGwtIncompatible(context)
			body = '''
				�FOR field : fieldsSetNowOnPreUpdate�
					�IF field.isOnlySetIfNull(PreUpdate.name)�
					if(�field.simpleName� == null) {
						�field.simpleName� = �field.type.createNowExpression�;
					}
					�ELSE�
						�field.simpleName� = �field.type.createNowExpression�;
					�ENDIF�
				�ENDFOR�
				�FOR preUpdateMethod : preUpdateMethods�
					�preUpdateMethod.simpleName�();
				�ENDFOR�
			'''
			addAnnotation(PreUpdate.newAnnotationReference)
		])
	}

	def private static String createNowExpression(TypeReference tref) {
		switch (tref.name) {
			case LocalDate.name: return '''new �LocalDate.name�.now()'''
			case LocalTime.name: return '''new �LocalTime.name�.now()'''
			case LocalDateTime.name: return '''new �LocalDateTime.name�.now()'''
			case Date.name: return '''new �Date.name�()'''
		}
	}

	def private static boolean isOnlySetIfNull(FieldDeclaration field, String qualifiedNameAnnotation) {
		val ann = field.annotations.findFirst[annotationTypeDeclaration.qualifiedName == qualifiedNameAnnotation]
		if (ann === null) {
			return true
		}
		return ann.getBooleanValue("onlySetIfNull")
	}

	/**
	 * TODO put this somewhere else
	 */
	def private static void setGwtIncompatible(MutableAnnotationTarget target,
		extension TransformationContext context) {
		if (target.annotations.exists[annotationTypeDeclaration.qualifiedName == GwtIncompatible.name]) {
			return // Annotation does already exist
		}
		if (GwtIncompatible.newAnnotationReference !== null) {
			val annGwtIncompatible = GwtIncompatible.newAnnotationReference [
				setStringValue("value", "java.util.UUID not implemented in GWT")
			]
			target.addAnnotation(annGwtIncompatible)
		}
	}
}
