package es.rick.activeannotations.processor.entity

import com.google.common.annotations.Beta
import es.rick.activeannotations.AccessorType
import java.util.List
import org.eclipse.xtend.lib.macro.TransformationContext
import org.eclipse.xtend.lib.macro.declaration.FieldDeclaration
import org.eclipse.xtend.lib.macro.declaration.MutableClassDeclaration
import org.eclipse.xtend.lib.macro.declaration.MutableFieldDeclaration
import org.eclipse.xtend.lib.macro.declaration.TypeReference
import org.eclipse.xtend.lib.macro.declaration.Visibility

/**
 * based on org.eclipse.xtend.lib.annotations.AccessorsProcessor
 */
class EntityAccessorsProcessor extends EntityBaseProcessor {
	override transformEntity(extension MutableClassDeclaration clazz, extension TransformationContext context,
		extension EntityProcessorConfig entityConfig) {
			declaredFields.filter[!static && !transient /*&& thePrimaryGeneratedJavaElement*/].forEach[transform(it, context, entityConfig)]
	}
	
	def void transform(MutableFieldDeclaration it, extension TransformationContext context, extension EntityProcessorConfig entityConfig) {
		extension val util = new EntityAccessorsProcessor.Util(context)
		if (shouldAddGetter(entityConfig)) {
			addGetter(accessors.getter.toVisibility)
		}
		if (shouldAddSetter(entityConfig)) {
			addSetter(accessors.setter.toVisibility)
		}
	}

	@Beta
	static class Util {
		extension TransformationContext context

		new(TransformationContext context) {
			this.context = context
		}

		def toVisibility(AccessorType type) {
			switch type {
				case PUBLIC: Visibility.PUBLIC
				case PROTECTED: Visibility.PROTECTED
				case PACKAGE: Visibility.DEFAULT
				case PRIVATE: Visibility.PRIVATE
				default: throw new IllegalArgumentException('''Cannot convert �type�''')
			}
		}

		def hasGetter(FieldDeclaration it) {
			possibleGetterNames.exists[name| declaringType.findDeclaredMethod(name) !== null]
		}

		def shouldAddGetter(FieldDeclaration it, extension EntityProcessorConfig entityConfig) {
			!hasGetter && accessors.getter !== AccessorType.NONE
		}

		def validateGetter(MutableFieldDeclaration field) {
		}

		def getGetterName(FieldDeclaration it) {
			possibleGetterNames.head
		}
		
		def List<String> getPossibleGetterNames(FieldDeclaration it) {
			val names = newArrayList
			// common case: a boolean field already starts with 'is'. Allow field name as getter method name
			if (type.orObject.isBooleanType && simpleName.startsWith('is') && simpleName.length>2 && Character.isUpperCase(simpleName.charAt(2))) {
				names += simpleName
			}
			names.addAll((if(type.orObject.isBooleanType) #["is", "get"] else #["get"]).map[prefix|prefix + simpleName.toFirstUpper])
			return names
		}

		def isBooleanType(TypeReference it) {
			!inferred && it == primitiveBoolean
		}

		def void addGetter(MutableFieldDeclaration field, Visibility visibility) {
			field.validateGetter
			field.markAsRead
			field.declaringType.addMethod(field.getterName) [
				primarySourceElement = field.primarySourceElement
				addAnnotation(newAnnotationReference(Pure))
				returnType = field.type.orObject
				body = '''return �field.fieldOwner�.�field.simpleName�;'''
				static = field.static
				it.visibility = visibility
			]
		}

		private def fieldOwner(MutableFieldDeclaration it) {
			if(static) declaringType.newTypeReference else "this"
		}

		def hasSetter(FieldDeclaration it) {
			declaringType.findDeclaredMethod(setterName, type.orObject) !== null
		}

		def getSetterName(FieldDeclaration it) {
			"set" + simpleName.toFirstUpper
		}

		def shouldAddSetter(FieldDeclaration it, extension EntityProcessorConfig entityConfig) {
			!final && !hasSetter && accessors.setter !== AccessorType.NONE
		}

		def validateSetter(MutableFieldDeclaration field) {
			if (field.final) {
				field.addError("Cannot set a final field")
			}
			if (field.type === null || field.type.inferred) {
				field.addError("Type cannot be inferred.")
				return
			}
		}

		def void addSetter(MutableFieldDeclaration field, Visibility visibility) {
			field.validateSetter
			field.declaringType.addMethod(field.setterName) [
				primarySourceElement = field.primarySourceElement
				returnType = primitiveVoid
				val param = addParameter(field.simpleName, field.type.orObject)
				body = '''�field.fieldOwner�.�field.simpleName� = �param.simpleName�;'''
				static = field.static
				it.visibility = visibility
			]
		}
		
		private def orObject(TypeReference ref) {
			if (ref === null) object else ref
		}
	}
}
