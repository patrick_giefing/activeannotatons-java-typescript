package es.rick.activeannotations.processor.typescript

import com.google.common.base.CaseFormat
import es.rick.activeannotations.TypeScriptServices
import es.rick.activeannotations.processor.delegate.ParameterizedDelegateProcessor
import es.rick.activeannotations.processor.delegate.ParameterizedDelegateProcessorConfig
import javax.ws.rs.PathParam
import javax.ws.rs.QueryParam
import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtend.lib.annotations.ToString
import org.eclipse.xtend.lib.macro.CodeGenerationContext
import org.eclipse.xtend.lib.macro.TransformationContext
import org.eclipse.xtend.lib.macro.ValidationContext
import org.eclipse.xtend.lib.macro.declaration.AnnotationReference
import org.eclipse.xtend.lib.macro.declaration.EnumerationTypeDeclaration
import org.eclipse.xtend.lib.macro.declaration.ParameterDeclaration
import org.eclipse.xtend.lib.macro.declaration.TypeDeclaration
import org.eclipse.xtend.lib.macro.declaration.TypeReference
import org.eclipse.xtend.lib.macro.services.ProblemSupport
import org.eclipse.xtext.xbase.lib.Functions.Function1

import static extension es.rick.activeannotations.processor.rest.RestProcessor.*
import javax.ws.rs.core.Context
import javax.ws.rs.Path
import es.rick.activeannotations.GenerationTarget

class TypeScriptServicesProcessor extends TypeScriptProcessor {
	override void doTransform(extension TypeDeclaration decl, extension TransformationContext context) {
	}

	override void doValidate(extension TypeDeclaration decl, extension ValidationContext context) {
		val servicesConfig = decl.readConfig
		val serviceClasses = servicesConfig.restServices.map[it.value]
		servicesConfig.checkConfig(decl, context)
		if (servicesConfig.projectRoot.nullOrEmpty || servicesConfig.filePath.nullOrEmpty) {
			decl.addError("projectRoot and filePath must not be null or empty")
		}
		TypeScriptImporter.createTypeScriptImports(servicesConfig, serviceClasses, #[], decl, context)
		validateTypeScriptWebSocketSubscriberServices(decl, context, servicesConfig.webSocketSubscriberServices)
		validateTypeScriptWebSocketRpcServices(decl, context, servicesConfig.webSocketRpcServices)
	}

	override void doGenerateCode(extension TypeDeclaration decl, extension CodeGenerationContext context) {
		val servicesConfig = decl.readConfig

		if (servicesConfig.projectRoot.nullOrEmpty || servicesConfig.filePath.nullOrEmpty) {
			// clazz.addError("Annotation attribute fileName must not be empty")
			// clazz.addError("Annotation attribute outputDirectory must not be empty")
			return
		}

		val sb = new StringBuilder
		val serviceClasses = #[
			servicesConfig.restServices + servicesConfig.webSocketSubscriberServices +
				servicesConfig.webSocketRpcServices
		].flatten.map[it.value]
		val imports = TypeScriptImporter.createTypeScriptImports(servicesConfig, serviceClasses, #[], decl, null)
		//sb.append('import {ServiceDefinitionWrapper, ServiceDefinition, TypeDefinition, ParamDefinition, MethodDefinition} from "basis.gui/util/basis.gui.service.definition";').nl
		if (!servicesConfig.webSocketSubscriberServices.nullOrEmpty ||
			!servicesConfig.webSocketRpcServices.nullOrEmpty) {
			sb.append(
				'import {WebSocketClient, WebSocketSubscription} from "basis.gui/websocket/basis.gui.websocket";')
		}
		sb.append(imports).nl(2)

		generateTypeScriptRestServices(decl, context, servicesConfig, sb)
		generateTypeScriptWebSocketSubscriberServices(decl, context, servicesConfig, sb)

		val tsFile = context.getProjectFolder(decl.compilationUnit.filePath).append(servicesConfig.projectRoot).append(
			servicesConfig.filePath)
		tsFile.contents = sb.toString
		decl.copyTypeScriptResources(context, servicesConfig)
	}

	def void generateDelegates(StringBuilder sb, TypeScriptServiceProcessorConfig serviceConfig) {
		if (serviceConfig.delegates.nullOrEmpty) {
			return
		}
		serviceConfig.delegates.forEach [ delegate |
			val paramTypes = ParameterizedDelegateProcessor.getParamPropertyTypes(null, null,
				serviceConfig.value.type as TypeDeclaration, delegate.attributes.toList)
			if (paramTypes.empty) {
				return
			}
			val serviceClass = serviceConfig.value.type as TypeDeclaration
			val delegateName = serviceClass.qualifiedName.generateTargetName(GenerationTarget.CLASS,
				delegate.namePattern).simpleName
			sb.append('''
			export class �delegateName��serviceConfig.value.getTypeString(false, true)� {
				public delegate: �serviceConfig.value.getTypeString(true, true)�;
				�FOR String attribute : delegate.attributes�
					�attribute�: �paramTypes.get(attribute).getTypeString(true, true)�;
				�ENDFOR�
				constructor(delegate: �serviceConfig.value.getTypeString(true, true)�, �delegate.attributes.map['''�it�: �paramTypes.get(it).getTypeString(true, true)�'''].join(", ")�) {
					this.delegate = delegate;
					�FOR String attribute : delegate.attributes�
						this.�attribute� = �attribute�;
					�ENDFOR�
				}''').nl(2)
			val methods = serviceClass.allDeclaredMethods
			methods.forEach [ method |
				val nonDelegateParams = method.parameters.filter[!delegate.attributes.contains(it)]
				// TODO subscriber services have subscription returnType
				sb.append('''public �method.simpleName�(�nonDelegateParams.map['''�simpleName�: �type.getTypeString(true, true)�''']�): void {
					this.delegate.�method.simpleName�(�method.parameters.map['''�IF delegate.attributes.contains(it)�this.�ENDIF��simpleName�'''].join(", ")�);
				}''').nl
			]
			sb.append('''}''').nl
		]
	}

	def int validateTypeScriptWebSocketRpcServices(TypeDeclaration decl, extension ProblemSupport problemSupport,
		Iterable<TypeScriptServiceProcessorConfig> rpcServices) {
		val errorCount = rpcServices.map [ rpcService |
			val rpcClazz = rpcService.value.type as TypeDeclaration
			val methods = rpcClazz.allDeclaredMethods
			val methodsWithPath = methods.filter [
				annotations.exists[annotationTypeDeclaration.qualifiedName == Path.name]
			]
			if (methodsWithPath.empty) {
				if (problemSupport !== null) {
					decl.
						addError('''�rpcClazz.qualifiedName� doesn't contain any methods annotated with @�Path.name�''')
				}
				return 1
			}
			return 0
		].reduce[a, b|a + b] ?: 0 
		return errorCount
	}

	def void generateTypeScriptWebSocketRpcServices(extension TypeDeclaration decl,
		extension CodeGenerationContext context, TypeScriptServicesProcessorConfig servicesConfig, StringBuilder sb) {
		val rpcServices = servicesConfig.webSocketRpcServices?.filter[value.type instanceof TypeDeclaration]
		if (rpcServices.nullOrEmpty) {
			return
		}
		sb.nl
		rpcServices.forEach [ rpcService |
			val serviceClazz = rpcService.value.type as TypeDeclaration
			val serviceMethods = serviceClazz.allDeclaredMethods
			sb.append('''export class �serviceClazz� {''').nl
			serviceMethods.forEach [ serviceMethod |
				sb.
					append('''	public static readonly �CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, serviceMethod.simpleName)� = "�serviceClazz.simpleName�.�serviceMethod.simpleName�";''').
					nl
			]
			sb.append('''	private webSocketClient : WampClient = null;''').nl(2)
			sb.append('''
			//
				constructor(webSocketClient : WampClient) {
					this.webSocketClient = webSocketClient;
				}''').nl
			serviceMethods.forEach [ serviceMethod |
				var methodPath = serviceMethod.rsPath
				val serviceMethodConstName = CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE,
					serviceMethod.simpleName)
				val responseConversion = serviceMethod.getConvertResponseDataExpression("entity")
				val responseTypeName = serviceMethod.returnType.getTypeString(true, true)
				val params = serviceMethod.parameters.filter [
					!annotations.exists[annotationTypeDeclaration.qualifiedName == Context.name]
				]
				var paramStrings = params.map['''�simpleName� : �it.type.getTypeString(true, true)�''']
				val paramString = #[
					paramStrings,
					#[
						'''callbackSuccess: (�IF !serviceMethod.returnType.primitiveIfWrapper.void�entity: �responseTypeName��ENDIF�) => void''',
						'''callbackError: (error: any) => void = null''',
						'''callbackAfter: () => void = null'''
					]
				].flatten.join(", ")
				sb.nl(2)
				sb.appendJavaDoc(serviceMethod.docComment, 1)
				sb.append('''	public �serviceMethod.simpleName� (�paramString�): void {''').nl
				sb.append('''		let topic = "�methodPath�";''').nl

				sb.append('''
				//
						let callbackSuccessNative = (data: any): void => {
							�IF !serviceMethod.returnType.primitiveIfWrapper.void�
								�responseConversion�
								callbackSuccess(entity);
							�ELSE�
								callbackSuccess();
							�ENDIF�
						};
						this.webSocketClient.call("�methodPath�", [�params.join(", ")�], callbackSuccessNative, callbackError, callbackAfter);''').
					nl
				sb.nl
			]
			sb.generateDelegates(rpcService)
		]
	}

	def int validateTypeScriptWebSocketSubscriberServices(TypeDeclaration decl, extension ProblemSupport problemSupport,
		Iterable<TypeScriptServiceProcessorConfig> subscriberServices) {
		val Function1<ParameterDeclaration, Boolean> filterNotHasPathParamAnnotation = [
			annotations.findFirst[annotationTypeDeclaration.qualifiedName == PathParam.name] === null
		]
		val errorCount = subscriberServices.map [ subscriberService |
			val interfaze = subscriberService.value.type as TypeDeclaration
			val methods = interfaze.allDeclaredMethods
			methods.map [ method |
				var errors = 0
				if (!method.returnType.void) {
					if (problemSupport !== null) {
						decl.
							addError('''Interface �interfaze.qualifiedName� Method �method.simpleName� must have void return type''')
					}
					errors++
				}
				val paramsWithoutPathAnnotation = method.parameters.filter(filterNotHasPathParamAnnotation)
				if (paramsWithoutPathAnnotation.size !== 1) {
					if (problemSupport !== null) {
						decl.
							addError('''Interface �interfaze.qualifiedName� Method �method.simpleName� has �method.parameters.size� parameters without annotation �PathParam.name�. WebSocket methods must have 1 parameter without path annotation (�paramsWithoutPathAnnotation.map[simpleName].join(", ")�)''')
					}
					errors++
				}
				return errors
			].reduce[a, b|a + b] ?: 0
		].reduce[a, b|a + b] ?: 0
		return errorCount
	}

	def void generateTypeScriptWebSocketSubscriberServices(extension TypeDeclaration decl,
		extension CodeGenerationContext context, TypeScriptServicesProcessorConfig servicesConfig, StringBuilder sb) {
		val subscriberServices = servicesConfig.webSocketSubscriberServices?.filter [
			value.type instanceof TypeDeclaration
		]
		if (subscriberServices.nullOrEmpty) {
			return
		}
		val Function1<ParameterDeclaration, Boolean> filterHasPathParamAnnotation = [
			annotations.findFirst[annotationTypeDeclaration.qualifiedName == PathParam.name] !== null
		]
		val Function1<ParameterDeclaration, Boolean> filterNotHasPathParamAnnotation = [
			annotations.findFirst[annotationTypeDeclaration.qualifiedName == PathParam.name] === null
		]
		val errorCount = validateTypeScriptWebSocketSubscriberServices(decl, null,
			servicesConfig.webSocketSubscriberServices)
		if (errorCount > 0) {
			return
		}
		sb.nl
		subscriberServices.forEach [ subscriberService |
			val serviceClazz = subscriberService.value.type as TypeDeclaration
			val servicePath = serviceClazz.rsPath
			val serviceMethods = serviceClazz.allDeclaredMethods
			sb.appendJavaDoc(serviceClazz.docComment)
			sb.append('''export class �serviceClazz.simpleName� {''').nl
			serviceMethods.forEach [ serviceMethod |
				sb.
					append('''	public static readonly �CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, serviceMethod.simpleName)� = "�serviceClazz.simpleName�.�serviceMethod.simpleName�";''').
					nl
			]
			sb.append('''	private webSocketClient : WebSocketClient = null;''').nl(2)
			sb.append('''
			//
				constructor(webSocketClient : WebSocketClient) {
					this.webSocketClient = webSocketClient;
				}''').nl
			serviceMethods.forEach [ serviceMethod |
				var methodPath = serviceMethod.rsPath
				if (!servicePath.nullOrEmpty) {
					methodPath = '''�servicePath.addTrailingSlash��methodPath�'''
				}
				val serviceMethodConstName = CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE,
					serviceMethod.simpleName)
				val responseTypeParam = serviceMethod.parameters.filter(filterNotHasPathParamAnnotation).head
				val responseType = responseTypeParam.type // Es kann nur genau einen Parameter geben
				val responseTypeName = responseType.getTypeString(true, true)
				val responseConversion = responseTypeParam.getConvertResponseDataExpression("data")
				val pathParamParams = serviceMethod.parameters.filter(filterHasPathParamAnnotation)
				var paramString = pathParamParams.map['''�simpleName� : �it.type.getTypeString(true, true)�'''].join(
					", ")
				sb.nl(2)
				sb.appendJavaDoc(serviceMethod.docComment, 1)
				sb.
					append('''	public register�serviceMethod.simpleName.toFirstUpper� (�paramString��IF !paramString.nullOrEmpty�, �ENDIF�callback : (�responseTypeName�) => void) : WebSocketSubscription<�responseTypeName�> {''').
					nl
				sb.append('''		let topic = "�methodPath�";''').nl
				pathParamParams.forEach [ pathParam |
					val pathAnnotation = pathParam.annotations.findFirst [ ann |
						ann.annotationTypeDeclaration.qualifiedName == PathParam.name
					]
					val pathName = pathAnnotation.getStringValue("value")
					sb.
						append('''		topic = topic.replace(/{�pathName�}/g, �pathParam.simpleName.encodeUrlPartIfString(pathParam.type)�);''').
						nl
				]
				sb.append('''
				//
						let subscription = new WebSocketSubscription<�responseTypeName�>();
						subscription.topic = topic;
						subscription.callback = callback;
						subscription.serviceMethodName = �serviceClazz.simpleName�.�serviceMethodConstName�;
						subscription.entityConversion = (data): �responseTypeName� => {
							�responseConversion�
							return entity;
						};
						this.webSocketClient.registerSubscription(subscription);
						return subscription;''').nl
				sb.nl
			]
			sb.nl
			sb.generateDelegates(subscriberService)
		]
	}

	def void generateTypeScriptRestServices(extension TypeDeclaration decl, extension CodeGenerationContext context,
		TypeScriptServicesProcessorConfig servicesConfig, StringBuilder sb) {
		val restServices = servicesConfig.restServices
		if (restServices.nullOrEmpty) {
			return
		}
		// TODO conversion for LocalDate etc
		restServices.forEach [ restService |
			val serviceClazz = restService.value.type as TypeDeclaration
			val servicePath = serviceClazz.rsPath
			val serviceMethods = serviceClazz.allDeclaredMethods.filter(
				ParameterizedDelegateProcessor.wrapperMethodsFilter)
			sb.appendJavaDoc(serviceClazz.docComment)
			sb.append('''export class �serviceClazz.simpleName� {''').nl
			serviceMethods.forEach [ serviceMethod |
				sb.
					append('''	public static readonly �CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, serviceMethod.simpleName)� = "�serviceClazz.simpleName�.�serviceMethod.simpleName�";''').
					nl
				sb.
					append('''	public static readonly �CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, serviceMethod.simpleName)�_REQUEST = "�serviceClazz.simpleName�.�serviceMethod.simpleName�Request";''').
					nl
				sb.
					append('''	public static readonly �CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, serviceMethod.simpleName)�_RESPONSE_SUCCESS = "�serviceClazz.simpleName�.�serviceMethod.simpleName�ResponseSuccess";''').
					nl
				sb.
					append('''	public static readonly �CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, serviceMethod.simpleName)�_RESPONSE_ERROR = "�serviceClazz.simpleName�.�serviceMethod.simpleName�ResponseError";''').
					nl
			]
			sb.append('''	private contextPath : string = null;''').nl(2)
			sb.append('''
			//
				constructor(contextPath : string) {
					this.contextPath = contextPath;
				}''')
			serviceMethods.forEach [ serviceMethod |
				val serviceMethodConstName = CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE,
					serviceMethod.simpleName)
				sb.nl(2)
				sb.appendJavaDoc(serviceMethod.docComment, 1)
				sb.append('''	public �serviceMethod.simpleName�Object(data : any) {''').nl
				var paramString = serviceMethod.parameters.map[param|'''data.�param.simpleName�'''].join(", ")
				if (!paramString.empty) {
					paramString += ", "
				}
				paramString += "data.callbackSuccess, data.callbackError, data.callbackAfter"
				sb.append('''		this.�serviceMethod.simpleName�(�paramString�);''').nl
				sb.append("	}").nl(2)
				val sParams = serviceMethod.parameters.map [ param |
					'''�param.simpleName� : �param.type.getTypeString(true, true)�'''
				].join(", ")
				val sParamsPrefix = '''�sParams��IF !sParams.nullOrEmpty�, �ENDIF�'''
				sb.appendJavaDoc(serviceMethod.docComment)
				sb.
					append('''	public �serviceMethod.simpleName�Func = (�sParamsPrefix�callbackSuccess : (�IF !serviceMethod.returnType.void�entity: �serviceMethod.returnType.getTypeString(true, true)��ENDIF�) => void, callbackError : (error: any) => void = null, callbackAfter : () => void = null) : void => {''').
					nl
				sb.append('''		this.�serviceMethod.simpleName�(�paramString.replace("data.", "")�);''').nl
				sb.append("	}").nl(2)

				sb.appendJavaDoc(serviceMethod.docComment)
				sb.
					append('''	public �serviceMethod.simpleName�(�sParamsPrefix�callbackSuccess : (�IF !serviceMethod.returnType.void�entity: �serviceMethod.returnType.getTypeString(true, true)��ENDIF�) => void, callbackError : (error: any) => void = null, callbackAfter : () => void = null) : void {''').
					nl
				sb.
					append('''		let sUrl : string = this.contextPath + "�servicePath�/�serviceMethod.rsPath�/?callTimestamp=" + (new Date()).getTime();''').
					nl
				val queryParamParams = serviceMethod.parameters.filter [
					annotations.exists[annotationTypeDeclaration.qualifiedName == QueryParam.name]
				]
				val pathParamParams = serviceMethod.parameters.filter [
					annotations.exists[annotationTypeDeclaration.qualifiedName == PathParam.name]
				]
				pathParamParams.forEach [ pathParam |
					val pathAnnotation = pathParam.annotations.findFirst [ ann |
						ann.annotationTypeDeclaration.qualifiedName == PathParam.name
					]
					val pathName = pathAnnotation.getStringValue("value")
					sb.
						append('''		sUrl = sUrl.replace(/{�pathName�}/g, �pathParam.simpleName.encodeUrlPartIfString(pathParam.type)�);''').
						nl
				]
				queryParamParams.forEach [ queryParam |
					val queryAnnotation = queryParam.annotations.findFirst [ ann |
						ann.annotationTypeDeclaration.qualifiedName == QueryParam.name
					]
					val queryName = queryAnnotation.getStringValue("value")
					if (queryParam.type.isCollection) {
						sb.append('''
						//
								if(�queryParam.simpleName�) {
									for(let �queryParam.simpleName�Entry of �queryParam.simpleName�) {
										sUrl += "&�queryName�=" + �(queryParam.simpleName + "Entry").encodeUrlPartIfString(queryParam.type)�;
									}
								}''');
					} else if (queryParam.type.type instanceof EnumerationTypeDeclaration) {
						sb.append('''
						//		
								if(�queryParam.simpleName�) {
									sUrl += "&�queryName�=" + �queryParam.type.simpleName�[�queryParam.simpleName�];
								}''')
					} else {
						sb.append('''
						//		
								if(�queryParam.simpleName� != null && �queryParam.simpleName� != undefined) {
									sUrl += "&�queryName�=" + �queryParam.simpleName.encodeUrlPartIfString(queryParam.type)�;
								}''')
					}
					sb.nl
				]
				val requestMethod = if(serviceMethod.isPost) "POST" else "GET"

				val paramsWithoutJaxRsAnnotations = serviceMethod.parameters.filter [
					annotations.filter[annotationTypeDeclaration.qualifiedName.startsWith("javax.ws.rs.")].empty
				]
				var boolean contentTypeJson = false
				if (serviceMethod.isPost) {
					/**
					 * TODO auf Consumes-Header �berpr�fen
					 * Im Moment wird json als Content-Type gesetzt wenn es ein RequestEntity gibt und dieses kein String ist
					 */
					if (!paramsWithoutJaxRsAnnotations.nullOrEmpty && !paramsWithoutJaxRsAnnotations.head.type.string) {
						contentTypeJson = true
					}
				}
				if (serviceMethod.isPost) {
					sb.append('''		let dataString = null;''').nl
					if (paramsWithoutJaxRsAnnotations.size === 1) {
						val entityName = paramsWithoutJaxRsAnnotations.head.simpleName
						sb.
							append('''		dataString = �entityName� && typeof �entityName� == "object" ? JSON.stringify(�entityName�) : �entityName�;''').
							nl
					} else if (paramsWithoutJaxRsAnnotations.size > 1) {
						// TODO clazz.addError('''�serviceClazz.qualifiedName�.�serviceMethod.simpleName�: There are multiple parameters without jax.rs-annotations''')
					}
				}

				val type = serviceMethod.returnType
				val convertDataExpression = serviceMethod.getConvertResponseDataExpression("data")

				sb.append('''
					//
							let callCallbackAfter = () => {
								if (callbackAfter != null) {
									try {
										callbackAfter();
									} catch (_ex2) {
										console.error("Error while executing after handler in ajax method �serviceClazz.simpleName�.�serviceMethod.simpleName� " + _ex2, _ex2);
									}
								}
							};
							let ajaxParams : any = {
							    method: "�requestMethod�",
							    url: sUrl,
							    �IF type.primitive || type.wrapper || type.void || type.string�dataType: "text",�ENDIF�
							    �IF requestMethod == "POST"�data: dataString,�ENDIF�
							    contentType: �IF contentTypeJson�"application/json; charset=utf-8"�ELSE�"text/plain; charset=utf-8"�ENDIF�
							};
							$(document).trigger(�serviceClazz.simpleName�.�serviceMethodConstName�_REQUEST, ajaxParams);
							$.ajax(ajaxParams).done((data : any, textStatus : string, jqXHR : any) => {
								let successParams : any = {ajax: this, response: jqXHR, url: sUrl, requestMethod: "�requestMethod�", code: jqXHR.status};
								$(document).trigger("basis.http.response", successParams);
								$(document).trigger(�serviceClazz.simpleName�.�serviceMethodConstName�_RESPONSE_SUCCESS, successParams);
								�convertDataExpression�
								try {
								   	callbackSuccess(�IF !type.primitiveIfWrapper.void�entity�ENDIF�);
								   } catch (_ex1) {
								   	console.error("Error while executing success handler in ajax method �serviceClazz.simpleName�.�serviceMethod.simpleName� " + _ex1, _ex1);
								   }
								   callCallbackAfter();
							}).fail((jqXHR : any, textStatus : string, errorThrown : any) => {
								let errorParams : any = {ajax: this, response: jqXHR, url: sUrl, requestMethod: "�requestMethod�", code: jqXHR.status};
								$(document).trigger("basis.http.response", errorParams);
								$(document).trigger(�serviceClazz.simpleName�.�serviceMethodConstName�_RESPONSE_ERROR, errorParams);
								   console.error("error in ajax method �serviceClazz.simpleName�.�serviceMethod.simpleName� " + textStatus + " - " + errorThrown);
								if(callbackError) {
								    try {
								    	callbackError(jqXHR);
								    } catch (_ex) {
								    	console.error("Error while executing error handler in ajax method �serviceClazz.simpleName�.�serviceMethod.simpleName� " + _ex, _ex);
								    }
								   }
								   callCallbackAfter();
							});
				''')
				sb.append("	}")
			]
			sb.nl.append("}").nl
			sb.generateDelegates(restService)
		]
	}

	def static boolean checkConfig(TypeScriptServicesProcessorConfig config, extension TypeDeclaration decl,
		extension ProblemSupport problemSupport) {
		if (config.restServices.nullOrEmpty && config.webSocketRpcServices.nullOrEmpty &&
			config.webSocketSubscriberServices.nullOrEmpty) {
			decl.addError("interfaceClasses or webSocketRpcClasses or webSocketSubscriberClasses must be set")
		}
		if (config.projectRoot.nullOrEmpty) {
			decl.addError("projectRoot must not be null or empty")
		}
		if (config.filePath.nullOrEmpty) {
			decl.addError("filePath must not be null or empty")
		}
		return true
	}

	def static TypeScriptServicesProcessorConfig readConfig(TypeDeclaration decl) {
		new TypeScriptServicesProcessorConfig(decl.annotations.findFirst [
			annotationTypeDeclaration.qualifiedName == TypeScriptServices.name
		])
	}
}

@ToString @Accessors
class TypeScriptServicesProcessorConfig extends TypeScriptProcessorConfig {
	val TypeScriptServiceProcessorConfig[] restServices
	val TypeScriptServiceProcessorConfig[] webSocketSubscriberServices
	val TypeScriptServiceProcessorConfig[] webSocketRpcServices

	new(AnnotationReference ann) {
		super(ann)
		restServices = ann.getAnnotationArrayValue("restServices").map[new TypeScriptServiceProcessorConfig(it)]
		webSocketSubscriberServices = ann.getAnnotationArrayValue("webSocketSubscriberServices").map [
			new TypeScriptServiceProcessorConfig(it)
		]
		webSocketRpcServices = ann.getAnnotationArrayValue("webSocketRpcServices").map [
			new TypeScriptServiceProcessorConfig(it)
		]
	}
}

@ToString @Accessors
class TypeScriptServiceProcessorConfig {
	val TypeReference value
	val ParameterizedDelegateProcessorConfig[] delegates

	new(AnnotationReference ann) {
		value = ann.getClassValue("value")
		delegates = ann.getAnnotationArrayValue("delegates").map[new ParameterizedDelegateProcessorConfig(it)]
	}
}
