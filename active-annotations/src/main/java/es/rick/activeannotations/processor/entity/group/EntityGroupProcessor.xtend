package es.rick.activeannotations.processor.entity.group

import es.rick.activeannotations.EntityGroup
import java.util.Arrays
import java.util.HashSet
import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtend.lib.annotations.ToString
import org.eclipse.xtend.lib.macro.AbstractClassProcessor
import org.eclipse.xtend.lib.macro.TransformationContext
import org.eclipse.xtend.lib.macro.declaration.AnnotationReference
import org.eclipse.xtend.lib.macro.declaration.ClassDeclaration
import org.eclipse.xtend.lib.macro.declaration.MutableClassDeclaration
import org.eclipse.xtend.lib.macro.declaration.TypeReference
import org.eclipse.xtend.lib.macro.declaration.Visibility
import java.util.Set
import javax.persistence.EmbeddedId

class EntityGroupProcessor extends AbstractClassProcessor {
	override doTransform(extension MutableClassDeclaration clazz, extension TransformationContext context) {
		val entityGroupConfig = clazz.readConfig
		doTransform(clazz, context, entityGroupConfig)
	}

	def void doTransform(extension MutableClassDeclaration clazz, extension TransformationContext context,
		extension EntityGroupProcessorConfig entityConfig) {
		val types = new HashSet<TypeReference>
		types.addAll(entityConfig.entities)
		if (appendCompilationUnitTypes) {
			var unitTypes = clazz.compilationUnit.sourceTypeDeclarations //
			.filter[qualifiedName != clazz.qualifiedName] // remove the class annoated with @CompilationUnitTypes
			.map[newSelfTypeReference]
			types.addAll(unitTypes)
		}
		val embeddedFieldTypes = types.filter[type instanceof ClassDeclaration].map[type as ClassDeclaration].map[
			declaredFields.filter[annotations.exists[annotationTypeDeclaration.qualifiedName == EmbeddedId.name]].map[type]
		].flatten.toSet
		types.addAll(embeddedFieldTypes)
		val typesFiltered = types.sortBy[name]
		clazz.addField("TYPES", [
			static = true
			final = true
			visibility = Visibility.PUBLIC
			type = Class.newTypeReference.newArrayTypeReference
			initializer = '''
			{
			�typesFiltered.map[t|'''		�t.name�.class'''].join(",\r\n")�
			}'''
			docComment = "Not immutable!"
		])
		clazz.addField("TYPE_LIST", [
			static = true
			final = true
			visibility = Visibility.PUBLIC
			type = List.newTypeReference(Class.newTypeReference.newWildcardTypeReference)
			initializer = '''
			�Arrays.name�.asList(
			�typesFiltered.map[t|'''		�t.name�.class'''].join(",\r\n")�
			)'''
		])
		checkAttributeNameWhiteList(clazz, context, typesFiltered, attributeNameWhnitelist.toSet)
	}

	def static void checkAttributeNameWhiteList(extension MutableClassDeclaration clazz,
		extension TransformationContext context, List<TypeReference> types, Set<String> nameWhitelist) {
		if (nameWhitelist.nullOrEmpty) {
			return
		}
		types.filter[type instanceof ClassDeclaration].map[type as ClassDeclaration].forEach [ t |
			t.declaredFields.filter[f|!f.static && !f.transient].sortBy[simpleName].forEach [ f |
				if (!nameWhitelist.contains(f.simpleName)) {
					clazz.addError('''field �t.qualifiedName�.�f.simpleName�: Name not in whitelist''')
				}
			// TODO additional validation
			]
		]
	}

	def static EntityGroupProcessorConfig readConfig(ClassDeclaration clazz) {
		new EntityGroupProcessorConfig(clazz.annotations.findFirst [
			annotationTypeDeclaration.qualifiedName == EntityGroup.name
		])
	}
}

@ToString @Accessors
class EntityGroupProcessorConfig {
	val TypeReference[] entities
	val String[] attributeNameWhnitelist
	val boolean appendCompilationUnitTypes

	new(AnnotationReference ann) {
		entities = ann.getClassArrayValue("entities")
		attributeNameWhnitelist = ann.getStringArrayValue("attributeNameWhnitelist")
		appendCompilationUnitTypes = ann.getBooleanValue("appendCompilationUnitTypes")
	}
}
