package es.rick.activeannotations.processor.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import com.google.common.annotations.GwtIncompatible
import es.rick.activeannotations.AccessorType
import es.rick.activeannotations.CompositeIdType
import es.rick.activeannotations.DefaultType
import es.rick.activeannotations.GenerateEntity
import es.rick.activeannotations.PropertyOrder
import es.rick.activeannotations.processor.ProcessorBase
import java.util.ArrayList
import java.util.HashSet
import java.util.List
import java.util.Set
import javafx.beans.property.ObjectProperty
import javax.persistence.Transient
import javax.ws.rs.Path
import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlRootElement
import javax.xml.bind.annotation.XmlTransient
import org.eclipse.xtend.lib.annotations.Accessors
import org.eclipse.xtend.lib.annotations.ToString
import org.eclipse.xtend.lib.macro.RegisterGlobalsContext
import org.eclipse.xtend.lib.macro.RegisterGlobalsParticipant
import org.eclipse.xtend.lib.macro.TransformationContext
import org.eclipse.xtend.lib.macro.TransformationParticipant
import org.eclipse.xtend.lib.macro.declaration.AnnotationReference
import org.eclipse.xtend.lib.macro.declaration.AnnotationTarget
import org.eclipse.xtend.lib.macro.declaration.ClassDeclaration
import org.eclipse.xtend.lib.macro.declaration.Element
import org.eclipse.xtend.lib.macro.declaration.EnumerationTypeDeclaration
import org.eclipse.xtend.lib.macro.declaration.FieldDeclaration
import org.eclipse.xtend.lib.macro.declaration.MethodDeclaration
import org.eclipse.xtend.lib.macro.declaration.MutableClassDeclaration
import org.eclipse.xtend.lib.macro.declaration.MutableFieldDeclaration
import org.eclipse.xtend.lib.macro.declaration.MutableMethodDeclaration
import org.eclipse.xtend.lib.macro.declaration.MutableTypeDeclaration
import org.eclipse.xtend.lib.macro.declaration.ParameterDeclaration
import org.eclipse.xtend.lib.macro.declaration.TypeDeclaration
import org.eclipse.xtend.lib.macro.declaration.TypeReference
import org.eclipse.xtend.lib.macro.declaration.Visibility
import org.eclipse.xtend.lib.macro.services.ProblemSupport
import org.junit.Test
import org.springframework.web.client.RestTemplate

/**
 * TODO @JsonInclude(Include.NON_NULL)
 */
final class EntityProcessor extends ProcessorBase implements RegisterGlobalsParticipant<TypeDeclaration>, TransformationParticipant<MutableTypeDeclaration> {
	override doRegisterGlobals(List<? extends TypeDeclaration> decls, extension RegisterGlobalsContext context) {
		decls.filter[it instanceof ClassDeclaration].map[it as ClassDeclaration].forEach[doRegisterGlobals(context)]
	}

	def doRegisterGlobals(TypeDeclaration decl, extension RegisterGlobalsContext context) {
		val entityConfig = decl.readConfig
		val compositeIdProcessor = new EntityCompositeIdProcessor
		compositeIdProcessor.doRegisterGlobals(decl, context, entityConfig)
		if (entityConfig.generateFieldsEnum) {
			val fieldsEnumProcessor = new EntityFieldsEnumProcessor
			fieldsEnumProcessor.doRegisterGlobals(decl, context, entityConfig)
		}
		if (!entityConfig.generateInterfaces.nullOrEmpty) {
			val generateInterfaceConfigs = entityConfig.generateInterfaces.map [
				GenerateInterfaceProcessor.create(it, decl.qualifiedName, null)
			]
			val generateInterfaceProcessor = new GenerateInterfaceProcessor
			generateInterfaceProcessor.doRegisterInterfaces(generateInterfaceConfigs, context)
		}
	}

	override doTransform(List<? extends MutableTypeDeclaration> decls, extension TransformationContext context) {
		decls.filter[!(it instanceof MutableClassDeclaration)].forEach[addError('''�simpleName� is not a class''')]
		decls.filter[it instanceof MutableClassDeclaration].map[it as MutableClassDeclaration].forEach [
			doTransform(context)
		]
	}

	def doTransform(extension MutableClassDeclaration clazz, extension TransformationContext context) {
		val entityConfig = clazz.readConfig
		doTransform(clazz, context, entityConfig)
		clazz.removeAnnotation(GenerateEntity.name)
	}

	def static transformGeneratedEntity(extension MutableClassDeclaration clazz,
		extension TransformationContext context, EntityProcessorConfig entityConfig) {
		val entityProcessor = new EntityProcessor
		entityProcessor.doTransform(clazz, context, entityConfig, true)
	}

	def void doTransform(extension MutableClassDeclaration clazz, extension TransformationContext context,
		EntityProcessorConfig entityConfig) {
		doTransform(clazz, context, entityConfig, false)
	}

	def void doTransform(extension MutableClassDeclaration clazz, extension TransformationContext context,
		EntityProcessorConfig entityConfig, boolean generatedEntity) {
		new EntityCompositeIdProcessor().transformEntity(clazz, context, entityConfig)
		new EntityPrePersistProcessor().transformEntity(clazz, context, entityConfig)
		new EntityJsonPropertyOrdersProcessor().transformEntity(clazz, context, entityConfig)
		if (entityConfig.toStringConfig.generateToString) {
			new EntityToStringProcessor().transformEntity(clazz, context, entityConfig)
		}
		if (entityConfig.generateEqualsAndHashCode) {
			new EntityEqualsHashCodeProcessor().transformEntity(clazz, context, entityConfig)
		}
		if (entityConfig.cloneable) {
			new EntityCloneableProcessor().transformEntity(clazz, context, entityConfig)
		}
		if (entityConfig.serializable) {
			new EntitySerializableProcessor().transformEntity(clazz, context, entityConfig)
		}
		if (entityConfig.collectionMethodsConfig.generateCollectionMethods) {
			new EntityCollectionMethodsProcessor().transformEntity(clazz, context, entityConfig)
		}
		new EntityAccessorsProcessor().transformEntity(clazz, context, entityConfig)
		// Generated constructors use previous generated setters
		if (!generatedEntity) {
			// the config information doesn't apply to generated sub entities
			new EntityConstructorProcessor().transformEntity(clazz, context, entityConfig)
			val generateInterfaceProcessor = new GenerateInterfaceProcessor
			entityConfig.generateInterfaces.forEach [
				val generateInterfaceConfig = GenerateInterfaceProcessor.create(it, clazz.qualifiedName, null)
				generateInterfaceProcessor.doTransform(clazz, context, generateInterfaceConfig)
			]
		}
		if (entityConfig.generateFieldsEnum) {
			new EntityFieldsEnumProcessor().transformEntity(clazz, context, entityConfig)
		}
	}

	def static EntityProcessorConfig readConfig(TypeDeclaration clazz) {
		new EntityProcessorConfig(clazz.annotations.findFirst [
			annotationTypeDeclaration.qualifiedName == GenerateEntity.name
		])
	}
}

abstract class EntityBaseProcessor {
	public static final String COBERTURA = "cobertura"

	def void doRegisterGlobals(TypeDeclaration clazz, extension RegisterGlobalsContext context,
		extension EntityProcessorConfig entityConfig) {}

	def abstract void transformEntity(extension MutableClassDeclaration clazz, extension TransformationContext context,
		extension EntityProcessorConfig entityConfig)

	def static MutableMethodDeclaration addGetterMethod(MutableClassDeclaration clazz, FieldDeclaration field,
		extension TransformationContext context) {
		return addGetterMethod(clazz, field, context, false)
	}

	def static MutableMethodDeclaration addGetterMethod(MutableClassDeclaration clazz, FieldDeclaration field,
		extension TransformationContext context, boolean ignore) {
		var getterName = '''get�field.simpleName.toFirstUpper�'''
		if (boolean.newTypeReference.isAssignableFrom(field.type)) {
			getterName = '''is�field.simpleName.toFirstUpper�'''
		}
		return clazz.addMethod(getterName, [
			primarySourceElement = field.primarySourceElement
			returnType = field.type
			body = '''return this.�field.simpleName�;'''
			if (ignore) {
				addAnnotation(Transient.newAnnotationReference)
				addAnnotation(XmlTransient.newAnnotationReference)
				addAnnotation(JsonIgnore.newAnnotationReference)
			}
		])
	}

	def static MutableMethodDeclaration addDelegateGetterMethod(MutableClassDeclaration clazz, FieldDeclaration field,
		FieldDeclaration delegate, extension TransformationContext context) {
		var getterName = '''get�field.simpleName.toFirstUpper�'''
		if (boolean.newTypeReference.isAssignableFrom(field.type)) {
			getterName = '''is�field.simpleName.toFirstUpper�'''
		}
		// TODO delegate erstellen falls null
		val delegateBody = '''return this.�delegate.simpleName�.�getterName�();'''
		return clazz.addMethod(getterName, [
			primarySourceElement = field.primarySourceElement
			returnType = field.type
			body = '''�delegateBody�'''
			addAnnotation(Transient.newAnnotationReference)
			addAnnotation(XmlTransient.newAnnotationReference)
			addAnnotation(JsonIgnore.newAnnotationReference)
		])
	}

	def static MutableMethodDeclaration addSetterMethod(MutableClassDeclaration clazz, FieldDeclaration field,
		extension TransformationContext context) {
		return clazz.addMethod('''set�field.simpleName.toFirstUpper�''', [
			primarySourceElement = field.primarySourceElement
			addParameter(field.simpleName, field.type)
			body = '''this.�field.simpleName� = �field.simpleName�;'''
		])
	}

	def static MutableMethodDeclaration addDelegateSetterMethod(MutableClassDeclaration clazz, FieldDeclaration field,
		FieldDeclaration delegate, extension TransformationContext context) {
		// TODO delegate erstellen falls null
		return clazz.addMethod('''set�field.simpleName.toFirstUpper�''', [
			primarySourceElement = field.primarySourceElement
			addParameter(field.simpleName, field.type)
			body = '''this.�delegate.simpleName�.set�field.simpleName.toFirstUpper�(�field.simpleName�);'''
		])
	}

	def static String getDefaultValueString(TypeReference typeRef, extension TransformationContext context) {
		var defaultValue = "null"
		if (typeRef.primitive) {
			if (Number.newTypeReference.isAssignableFrom(typeRef)) {
				defaultValue = "0"
			} else if (boolean.newTypeReference.isAssignableFrom(typeRef)) {
				defaultValue = "false"
			}
		}
		return defaultValue
	}

	def static void setXmlRootElementAndAccessorType(MutableClassDeclaration clazz,
		extension TransformationContext context) {
		clazz.addAnnotation(XmlRootElement.newAnnotationReference)
		clazz.addAnnotation(XmlAccessorType.newAnnotationReference [
			val enAccessType = XmlAccessType.newTypeReference.type as EnumerationTypeDeclaration
			val enAccessTypeField = enAccessType.declaredValues.findFirst[it.simpleName.equalsIgnoreCase("FIELD")]
			setEnumValue("value", enAccessTypeField)
		])
	}

	def static <T> T addErrorIfNull(T t, ProblemSupport problemSupport, Element element, String message) {
		if (t === null) {
			problemSupport.addError(element, message)
		}
		return t
	}

	def static List<FieldDeclaration> sortFields(List<FieldDeclaration> fields, ClassDeclaration declaringClazz,
		PropertyOrder[] orders) {
		if (orders.nullOrEmpty) {
			return fields
		}
		val l = fields.sortWith([ c1, c2 |
			for (PropertyOrder order : orders) {
				if (order === PropertyOrder.AlphabeticalAsc || order === PropertyOrder.AlphabeticalDesc) {
					var diff = c1.simpleName.compareStrings(c2.simpleName)
					if (order == PropertyOrder.AlphabeticalDesc) {
						diff = diff * -1
					}
					if (diff !== 0) {
						return diff
					}
				} else if (order === PropertyOrder.IdsFirst || order === PropertyOrder.IdsLast) {
					val id1 = c1.simpleName.startsWith("id") || c1.simpleName.endsWith("Id")
					val id2 = c2.simpleName.startsWith("id") || c2.simpleName.endsWith("Id")
					var diff = 0
					if (id1 && !id2) {
						diff = -1
					} else if (!id1 && id2) {
						diff = 1
					}
					if (order == PropertyOrder.IdsLast) {
						diff = diff * -1
					}
					if (diff !== 0) {
						return diff
					}
				} else if (order === PropertyOrder.PrimitiveFirst || order === PropertyOrder.PrimitiveLast) {
					val primitive1 = c1.type.primitive
					val primitive2 = c2.type.primitive
					var diff = 0
					if (primitive1 && !primitive2) {
						diff = -1
					} else if (!primitive1 && primitive2) {
						diff = 1
					}
					if (order == PropertyOrder.PrimitiveLast) {
						diff = diff * -1
					}
					if (diff !== 0) {
						return diff
					}
				} else if (order === PropertyOrder.ParentFirst || order === PropertyOrder.ParentLast) {
					val c1Parent = c1.declaringType != declaringClazz
					val c2Parent = c2.declaringType != declaringClazz
					var diff = 0
					if (c1Parent && !c2Parent) {
						diff = -1
					} else if (!c1Parent && c2Parent) {
						diff = 1
					}
					if (order == PropertyOrder.ParentLast) {
						diff = diff * -1
					}
					if (diff !== 0) {
						return diff
					}
				}
			}
			return 0
		])
		return l
	}

	def static int compareStrings(String s1, String s2) {
		if (s1 === s2) {
			return 0
		}
		if (s1 === null) {
			return -1
		}
		if (s2 === null) {
			return 1
		}
		return s1.compareTo(s2)
	}

	def static List<MethodDeclaration> getAllPublicMemberMethods(ClassDeclaration clazz,
		@Extension TransformationContext context) {
		val allMethods = getAllMethods(clazz, context)
		allMethods.filter [
			/*returnType.void && */ visibility === Visibility.PUBLIC && !static
		].toList
	}

	def static List<MethodDeclaration> getAllMethods(ClassDeclaration clazz, @Extension TransformationContext context) {
		val List<MethodDeclaration> methods = newArrayList
		methods.addAll(clazz.declaredMethods)
		val extendedClazz = findClass(clazz.extendedClass.type.qualifiedName)
		if (extendedClazz !== null) {
			methods.addAll(getAllMethods(extendedClazz, context))
		}
		methods
	}

	def static List<FieldDeclaration> getAllFields(ClassDeclaration clazz) {
		val List<FieldDeclaration> fields = newArrayList
		fields.addAll(clazz.declaredFields.filter[!static && !final && !transient && !simpleName.contains(COBERTURA)])
		if (clazz?.extendedClass?.type instanceof ClassDeclaration) {
			val extendedClazz = clazz.extendedClass.type as ClassDeclaration
			fields.addAll(getAllFields(extendedClazz))
		}
		fields
	}

	def static TypeReference getCollectionTypeNew(@Extension AnnotationTarget annTarget,
		@Extension TransformationContext context) {
		getCollectionTypeNew(annTarget, annTarget, context)
	}

	def static TypeReference getCollectionTypeNew(@Extension AnnotationTarget annTarget, Element errorElement,
		@Extension TransformationContext context) {
		var _type = getType(annTarget, errorElement, context)
		val defaultType = getDefaultType(annTarget, errorElement, context)
		if (defaultType !== null) {
			return defaultType
		}
		if (_type.array) {
			return _type
		}
		if (List.name.equals(_type.type?.qualifiedName)) {
			_type = ArrayList.newTypeReference(_type.actualTypeArguments.head.upperBound)
		} else if (Set.name.equals(_type.type?.qualifiedName)) {
			_type = HashSet.newTypeReference(_type.actualTypeArguments.head.upperBound)
		}
		return _type
	}

	def static TypeReference getWrapperType(@Extension MutableFieldDeclaration field,
		@Extension TransformationContext context) {
		getDefaultType(field, context) ?: field.type
	}

	def static TypeReference getType(@Extension AnnotationTarget annTarget, @Extension TransformationContext context) {
		getType(annTarget, annTarget, context)
	}

	def static TypeReference getType(@Extension AnnotationTarget annTarget, Element errorElement,
		@Extension TransformationContext context) {
		if (annTarget instanceof FieldDeclaration) {
			return annTarget.type
		} else if (annTarget instanceof MethodDeclaration) {
			return annTarget.returnType
		} else if (annTarget instanceof ParameterDeclaration) {
			return annTarget.type
		}
		context.addError(errorElement, '''Unknown target type �annTarget�''')
		return null
	}

	def static TypeReference getDefaultType(@Extension AnnotationTarget annTarget,
		@Extension TransformationContext context) {
		getDefaultType(annTarget, annTarget, context)
	}

	def static TypeReference getDefaultType(@Extension AnnotationTarget annTarget, Element errorElement,
		@Extension TransformationContext context) {
		var TypeReference _type = getType(annTarget, context)
		var TypeReference _default = annTarget.annotations.findFirst [
			DefaultType.name.equals(annotationTypeDeclaration.qualifiedName)
		]?.getClassValue("value")
		if (_default !== null) {
			if (findClass(_default.type?.qualifiedName)?.abstract) {
				context.addError(errorElement, "Can't use abstract class as default instantiation type")
			}
			if (!_type.isAssignableFrom(_default)) {
				context.addError(
					errorElement, '''Annotation �DefaultType.name� set with a defaultType that is not assignable for field type''')
			}
			return context.findTypeGlobally(_default?.name).newTypeReference(_type.actualTypeArguments)
		}
		return null
	}

	def Iterable<? extends FieldDeclaration> getJsonFields(ClassDeclaration clazz) {
		clazz.declaredFields.filter [
			!static && !transient && !annotations.exists[annotationTypeDeclaration.qualifiedName == JsonIgnore.name]
		]
	}

	static class ClassPathUtil {
		def static boolean checkSpringWebClassPath(Element elem, extension TransformationContext context) {
			if (RestTemplate.newTypeReference === null) {
				elem.addError('''Couldn't find org.springframework.web.* on classpath''')
				return false
			}
			return true
		}

		def static boolean checkJavaxRsClassPath(Element elem, extension TransformationContext context) {
			if (Path.newAnnotationReference === null) {
				elem.addError('''Couldn't find javax.ws.rs.* on classpath''')
				return false
			}
			return true
		}

		def static boolean checkPersistenceClassPath(Element elem, extension TransformationContext context) {
			if (Transient.newAnnotationReference === null) {
				elem.addError('''Couldn't find javax.persistence.* on classpath''')
				return false
			}
			return true
		}

		def static boolean checkXmlBindClassPath(Element elem, extension TransformationContext context) {
			if (XmlTransient.newAnnotationReference === null) {
				elem.addError('''Couldn't find javax.xml.bind.annotation.* on classpath''')
				return false
			}
			return true
		}

		def static boolean checkGuavaClassPath(Element elem, extension TransformationContext context) {
			if (GwtIncompatible.newAnnotationReference === null) {
				elem.addError('''Couldn't find com.google.common.annotations.* on classpath''')
				return false
			}
			return true
		}

		def static boolean checkJacksonClassPath(Element elem, extension TransformationContext context) {
			if (JsonIgnore.newAnnotationReference === null) {
				elem.addError('''Couldn't find com.fasterxml.jackson.annotation.* on classpath''')
				return false
			}
			return true
		}

		def static boolean checkJUnitClassPath(Element elem, extension TransformationContext context) {
			if (Test.newAnnotationReference === null) {
				elem.addError('''Couldn't find org.junit.* on classpath''')
				return false
			}
			return true
		}

		def static boolean checkJavaFxClassPath(Element elem, extension TransformationContext context) {
			if (ObjectProperty.newTypeReference === null) {
				elem.addError('''Couldn't find javafx.beans.property.* on classpath''')
				return false
			}
			return true
		}
	}
}

@ToString @Accessors
class EntityProcessorConfig {
	val ToStringProcessorConfig toStringConfig
	val JpaProcessorConfig jpaConfig
	val PropertyOrder[] propertyOrder
	val AccessorsProcessorConfig accessors
	val boolean generateEqualsAndHashCode
	val boolean cloneable
	val boolean serializable
	val boolean generateFieldsEnum
	val boolean generateBuilder
	val CollectionMethodsProcessorConfig collectionMethodsConfig
	val EntityConstructorConfig[] constructorConfigs
	val GenerateInterfaceProcessorConfig[] generateInterfaces

	new(AnnotationReference ann) {
		toStringConfig = new ToStringProcessorConfig(ann.getAnnotationValue("toStringConfig"))
		jpaConfig = new JpaProcessorConfig(ann.getAnnotationValue("jpaConfig"))
		collectionMethodsConfig = new CollectionMethodsProcessorConfig(
			ann.getAnnotationValue("collectionMethodsConfig"))
		propertyOrder = ann.getEnumArrayValue("propertyOrder").map[PropertyOrder.valueOf(simpleName)]
		accessors = new AccessorsProcessorConfig(ann.getAnnotationValue("accessors"))
		generateEqualsAndHashCode = ann.getBooleanValue("generateEqualsAndHashCode")
		cloneable = ann.getBooleanValue("cloneable")
		serializable = ann.getBooleanValue("serializable")
		generateFieldsEnum = ann.getBooleanValue("generateFieldsEnum")
		generateBuilder = ann.getBooleanValue("generateBuilder")
		constructorConfigs = ann.getAnnotationArrayValue("constructors").map[new EntityConstructorConfig(it)]
		generateInterfaces = ann.getAnnotationArrayValue("generateInterfaces").map [
			new GenerateInterfaceProcessorConfig(it)
		]
	}
}

@ToString @Accessors
class EntityConstructorConfig {
	val String[] properties

	new(AnnotationReference ann) {
		properties = ann.getStringArrayValue("value")
	}
}

@ToString @Accessors
class AccessorsProcessorConfig {
	val AccessorType getter
	val AccessorType setter

	new(AnnotationReference ann) {
		getter = AccessorType.valueOf(ann.getEnumValue("getter").simpleName)
		setter = AccessorType.valueOf(ann.getEnumValue("setter").simpleName)
	}
}

@ToString @Accessors
class CollectionMethodsProcessorConfig {
	val boolean generateCollectionMethods
	val boolean recreateOnUnsupportedOperationException

	new(AnnotationReference ann) {
		generateCollectionMethods = ann.getBooleanValue("generateCollectionMethods")
		recreateOnUnsupportedOperationException = ann.getBooleanValue("recreateOnUnsupportedOperationException")
	}
}

@ToString @Accessors
class ToStringProcessorConfig {
	val boolean generateToString
	val boolean skipNulls
	val boolean singleLine
	val boolean hideFieldNames
	val boolean verbatimValues

	new(AnnotationReference toStringConfig) {
		generateToString = toStringConfig.getBooleanValue("generateToString")
		skipNulls = toStringConfig.getBooleanValue("skipNulls")
		singleLine = toStringConfig.getBooleanValue("singleLine")
		hideFieldNames = toStringConfig.getBooleanValue("hideFieldNames")
		verbatimValues = toStringConfig.getBooleanValue("verbatimValues")
	}
}

@ToString @Accessors
class JpaProcessorConfig {
	val CompositeIdType compositeIdType

	new(AnnotationReference ann) {
		compositeIdType = CompositeIdType.valueOf(ann.getEnumValue("compositeIdType").simpleName)
	}
}
