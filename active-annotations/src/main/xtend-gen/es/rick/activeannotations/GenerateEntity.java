package es.rick.activeannotations;

import es.rick.activeannotations.GenerateConstructors;
import es.rick.activeannotations.JaxbConfig;
import es.rick.activeannotations.JpaConfig;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface GenerateEntity {
  public boolean generateBuilder() default false;
  public boolean generateToString() default true;
  /**
   * @see https://www.artima.com/lejava/articles/equality.html
   * @return
   */
  public boolean generateEqualsAndHashCode() default true;
  public boolean generateFieldsEnum() default false;
  public boolean generateCollectionMethods() default true;
  public boolean cloneable() default true;
  public boolean serializable() default true;
  public boolean accessors() default true;
  public GenerateConstructors constructors() default @GenerateConstructors;
  public JpaConfig jpaConfig() default @JpaConfig;
  public JaxbConfig jaxbConfig() default @JaxbConfig;
}
