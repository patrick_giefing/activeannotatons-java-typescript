package es.rick.activeannotations;

import es.rick.activeannotations.PropertyOrder;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface JaxbConfig {
  public boolean xmlAttributes() default false;
  public boolean xmlElementWrappers() default false;
  public PropertyOrder[] propertyOrder() default {};
}
