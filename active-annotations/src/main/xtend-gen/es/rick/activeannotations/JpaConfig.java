package es.rick.activeannotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface JpaConfig {
  public boolean generateCompositeId() default true;
}
