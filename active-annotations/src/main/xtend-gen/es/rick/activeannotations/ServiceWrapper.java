package es.rick.activeannotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface ServiceWrapper {
  public String[] properties() default {};
  public String wrapperPrefix() default "";
  public String wrapperSuffix() default "";
  public String wrapperName() default "";
}
