package es.rick.activeannotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface EntityGroup {
  public Class<?>[] entities();
  public String[] attributeNameWhnitelist() default {};
  public boolean appendCompilerUnitTypes() default false;
}
