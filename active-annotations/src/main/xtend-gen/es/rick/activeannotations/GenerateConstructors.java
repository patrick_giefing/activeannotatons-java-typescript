package es.rick.activeannotations;

import es.rick.activeannotations.GenerateConstructor;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface GenerateConstructors {
  public GenerateConstructor[] value() default {};
  public boolean defaultConstructor() default true;
}
