package es.rick.activeannotations;

import es.rick.activeannotations.ServiceWrapper;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface RestService {
  public Class<?> interfaceClass();
  public String interfaceName();
  public ServiceWrapper[] wrappers() default {};
}
