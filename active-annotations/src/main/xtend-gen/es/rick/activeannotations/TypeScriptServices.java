package es.rick.activeannotations;

import es.rick.activeannotations.TypeScriptEntities;
import es.rick.activeannotations.TypeScriptImport;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface TypeScriptServices {
  public String directory();
  public String filename();
  public Class<?>[] interfaceClasses();
  public Class<?>[] webSocketSubscriberClasses();
  public Class<?>[] webSocketRpcClasses();
  public TypeScriptEntities[] entityLibrary() default {};
  public TypeScriptImport[] entityImports() default {};
}
