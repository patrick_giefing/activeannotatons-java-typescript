package es.rick.activeannotations;

import es.rick.activeannotations.RestService;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface RestServices {
  public RestService[] value() default {};
  public boolean innerClasses() default true;
}
