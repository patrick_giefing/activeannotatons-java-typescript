package es.rick.activeannotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface TypeScriptEntities {
  public String directory();
  public String filename();
  public Class<?>[] entityClasses();
}
