package es.rick.activeannotations;

@SuppressWarnings("all")
public enum PropertyOrder {
  ParentFirst,
  
  ParentLast,
  
  AlphabeticalAsc,
  
  AlphabeticalDesc,
  
  IdsFirst,
  
  IdsLast,
  
  PrimitiveFirst,
  
  PrimitiveLast;
}
